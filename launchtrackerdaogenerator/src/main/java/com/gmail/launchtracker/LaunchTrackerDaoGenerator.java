package com.gmail.launchtracker;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class LaunchTrackerDaoGenerator {

    public static void main(String[] args) throws Exception {

        Schema schema = new Schema(1000, "com.gmail.launchtracker");
        createForecastEntity(schema);

        new DaoGenerator().generateAll(schema, "../app/src/main/java");
    }

    public static void createForecastEntity(Schema schema) {
        Entity forecast = schema.addEntity("Forecast");

        forecast.setTableName("FORECAST");
        forecast.addIdProperty();
        forecast.addStringProperty("desc");
        forecast.addStringProperty("tempMinMax");
        forecast.addStringProperty("day");
        forecast.addStringProperty("day_full");
        forecast.addStringProperty("date");
        forecast.addStringProperty("time");
        forecast.addStringProperty("icon");
        forecast.addIntProperty("temp");
        forecast.addDoubleProperty("pressure");
        forecast.addDoubleProperty("humidity");
        forecast.addDoubleProperty("windSpeed");
        forecast.addIntProperty("cloud");
    }
 }
