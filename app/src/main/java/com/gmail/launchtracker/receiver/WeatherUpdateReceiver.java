package com.gmail.launchtracker.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gmail.launchtracker.network.services.GetWeatherIntentService;

public class WeatherUpdateReceiver extends BroadcastReceiver {
    public WeatherUpdateReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent update_weather = new Intent(context, GetWeatherIntentService.class);
        context.startService(update_weather);
    }
}
