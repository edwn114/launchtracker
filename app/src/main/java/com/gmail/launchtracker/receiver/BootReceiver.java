package com.gmail.launchtracker.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gmail.launchtracker.network.services.GetLaunchesIntentService;
import com.gmail.launchtracker.network.services.GetWeatherIntentService;
import com.gmail.launchtracker.models.Constants;

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent update_upcoming_launches = new Intent(context, GetLaunchesIntentService.class);
        update_upcoming_launches.setAction(Constants.ACTION_GET_UPCOMING_LAUNCHES);

        context.startService(update_upcoming_launches);

        Intent update_weather = new Intent(context, GetWeatherIntentService.class);
        context.startService(update_weather);
    }
}
