package com.gmail.launchtracker.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gmail.launchtracker.network.services.NotifyUserIntentService;

public class NotifyUserReceiver extends BroadcastReceiver {

    public NotifyUserReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notifyUser = new Intent(context, NotifyUserIntentService.class);
        context.startService(notifyUser);
    }
}
