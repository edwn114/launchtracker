package com.gmail.launchtracker.network.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.mvp_presenters.NextLaunchesPresenter;
import com.gmail.launchtracker.storage.SharedPreference;

import java.util.Calendar;

public class GetLaunchesIntentService extends IntentService {

    private SharedPreference sharedPreference;
    private SharedPreferences sharedPref;
    private Calendar calendar;

    public GetLaunchesIntentService() {
        super("GetLaunchesIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        sharedPreference = SharedPreference.getInstance(getApplicationContext());
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        calendar = Calendar.getInstance();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent != null) {
            final String action = intent.getAction();

            if (Constants.ACTION_GET_UPCOMING_LAUNCHES.equals(action)) {

                new NextLaunchesPresenter(getApplicationContext(), sharedPreference, sharedPref, calendar);
            } else if (Constants.ACTION_GET_PREVIOUS_LAUNCHES.equals(action)) {
                // new Prev
            }
        }
    }

   /* public void parseSpaceFlightNowLaunchWebPage() {
        Intent intent = new Intent(getApplicationContext(), GetSpaceFlightDataIntentService.class);
        getApplicationContext().startService(intent);
    } */
}
