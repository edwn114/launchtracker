package com.gmail.launchtracker.network.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.gmail.launchtracker.views.activities.MainActivity;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.storage.SharedPreference;

public class NotifyUserIntentService extends IntentService {
    private SharedPreference sharedPreference;
    private int next_launch_pos = 0;

    public NotifyUserIntentService() {
        super("NotifyUserIntentService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreference = SharedPreference.getInstance(getApplicationContext());
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        for (int i = 0; i < sharedPreference.getUpcomingLaunches().size(); i++) {
            if (sharedPreference.getUpcomingLaunches().get(i).getNet() != 0) {
                next_launch_pos = i;
                break;
            }
        }

        if (intent != null) {
            showNotification();
        }
    }

    private void showNotification() {
        NotificationManager mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        CharSequence title = sharedPreference.getUpcomingLaunches().get(next_launch_pos).getName();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        CharSequence text;

        if(sharedPref.getBoolean("localTime", false))
            text = sharedPreference.getUpcomingLaunches().get(next_launch_pos).getLocal_notification();
        else
            text = sharedPreference.getUpcomingLaunches().get(next_launch_pos).getGlobal_notification();

        PendingIntent contentIntent = PendingIntent.getActivity(
                this, Constants.NOTIFY_USER, new Intent(this, MainActivity.class), 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notifications_white_24dp)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .setContentIntent(contentIntent);

        if(sharedPref.getBoolean("vibration", false))
            notificationBuilder.setVibrate(new long[] {500, 1000});

        mNM.notify(Constants.NOTIFY_USER, notificationBuilder.build());
    }
}
