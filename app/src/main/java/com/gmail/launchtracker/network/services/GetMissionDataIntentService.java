package com.gmail.launchtracker.network.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gmail.launchtracker.models.launchlibrary_1_2.Mission;
import com.gmail.launchtracker.models.launchlibrary_1_2.Rocket;
import com.gmail.launchtracker.network.GsonRequest;
import com.gmail.launchtracker.storage.SharedPreference;
import com.gmail.launchtracker.network.VolleyHelper;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.check.CheckPosition;
import com.gmail.launchtracker.models.rocketDetail.Missions;
import com.gmail.launchtracker.models.rocket.Rockets;

import java.util.ArrayList;

public class GetMissionDataIntentService extends IntentService {

    private SharedPreference sharedPreference;
    private int pos;

    public GetMissionDataIntentService() {
        super("GetMissionDataIntentService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreference = SharedPreference.getInstance(getApplicationContext());
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent != null) {
            pos = intent.getIntExtra("pos", sharedPreference.getCurrentPosition());
            final String action = intent.getAction();

            if (Constants.ACTION_GET_UPCOMING_MISSION_DATA.equals(action)) {
                getUpComingMission();
            } else if(Constants.ACTION_GET_PREVIOUS_MISSION_DATA.equals(action)) {
                getPreviousMission();
            }
        }
    }

    private void getUpComingMission() {
        new DoParsingUpcoming().execute();
    }

    private void getPreviousMission() {
        new DoParsingPrevious().execute();
    }


    private class DoParsingUpcoming extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<Mission> missionArrayList = new ArrayList<>();

            Rocket rocket;
            int value_rocket;
            int value_mission;
            String url_rocket = "https://launchlibrary.net/1.1/rocket/";
            String url_mission = "https://launchlibrary.net/1.1/mission/";

            missionArrayList.clear();
            missionArrayList.addAll(sharedPreference.getLaunchesUpcoming().get(pos).getMissions());

            rocket = sharedPreference.getLaunchesUpcoming().get(pos).getRocket();

            value_rocket = rocket.getId();

            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(
                    new GsonRequest<>(url_rocket.toLowerCase() + value_rocket, Rockets.class, null,
                            new Response.Listener<Rockets>() {
                                @Override
                                public void onResponse(final Rockets response) {
                                    com.gmail.launchtracker.models.rocket.Rocket rocket1
                                            = response.getRockets().get(0);
                                    sharedPreference.addDetailRocketUpcoming(pos, rocket1);

                                    ArrayList<CheckPosition> getPositions =
                                            new ArrayList<>(sharedPreference.getRocketPositionsUpcoming());
                                    getPositions.get(pos).setChecked(true);
                                    sharedPreference.saveRocketPositionsUpcoming(getPositions);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ArrayList<CheckPosition> getPositions =
                                    new ArrayList<>(sharedPreference.getRocketPositionsUpcoming());
                            getPositions.get(pos).setChecked(false);
                            sharedPreference.saveRocketPositionsUpcoming(getPositions);
                        }
                    })
            );

            for (int j = 0; j < missionArrayList.size(); j++) {
                value_mission = missionArrayList.get(j).getId();

                VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(
                        new GsonRequest<>(url_mission.toLowerCase() + value_mission, Missions.class, null,
                                new Response.Listener<Missions>() {
                                    @Override
                                    public void onResponse(final Missions response) {

                                        sharedPreference.saveMissionsUpcoming(pos, response.getMissions());

                                        ArrayList<CheckPosition> getPositions =
                                                new ArrayList<>(sharedPreference.getPositionsUpcoming());
                                        getPositions.get(pos).setChecked(true);
                                        sharedPreference.savePositionsUpcoming(getPositions);
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ArrayList<CheckPosition> getPositions =
                                        new ArrayList<>(sharedPreference.getPositionsUpcoming());
                                getPositions.get(pos).setChecked(false);
                                sharedPreference.savePositionsUpcoming(getPositions);
                            }
                        })
                );
            }

            return null;
        }
    }


    private class DoParsingPrevious extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<Mission> missionArrayList = new ArrayList<>();

            Rocket rocket;
            int value_rocket;
            int value_mission;
            String url_rocket = "https://launchlibrary.net/1.2/rocket/";
            String url_mission = "https://launchlibrary.net/1.2/mission/";

            missionArrayList.clear();
            missionArrayList.addAll(sharedPreference.getLaunchesPrevious().get(pos).getMissions());

            rocket = sharedPreference.getLaunchesPrevious().get(pos).getRocket();

            value_rocket = rocket.getId();

            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(
                    new GsonRequest<>(url_rocket.toLowerCase() + value_rocket, Rockets.class, null,
                            new Response.Listener<Rockets>() {
                                @Override
                                public void onResponse(final Rockets response) {
                                    com.gmail.launchtracker.models.rocket.Rocket rocket1
                                            = response.getRockets().get(0);

                                    sharedPreference.addDetailRocketPrevious(pos, rocket1);

                                    ArrayList<CheckPosition> getPositions =
                                            new ArrayList<>(sharedPreference.getRocketPositionsPrevious());
                                    getPositions.get(pos).setChecked(true);
                                    sharedPreference.saveRocketPositionsPrevious(getPositions);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            ArrayList<CheckPosition> getPositions =
                                    new ArrayList<>(sharedPreference.getRocketPositionsPrevious());
                            getPositions.get(pos).setChecked(false);
                            sharedPreference.saveRocketPositionsPrevious(getPositions);
                        }
                    })
            );

            for (int j = 0; j < missionArrayList.size(); j++) {
                value_mission = missionArrayList.get(j).getId();

                VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(
                        new GsonRequest<>(url_mission.toLowerCase() + value_mission, Missions.class, null,
                                new Response.Listener<Missions>() {
                                    @Override
                                    public void onResponse(final Missions response) {

                                        sharedPreference.saveMissionsPrevious(pos, response.getMissions());

                                        ArrayList<CheckPosition> getPositions =
                                                new ArrayList<>(sharedPreference.getPositionsPrevious());
                                        getPositions.get(pos).setChecked(true);
                                        sharedPreference.savePositionsPrevious(getPositions);
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ArrayList<CheckPosition> getPositions =
                                        new ArrayList<>(sharedPreference.getPositionsPrevious());
                                getPositions.get(pos).setChecked(false);
                                sharedPreference.savePositionsPrevious(getPositions);
                            }
                        })
                );
            }

            return null;
        }
    }
}
