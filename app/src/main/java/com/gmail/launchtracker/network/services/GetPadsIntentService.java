package com.gmail.launchtracker.network.services;

import android.app.IntentService;
import android.content.Intent;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gmail.launchtracker.network.GsonRequest;
import com.gmail.launchtracker.network.VolleyHelper;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.check.CheckPosition;
import com.gmail.launchtracker.models.rocket.Pad;
import com.gmail.launchtracker.models.rocket.Pads;
import com.gmail.launchtracker.storage.SharedPreference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GetPadsIntentService extends IntentService {

    private SharedPreference sharedPreference;
    private int pos;

    public GetPadsIntentService() {
        super("GetPadsIntentService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreference = SharedPreference.getInstance(getApplicationContext());

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            pos = intent.getIntExtra("pos", sharedPreference.getCurrentPosition());
            final String action = intent.getAction();

            if (Constants.ACTION_GET_UPCOMING_PADS.equals(action)) {
                getUpComingPads();
            } else if(Constants.ACTION_GET_PREVIOUS_PADS.equals(action)) {
                getPreviousPads();
            }
        }
    }

    public void getUpComingPads() {

        String padValues = sharedPreference.getDetailRocketsUpcoming().get(pos).getDefaultPads();

        if (padValues == null)
            return;

        List<String> padList;

        if (padValues.contains(",")) {
            String[] pads = padValues.split(",");
            padList = Arrays.asList(pads);
        } else {
            padList = new ArrayList<>();
            padList.add(padValues);
        }

        for (int i = 0; i < padList.size(); i++) {

            String url_pad = "https://launchlibrary.net/1.2/pad/";
            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(
                    new GsonRequest<>(url_pad.toLowerCase() + padList.get(i), Pads.class, null,
                            new Response.Listener<Pads>() {
                                @Override
                                public void onResponse(final Pads response) {
                                    if(response.getPadList().size() != 0) {
                                        Pad pad = response.getPadList().get(0);
                                        sharedPreference.addPadUpcoming(pos, pad);

                                        ArrayList<CheckPosition> getPositions =
                                                new ArrayList<>(sharedPreference.getPadPositionsUpcoming());
                                        getPositions.get(pos).setChecked(true);
                                        sharedPreference.savePadPositionsUpcoming(getPositions);
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ArrayList<CheckPosition> getPositions =
                                    new ArrayList<>(sharedPreference.getPadPositionsUpcoming());
                            getPositions.get(pos).setChecked(false);
                            sharedPreference.savePadPositionsUpcoming(getPositions);
                        }
                    })
            );
        }
    }

    public void getPreviousPads() {

        String padValues = sharedPreference.getDetailRocketsPrevious().get(pos).getDefaultPads();

        if (padValues == null)
            return;

        List<String> padList;

        if (padValues.contains(",")) {
            String[] pads = padValues.split(",");
            padList = Arrays.asList(pads);
        } else {
            padList = new ArrayList<>();
            padList.add(padValues);
        }

        for (int i = 0; i < padList.size(); i++) {

            String url_pad = "https://launchlibrary.net/1.2/pad/";
            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(
                    new GsonRequest<>(url_pad.toLowerCase() + padList.get(i), Pads.class, null,
                            new Response.Listener<Pads>() {
                                @Override
                                public void onResponse(final Pads response) {
                                    if(response.getPadList().size() != 0) {
                                        Pad pad = response.getPadList().get(0);
                                        sharedPreference.addPadPrevious(pos, pad);

                                        ArrayList<CheckPosition> getPositions =
                                                new ArrayList<>(sharedPreference.getPadPositionsPrevious());
                                        getPositions.get(pos).setChecked(true);
                                        sharedPreference.savePadPositionsPrevious(getPositions);
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ArrayList<CheckPosition> getPositions =
                                    new ArrayList<>(sharedPreference.getPadPositionsPrevious());
                            getPositions.get(pos).setChecked(false);
                            sharedPreference.savePadPositionsPrevious(getPositions);
                        }
                    })
            );
        }
    }
}
