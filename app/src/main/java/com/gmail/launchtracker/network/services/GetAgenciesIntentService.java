package com.gmail.launchtracker.network.services;

import android.app.IntentService;
import android.content.Intent;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gmail.launchtracker.network.GsonRequest;
import com.gmail.launchtracker.network.VolleyHelper;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.check.CheckPosition;
import com.gmail.launchtracker.models.rocket.Agencies;
import com.gmail.launchtracker.models.rocket.Agency;
import com.gmail.launchtracker.storage.SharedPreference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GetAgenciesIntentService extends IntentService {

    private SharedPreference sharedPreference;
    private int pos;

    public GetAgenciesIntentService() {
        super("GetAgenciesIntentService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreference = SharedPreference.getInstance(getApplicationContext());

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            pos = intent.getIntExtra("pos", sharedPreference.getCurrentPosition());
            final String action = intent.getAction();

            if (Constants.ACTION_GET_UPCOMING_PADS.equals(action)) {
                getUpComingAgencies();
            } else if(Constants.ACTION_GET_PREVIOUS_PADS.equals(action)) {
                getPreviousAgencies();
            }
        }
    }

    public void getUpComingAgencies() {
        String agencyValues = sharedPreference.
                getDetailRocketsUpcoming().get(pos).getFamily().getAgencies();

        if(agencyValues == null)
            return;

        List<String> agencyList;

        if(agencyValues.contains(",")) {
            String[] agencies = agencyValues.split(",");
            agencyList = Arrays.asList(agencies);
        }
        else {
            agencyList = new ArrayList<>();
            agencyList.add(agencyValues);
        }

        for(int i = 0; i < agencyList.size(); i++) {

            String url_agency = "https://launchlibrary.net/1.2/agency/";
            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(
                    new GsonRequest<>(url_agency.toLowerCase() + agencyList.get(i), Agencies.class, null,
                            new Response.Listener<Agencies>() {
                                @Override
                                public void onResponse(Agencies response) {
                                    Agency agency = response.getAgencies().get(0);
                                    sharedPreference.addAgencyUpcoming(pos, agency);

                                    ArrayList<CheckPosition> getPositions =
                                            new ArrayList<>(sharedPreference.getAgencyPositionsUpcoming());
                                    getPositions.get(pos).setChecked(true);
                                    sharedPreference.saveAgencyPositionsUpcoming(getPositions);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ArrayList<CheckPosition> getPositions =
                                    new ArrayList<>(sharedPreference.getAgencyPositionsUpcoming());
                            getPositions.get(pos).setChecked(false);
                            sharedPreference.saveAgencyPositionsUpcoming(getPositions);
                        }
                    })
            );
        }
    }

    public void getPreviousAgencies() {
        String agencyValues = sharedPreference.getDetailRocketsPrevious().get(pos).getFamily().getAgencies();

        if(agencyValues == null)
            return;

        List<String> agencyList;

        if(agencyValues.contains(",")) {
            String[] agencies = agencyValues.split(",");
            agencyList = Arrays.asList(agencies);
        }
        else {
            agencyList = new ArrayList<>();
            agencyList.add(agencyValues);
        }

        for(int i = 0; i < agencyList.size(); i++) {

            String url_agency = "https://launchlibrary.net/1.1/agency/";
            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(
                    new GsonRequest<>(url_agency.toLowerCase() + agencyList.get(i), Agencies.class, null,
                            new Response.Listener<Agencies>() {
                                @Override
                                public void onResponse(Agencies response) {
                                    Agency agency = response.getAgencies().get(0);
                                    sharedPreference.addAgencyPrevious(pos, agency);

                                    ArrayList<CheckPosition> getPositions =
                                            new ArrayList<>(sharedPreference.getAgencyPositionsPrevious());
                                    getPositions.get(pos).setChecked(true);
                                    sharedPreference.saveAgencyPositionsPrevious(getPositions);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ArrayList<CheckPosition> getPositions =
                                    new ArrayList<>(sharedPreference.getAgencyPositionsPrevious());
                            getPositions.get(pos).setChecked(false);
                            sharedPreference.saveAgencyPositionsPrevious(getPositions);
                        }
                    })
            );
        }
    }
}
