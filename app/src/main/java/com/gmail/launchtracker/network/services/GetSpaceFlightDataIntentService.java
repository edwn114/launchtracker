package com.gmail.launchtracker.network.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.util.ArrayMap;

import com.gmail.launchtracker.models.RocketLaunch;
import com.gmail.launchtracker.storage.SharedPreference;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GetSpaceFlightDataIntentService extends IntentService {
    private SharedPreference sharedPreference;

    public GetSpaceFlightDataIntentService() {
        super("GetSpaceFlightDataIntentService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreference = SharedPreference.getInstance(getApplicationContext());

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        parseSite();
    }

    public void parseSite() {
        try {
            String url = "https://spaceflightnow.com/launch-schedule/";
            Document document = Jsoup.connect(url).get();

            Element containerQuery = document.select("div.mh-container").first();
            Element wrapperQuery = containerQuery.select("div.mh-wrapper").first();
            Element mainQuery = wrapperQuery.select("div.mh-main").first();
            Element contentQuery = mainQuery.select("div.mh-content").first();
            Elements postQuery = contentQuery.select("div.post-64");

            ArrayList<String> date = new ArrayList<>();
            ArrayList<String> name = new ArrayList<>();
            ArrayList<String> desc = new ArrayList<>();

            ArrayMap<Integer, ArrayList<char[]>> web_b1 = new ArrayMap<>();
            ArrayMap<Integer, ArrayList<char[]>> web_b2 = new ArrayMap<>();

            ArrayList<String> web_payload = new ArrayList<>();

            ArrayMap<Integer, ArrayList<char[]>> library_b1 = new ArrayMap<>();
            ArrayMap<Integer, ArrayList<char[]>> library_b2 = new ArrayMap<>();

            ArrayList<String> library_payload = new ArrayList<>();

            for (Element entryQuery : postQuery.select("div.entry")) {
                Elements dateName = entryQuery.select("div.datename");
                Elements missDescrip = entryQuery.select("div.missdescrip");

                Elements missionDate = dateName.select("span.launchdate");
                Elements mission = dateName.select("span.mission");

                Iterator<Element> dateIter = missionDate.iterator();
                Iterator<Element> missionIter = mission.iterator();
                Iterator<Element> descIter = missDescrip.iterator();

                date.clear();
                name.clear();
                desc.clear();

                while (dateIter.hasNext() && missionIter.hasNext() && descIter.hasNext()) {
                    Element dateData = dateIter.next();
                    Element missionData = missionIter.next();
                    Element descData = descIter.next();

                    char[] three = new char[3];
                    three[0] = dateData.text().charAt(0);
                    three[1] = dateData.text().charAt(1);
                    three[2] = dateData.text().charAt(2);

                    date.add(String.valueOf(three));
                    name.add(missionData.text());
                    desc.add(descData.text());
                }

                web_b1.clear();
                web_b2.clear();
                web_payload.clear();

                for (int k = 0; k < name.size(); k++) {
                    String vehicle = "";
                    String payload = "";

                    String name_to_split = name.get(k);

                    String[] parts = name_to_split.split(" ");

                    List<String> ar = Arrays.asList(parts);

                    int getPosOFl = 0;

                    for (int i = 0; i < ar.size(); i++) {
                        if (ar.get(i).equals("•")) {
                            getPosOFl = i;

                            break;
                        }
                    }

                    boolean first_space_check = true;

                    for (int j = 0; j < ar.size(); j++) {
                        if (j < getPosOFl) {
                            vehicle += " " + ar.get(j);
                        } else if (j > getPosOFl && first_space_check) {
                            payload += ar.get(j);
                            first_space_check = false;
                        } else if (j > getPosOFl) {
                            payload += " " + ar.get(j);
                        }
                    }

                    web_payload.add(payload);

                    web_b1.put(k, bi_gram(vehicle.toLowerCase()));
                    web_b2.put(k, bi_gram(payload.toLowerCase()));
                }

                library_b1.clear();
                library_b2.clear();

                library_payload.clear();

                for (int k = 0; k < sharedPreference.getUpcomingLaunches().size(); k++) {
                    String vehicle = "";
                    String payload = "";

                    String name_to_split = sharedPreference.getUpcomingLaunches().get(k).getName();

                    String[] parts = name_to_split.split(" ");

                    List<String> ar = Arrays.asList(parts);

                    int getPosOFl = 0;

                    for (int i = 0; i < ar.size(); i++) {
                        if (ar.get(i).equals("|")) {
                            getPosOFl = i;

                            break;
                        }
                    }

                    for (int j = 0; j < ar.size(); j++) {
                        if (j < getPosOFl) {
                            vehicle += " " + ar.get(j);
                        } else if (j > getPosOFl) {
                            payload += " " + ar.get(j);
                        }
                    }

                    library_payload.add(payload);

                    library_b1.put(k, bi_gram(vehicle.toLowerCase()));
                    library_b2.put(k, bi_gram(payload.toLowerCase()));
                }

                // fuzzy matching of similar string
                for (int i = 0; i < sharedPreference.getUpcomingLaunches().size(); i++) {
                    RocketLaunch rocketLaunch = sharedPreference.getUpcomingLaunches().get(i);

                    int highest_score = 0;
                    int score_pos = 0;

                    String name_to_split = library_payload.get(i);

                    String[] part = name_to_split.split("[\\W]");

                    final List<String> parts = Arrays.asList(part);

                    int largest = 0;
                    int second_largest = 0;

                    String to_search_first = parts.get(0);
                    String to_search_second = parts.get(0);

                    for (int s = 0; s < parts.size(); s++) {
                        if (parts.get(s).length() > largest) {
                            largest = parts.get(s).length();
                            to_search_first = parts.get(s);
                        } else if (parts.get(s).length() > second_largest) {
                            second_largest = parts.get(s).length();
                            to_search_second = parts.get(s);
                        }
                    }

                    for (int j = 0; j < name.size(); j++) {
                        int score = 0;

                        if (match(library_b2.get(i), web_b2.get(j)) > 0.90) {
                            highest_score = 3;
                            score_pos = j;

                            break;
                        }

                        if (rocketLaunch.getFirst_three_month().toLowerCase().
                                equals(date.get(j).toLowerCase())) {
                            score = score + 1;
                        }

                        if (match(library_b1.get(i), web_b1.get(j)) > 0.40) {
                            score = score + 1;
                        }

                        if (match(library_b2.get(i), web_b2.get(j)) > 0.40) {
                            score = score + 1;
                        }

                        if (desc.get(j).toLowerCase().contains(to_search_first.toLowerCase())) {
                            if (desc.get(j).toLowerCase().contains(to_search_second.toLowerCase())) {
                                score = score + 1;
                            } else {
                                if(!checkForSimilarity(name, i, to_search_first)) {
                                    score = score + 1;
                                }
                            }
                        }

                        if (score > highest_score) {
                            highest_score = score;
                            score_pos = j;
                        }
                    }

                    if (highest_score > 2) {
                        sharedPreference.addWebData(i, desc.get(score_pos));
                        sharedPreference.addWebMission(i, web_payload.get(score_pos));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<char[]> bi_gram(String input) {
        ArrayList<char[]> bi_gram = new ArrayList<>();

        for (int i = 0; i < input.length(); i++) {
            char[] chars = new char[2];
            chars[0] = input.charAt(i);

            if (i + 1 < input.length())
                chars[1] = input.charAt(i + 1);
            else
                chars[1] = ' ';

            bi_gram.add(chars);
        }

        return bi_gram;
    }

    public double match(ArrayList<char[]> bi_gram1, ArrayList<char[]> bi_gram2) {
        if (bi_gram1 == null || bi_gram2 == null)
            return 0;

        ArrayList<char[]> copy = new ArrayList<>(bi_gram2);

        int matches = 0;

        for (int i = bi_gram1.size(); --i >= 0; ) {
            char[] bi_gram = bi_gram1.get(i);
            for (int j = copy.size(); --j >= 0; ) {
                char[] toMatch = copy.get(j);
                if (bi_gram[0] == toMatch[0] && bi_gram[1] == toMatch[1]) {
                    copy.remove(j);
                    matches += 2;

                    break;
                }
            }
        }

        return (double) matches / (bi_gram1.size() + bi_gram2.size());
    }

    public boolean checkForSimilarity(ArrayList<String> names, int skip_pos, String value) {
        for(int i = 0; i < names.size(); i++) {
            if(i != skip_pos) {
                if(names.get(i).toLowerCase().contains(value.toLowerCase()))
                    return true;
            }
        }

        return  false;
    }
}