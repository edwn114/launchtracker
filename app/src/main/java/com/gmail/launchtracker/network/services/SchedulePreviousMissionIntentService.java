package com.gmail.launchtracker.network.services;

import android.app.IntentService;
import android.content.Intent;

import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.storage.SharedPreference;

public class SchedulePreviousMissionIntentService extends IntentService {
    private SharedPreference sharedPreference;

    public SchedulePreviousMissionIntentService() {
        super("SchedulePreviousMissionIntentService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreference = SharedPreference.getInstance(getApplicationContext());

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            schedule();
        }
    }

    public void schedule() {
        new Thread() {
            public void run() {
                int timeDelay = 15 * 1000;

                for (int i = 0; i < sharedPreference.getLaunchesPrevious().size(); i++) {
                    Intent intent = new Intent(getApplicationContext(), GetMissionDataIntentService.class);
                    intent.setAction(Constants.ACTION_GET_PREVIOUS_MISSION_DATA);
                    intent.putExtra("pos", i);
                    getApplicationContext().startService(intent);

                    try {
                        Thread.sleep(timeDelay);
                    } catch (Exception ignored) {
                    }
                }
            }
        }.start();
    }
}
