package com.gmail.launchtracker.network.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.mvp_presenters.WeatherForecastPresenter;
import com.gmail.launchtracker.mvp_presenters.WeatherNowPresenter;
import com.gmail.launchtracker.storage.SharedPreference;

public class GetWeatherIntentService extends IntentService {

    // TODO: 9/17/2016 Implement Recurring weather updates with Presenters
    private SharedPreference sharedPreference;
    private SharedPreferences sharedPref;

    private AlarmManager alarmManager;
    private boolean is_weather_now;

    public GetWeatherIntentService() {
        super("GetWeatherIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreference = SharedPreference.getInstance(getApplicationContext());
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        is_weather_now = intent.getBooleanExtra("is_weather_now", false);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (sharedPref.getBoolean("weather_updates", true))
            scheduleWeatherUpdates();

        if (!is_weather_now)
            new WeatherNowPresenter(getApplicationContext(), sharedPreference);
    }

    public void scheduleWeatherUpdates() {

        String frequency = sharedPref.getString("weather_update_frequency", "null");

        long time_to_add;

        switch (frequency) {
            case "every_three_hours":
                time_to_add = 3 * 60 * 60 * 1000;
                break;
            case "every_seven_hours":
                time_to_add = 7 * 60 * 60 * 1000;
                break;
            case "twice_a_day":
                time_to_add = 11 * 60 * 60 * 1000;
                break;
            case "once_a_day":
                time_to_add = 23 * 60 * 60 * 1000;
                break;
            default:
                time_to_add = 7 * 60 * 60 * 1000;
                break;
        }

        int alarmType = AlarmManager.ELAPSED_REALTIME;
        long time_to_update = SystemClock.elapsedRealtime() + time_to_add;

        Intent intent_update_weather = new Intent(Constants.ACTION_UPDATE_WEATHER_STATUS);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this, Constants.WEATHER, intent_update_weather, 0);

        alarmManager.setInexactRepeating(alarmType, time_to_update, time_to_add, pendingIntent);
    }
}
