package com.gmail.launchtracker.views.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.views.fragments.DetailFragment;
import com.gmail.launchtracker.views.fragments.MissionFragment;
import com.gmail.launchtracker.views.fragments.RocketFragment;
import com.gmail.launchtracker.network.services.GetAgenciesIntentService;
import com.gmail.launchtracker.network.services.GetMissionDataIntentService;
import com.gmail.launchtracker.network.services.GetPadsIntentService;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.storage.SharedPreference;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private SharedPreference sharedPreference;

    private ImageView imageView;
    private String imgUrl;

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private TabLayout tabLayout;

    private boolean is_upcoming;
    private boolean use_palette;

    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        boolean dark_theme = sharedPref.getBoolean("theme", false);

        int m_theme;

        if (dark_theme) {
            m_theme = R.style.DarkTheme;
        } else {
            m_theme = R.style.LightTheme;
        }

        setTheme(m_theme);

        sharedPreference = SharedPreference.getInstance(getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        imageView = (ImageView) findViewById(R.id.imageView);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        if(collapsingToolbarLayout != null)
            collapsingToolbarLayout.setTitleEnabled(false);

        if (toolbar != null)
            toolbar.setTitle(getString(R.string.title_activity_detail));

        use_palette = sharedPref.getBoolean("images_use_palette", false);
        is_upcoming = getIntent().getBooleanExtra("IS_UPCOMING", true);

        pos = sharedPreference.getCurrentPosition();

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        setupViewPager(viewPager);

        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
        }

        try {
            if (is_upcoming) {
                if (sharedPreference.getRocketPositionsUpcoming().get(pos).getChecked()) {
                    if (!sharedPreference.getPadPositionsUpcoming().get(pos).getChecked())
                        makePadRequestUpcoming();

                    if (!sharedPreference.getAgencyPositionsUpcoming().get(pos).getChecked())
                        makeAgencyRequestUpcoming();
                } else {
                    fetchUpcomingMissionData();
                }
            } else {
                if (sharedPreference.getRocketPositionsPrevious().get(pos).getChecked()) {
                    if (!sharedPreference.getPadPositionsPrevious().get(pos).getChecked())
                        makePadRequestPrevious();

                    if (!sharedPreference.getAgencyPositionsPrevious().get(pos).getChecked())
                        makeAgencyRequestPrevious();
                } else {
                    fetchPreviousMissionData();
                }

            }
        } catch (NullPointerException ignored) {
        }

        boolean show_image = sharedPref.getBoolean("images", false);

        if (show_image) {
            if (is_upcoming)
                getImageUpcoming();
            else
                getImagePrevious();
        }

        if (!dark_theme) {
            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
/*
            if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        R.color.light_theme_primary_text_color));*/
        } else {
            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

  /*          if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        R.color.dark_theme_primary_text_color));
  */
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter =
                new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putBoolean("IS_UPCOMING", is_upcoming);

        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);
        adapter.addFragment(detailFragment, getText(R.string.summary_title).toString());

        MissionFragment missionFragment = new MissionFragment();
        missionFragment.setArguments(bundle);
        adapter.addFragment(missionFragment, getText(R.string.mission_title).toString());

        RocketFragment rocketFragment = new RocketFragment();
        rocketFragment.setArguments(bundle);
        adapter.addFragment(rocketFragment, getText(R.string.rocket_title).toString());

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    public void makePadRequestUpcoming() {
        Intent intent = new Intent(getApplicationContext(), GetPadsIntentService.class);
        intent.putExtra("pos", pos);
        intent.setAction(Constants.ACTION_GET_UPCOMING_PADS);
        getApplicationContext().startService(intent);
    }

    public void makePadRequestPrevious() {
        Intent intent = new Intent(getApplicationContext(), GetPadsIntentService.class);
        intent.putExtra("pos", pos);
        intent.setAction(Constants.ACTION_GET_PREVIOUS_PADS);
        getApplicationContext().startService(intent);
    }

    public void makeAgencyRequestUpcoming() {
        Intent intent = new Intent(getApplicationContext(), GetAgenciesIntentService.class);
        intent.putExtra("pos", pos);
        intent.setAction(Constants.ACTION_GET_UPCOMING_AGENCIES);
        getApplicationContext().startService(intent);
    }

    public void makeAgencyRequestPrevious() {
        Intent intent = new Intent(getApplicationContext(), GetAgenciesIntentService.class);
        intent.putExtra("pos", pos);
        intent.setAction(Constants.ACTION_GET_PREVIOUS_AGENCIES);
        getApplicationContext().startService(intent);
    }

    public void fetchUpcomingMissionData() {
        Intent intent = new Intent(getApplicationContext(), GetMissionDataIntentService.class);
        intent.putExtra("pos", pos);
        intent.setAction(Constants.ACTION_GET_UPCOMING_MISSION_DATA);
        getApplicationContext().startService(intent);
    }

    public void fetchPreviousMissionData() {
        Intent intent = new Intent(getApplicationContext(), GetMissionDataIntentService.class);
        intent.putExtra("pos", pos);
        intent.setAction(Constants.ACTION_GET_PREVIOUS_MISSION_DATA);
        getApplicationContext().startService(intent);
    }

    public void getImageUpcoming() {
        if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("Soyuz".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449484659/soyuz_vifyhe.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("Falcon".toLowerCase())) {
            if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                    toLowerCase().contains("Heavy".toLowerCase()))
                imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449485242/falcon_heavy_p244cq.jpg";
            else
                imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449485511/falcon9_dshvmx.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("Atlas".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449472928/atlasV_hoxyqw.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("Proton".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449474028/proton_fiuxmm.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("Long March".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449473750/long_march_mzlpes.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("PSLV".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449484741/pslv_xbpgxn.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("Ariane".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449484962/ariane_impkeu.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("Delta".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449485831/delta4_bfyoyx.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("Zenit".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449486332/zenit_h5shm3.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("H-IIA".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449488126/h-iia_zpbm4l.jpg";
        } else if (sharedPreference.getUpcomingLaunches().get(pos).getName().
                toLowerCase().contains("Dnepr".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449488320/Dnepr1_htgqjt.jpg";
        }

        if (use_palette)
            Glide.with(this).load(imgUrl).asBitmap().into(target);
        else
            Glide.with(this).load(imgUrl).asBitmap().into(imageView);
    }

    public void getImagePrevious() {
        if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("Soyuz".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449484659/soyuz_vifyhe.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("Falcon".toLowerCase())) {
            if (sharedPreference.getPreviousLaunches().get(pos).getName().
                    toLowerCase().contains("Heavy".toLowerCase()))
                imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449485242/falcon_heavy_p244cq.jpg";
            else
                imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449485511/falcon9_dshvmx.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("Atlas".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449472928/atlasV_hoxyqw.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("Proton".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449474028/proton_fiuxmm.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("Long March".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449473750/long_march_mzlpes.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("PSLV".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449484741/pslv_xbpgxn.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("Ariane".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449484962/ariane_impkeu.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("Delta".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449485831/delta4_bfyoyx.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("Zenit".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449486332/zenit_h5shm3.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("H-IIA".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449488126/h-iia_zpbm4l.jpg";
        } else if (sharedPreference.getPreviousLaunches().get(pos).getName().
                toLowerCase().contains("Dnepr".toLowerCase())) {
            imgUrl = "http://res.cloudinary.com/edwn112/image/upload/v1449488320/Dnepr1_htgqjt.jpg";
        }

        if (use_palette)
            Glide.with(this).load(imgUrl).asBitmap().into(target);
        else
            Glide.with(this).load(imgUrl).asBitmap().into(imageView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private SimpleTarget target = new SimpleTarget<Bitmap>() {

        @Override
        public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {

            imageView.setImageBitmap(bitmap);

            Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {
                    Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();
                    Palette.Swatch darkVibrantSwatch = palette.getDarkVibrantSwatch();
                    Palette.Swatch lightMutedSwatch = palette.getLightMutedSwatch();

                    if (vibrantSwatch != null) {
                        collapsingToolbarLayout.setContentScrimColor(vibrantSwatch.getRgb());

                        tabLayout.setTabTextColors(vibrantSwatch.getTitleTextColor(),
                                vibrantSwatch.getBodyTextColor());
                    }

                    if (darkVibrantSwatch != null) {
                        collapsingToolbarLayout.setStatusBarScrimColor(darkVibrantSwatch.getRgb());
                    }

                    if (lightMutedSwatch != null) {
                        tabLayout.setSelectedTabIndicatorColor(lightMutedSwatch.getRgb());
                    }
                }
            });
        }
    };
}