package com.gmail.launchtracker.views.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;

public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {
    private Callback mCallback;

    private static final String KEY_1 = "appearance";
    private static final String KEY_2 = "notifications";
    private static final String KEY_3 = "updates";
    private static final String KEY_4 = "more";
    private static final String KEY_5 = "other";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource

        try {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
            boolean dark_theme = sharedPref.getBoolean("theme", false);

            if (dark_theme)
                addPreferencesFromResource(R.xml.settings_dark_theme);
            else
                addPreferencesFromResource(R.xml.settings_light_theme);
        } catch (NullPointerException ignored) {

        }

        if (getActivity() instanceof Callback)
            mCallback = (Callback) getActivity();

        // add listeners for non-default actions
        Preference preference = findPreference(KEY_1);
        preference.setOnPreferenceClickListener(this);

        preference = findPreference(KEY_2);
        preference.setOnPreferenceClickListener(this);

        preference = findPreference(KEY_3);
        preference.setOnPreferenceClickListener(this);

        preference = findPreference(KEY_4);
        preference.setOnPreferenceClickListener(this);

        preference = findPreference(KEY_5);
        preference.setOnPreferenceClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        MyApplication.getInstance().trackScreenView("Settings Fragment");
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {

        // here you should use the same keys as you used in the xml-file
        if (preference.getKey().equals(KEY_1)) {
            mCallback.onNestedPreferenceSelected(NestedPreferenceFragment.NESTED_SCREEN_1_KEY);
        }

        if (preference.getKey().equals(KEY_2)) {
            mCallback.onNestedPreferenceSelected(NestedPreferenceFragment.NESTED_SCREEN_2_KEY);
        }

        if (preference.getKey().equals(KEY_3)) {
            mCallback.onNestedPreferenceSelected(NestedPreferenceFragment.NESTED_SCREEN_3_KEY);
        }

        if (preference.getKey().equals(KEY_4)) {
            mCallback.onNestedPreferenceSelected(NestedPreferenceFragment.NESTED_SCREEN_4_KEY);
        }

        if (preference.getKey().equals(KEY_5)) {
            mCallback.onNestedPreferenceSelected(NestedPreferenceFragment.NESTED_SCREEN_5_KEY);
        }

        return false;
    }

    public interface Callback {
        void onNestedPreferenceSelected(int key);
    }
}