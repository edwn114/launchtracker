package com.gmail.launchtracker.views.activities;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.gmail.launchtracker.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Credits extends AppCompatActivity {
    @BindView(R.id.tvLaunchLibrary)
    TextView tvLaunchLibrary;
    @BindView(R.id.tvTmro)
    TextView tvTmro;
    @BindView(R.id.tvDescription1)
    TextView tvDescription1;
    @BindView(R.id.tvDescription2)
    TextView tvDescription2;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title_text)
    TextView tvTitle;

    private boolean dark_theme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        configureTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);

        ButterKnife.bind(this);

        configureLayout();
    }

    public void configureTheme() {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        dark_theme = sharedPref.getBoolean("theme", false);

        int m_theme;

        if (dark_theme) {
            m_theme = R.style.DarkTheme;
        } else {
            m_theme = R.style.LightTheme;
        }

        setTheme(m_theme);
    }

    public void configureLayout() {

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }

        tvTitle.setText(getString(R.string.credits));

        if (dark_theme) {
            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

            if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        R.color.dark_theme_primary_text_color));

            tvLaunchLibrary.setTextColor(ContextCompat.getColor(getApplicationContext(),
                    R.color.dark_theme_primary_text_color));
            tvTmro.setTextColor(ContextCompat.getColor(getApplicationContext(),
                    R.color.dark_theme_primary_text_color));
            tvDescription1.setTextColor(ContextCompat.getColor(getApplicationContext(),
                    R.color.dark_theme_primary_text_color));
            tvDescription2.setTextColor(ContextCompat.getColor(getApplicationContext(),
                    R.color.dark_theme_primary_text_color));
        } else {
            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

            if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        R.color.light_theme_primary_text_color));

            tvLaunchLibrary.setTextColor(ContextCompat.getColor(getApplicationContext(),
                    R.color.light_theme_primary_text_color));
            tvTmro.setTextColor(ContextCompat.getColor(getApplicationContext(),
                    R.color.light_theme_primary_text_color));
            tvDescription1.setTextColor(ContextCompat.getColor(getApplicationContext(),
                    R.color.light_theme_primary_text_color));
            tvDescription2.setTextColor(ContextCompat.getColor(getApplicationContext(),
                    R.color.light_theme_primary_text_color));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
