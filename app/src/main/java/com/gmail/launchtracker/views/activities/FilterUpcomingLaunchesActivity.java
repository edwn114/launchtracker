package com.gmail.launchtracker.views.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.gmail.launchtracker.views.activities.MainActivity;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.storage.SharedPreference;

public class FilterUpcomingLaunchesActivity extends AppCompatActivity {
    private SharedPreference sharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean dark_theme = sharedPref.getBoolean("theme", false);

        int m_theme;

        if (dark_theme) {
            m_theme = R.style.DarkTheme;
        } else {
            m_theme = R.style.LightTheme;
        }

        setTheme(m_theme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_upcoming_launches);

        TextView tvRocketName = (TextView) findViewById(R.id.tvRocketName);
        TextView tvLocation = (TextView) findViewById(R.id.tvLocation);
        TextView tvTimeRange = (TextView) findViewById(R.id.tvTimeRange);

        sharedPreference = SharedPreference.getInstance(getApplicationContext());

        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        CardView cvRocketFilter = (CardView) findViewById(R.id.cvRocketFilter);
        CardView cvLocationFilter = (CardView) findViewById(R.id.cvLocationFilter);
        CardView cvTimeRangeFilter = (CardView) findViewById(R.id.cvTimeRangeFilter);

        Button btApplyFilter = (Button) findViewById(R.id.btApplyFilters);

        if (btApplyFilter != null) {
            btApplyFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("POS", 2);
                    startActivity(intent);
                }
            });
        }

        final AppCompatSpinner spFilterLocation = (AppCompatSpinner) findViewById(R.id.spFilterLocation);
        final AppCompatSpinner spFilterTimeRange = (AppCompatSpinner) findViewById(R.id.spFilterTimeRange);
        final AppCompatSpinner spFilterRocketName = (AppCompatSpinner) findViewById(R.id.spFilterRocketName);

        ArrayAdapter<CharSequence> adapterLocation;
        ArrayAdapter<CharSequence> adapterTimeRange;
        ArrayAdapter<CharSequence> adapterRocketName;

        adapterLocation = ArrayAdapter.createFromResource(this,
                R.array.locations_array, android.R.layout.simple_spinner_item);
        adapterLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapterTimeRange = ArrayAdapter.createFromResource(this,
                R.array.filter_array, android.R.layout.simple_spinner_item);
        adapterTimeRange.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapterRocketName = ArrayAdapter.createFromResource(this,
                R.array.rocket_array, android.R.layout.simple_spinner_item);
        adapterRocketName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Button btReset = (Button) findViewById(R.id.btReset);

        if (btReset != null) {
            btReset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (spFilterLocation != null) {
                        spFilterLocation.setSelection(0);
                    }
                    sharedPreference.saveUpcomingLocationPosition(0);

                    if (spFilterTimeRange != null) {
                        spFilterTimeRange.setSelection(0);
                    }
                    sharedPreference.saveUpcomingTimeRangePosition(0);

                    if (spFilterRocketName != null) {
                        spFilterRocketName.setSelection(0);
                    }
                    sharedPreference.saveUpcomingRocketPosition(0);
                }
            });
        }


        if (spFilterLocation != null) {
            spFilterLocation.setAdapter(adapterLocation);

            spFilterLocation.setSelection(sharedPreference.getUpcomingLocationPosition());

            spFilterLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    sharedPreference.saveUpcomingLocationPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        if (spFilterTimeRange != null) {
            spFilterTimeRange.setAdapter(adapterTimeRange);

            spFilterTimeRange.setSelection(sharedPreference.getUpcomingTimeRangePosition());

            spFilterTimeRange.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    sharedPreference.saveUpcomingTimeRangePosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        if (spFilterRocketName != null) {
            spFilterRocketName.setAdapter(adapterRocketName);

            spFilterRocketName.setSelection(sharedPreference.getUpcomingRocketPosition());

            spFilterRocketName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    sharedPreference.saveUpcomingRocketPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);

        if (dark_theme) {
            if (cvRocketFilter != null) {
                cvRocketFilter.setCardBackgroundColor(ContextCompat.getColor(this, R.color.dark_theme_card));
            }

            if (cvLocationFilter != null) {
                cvLocationFilter.setCardBackgroundColor(ContextCompat.getColor(this, R.color.dark_theme_card));
            }
            if (cvTimeRangeFilter != null) {
                cvTimeRangeFilter.setCardBackgroundColor(ContextCompat.getColor(this, R.color.dark_theme_card));
            }

            if (tvRocketName != null) {
                tvRocketName.setTextColor(ContextCompat.getColor(this, R.color.dark_theme_primary_text_color));
            }

            if (tvLocation != null) {
                tvLocation.setTextColor(ContextCompat.getColor(this, R.color.dark_theme_primary_text_color));
            }

            if (tvTimeRange != null) {
                tvTimeRange.setTextColor(ContextCompat.getColor(this, R.color.dark_theme_primary_text_color));
            }

            if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        R.color.dark_theme_primary_text_color));

            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {
            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

            if (tvRocketName != null) {
                tvRocketName.setTextColor(ContextCompat.getColor(this, R.color.light_theme_primary_text_color));
            }

            if (tvLocation != null) {
                tvLocation.setTextColor(ContextCompat.getColor(this, R.color.light_theme_primary_text_color));
            }

            if (tvTimeRange != null) {
                tvTimeRange.setTextColor(ContextCompat.getColor(this, R.color.light_theme_primary_text_color));
            }

            if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        R.color.light_theme_primary_text_color));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("POS", 2);
            startActivity(intent);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
