package com.gmail.launchtracker.views.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pwittchen.weathericonview.WeatherIconView;
import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.mvp_presenters.LaunchStatusPresenter;
import com.gmail.launchtracker.mvp_presenters.NextLaunchesPresenter;
import com.gmail.launchtracker.mvp_presenters.WeatherNowPresenter;
import com.gmail.launchtracker.storage.SharedPreference;
import com.gmail.launchtracker.utilities.TimerMinus;
import com.gmail.launchtracker.utilities.TimerPlus;
import com.gmail.launchtracker.views.activities.DetailActivity;
import com.gmail.launchtracker.views.activities.WeatherForecastActivity;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NextLaunchFragment extends Fragment {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.tvTimeLeft)
    TextView tvTimeLeft;
    @BindView(R.id.tvDay)
    TextView tvDay;
    @BindView(R.id.tvHr)
    TextView tvHr;
    @BindView(R.id.tvMin)
    TextView tvMin;
    @BindView(R.id.tvSec)
    TextView tvSec;
    @BindView(R.id.tvTemp)
    TextView tvTemp;
    @BindView(R.id.tvWindSpeed)
    TextView tvWindSpeed;
    @BindView(R.id.tvCloudiness)
    TextView tvCloudiness;
    @BindView(R.id.tvHumidity)
    TextView tvHumidity;
    @BindView(R.id.tvPressure)
    TextView tvPressure;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    //@BindView(R.id.tvLastUpdate) private TextView tvLastUpdate;
    @BindView(R.id.tvFirstColon)
    TextView tvFirstColon;
    @BindView(R.id.tvSecondColon)
    TextView tvSecondColon;
    @BindView(R.id.tvThirdColon)
    TextView tvThirdColon;
    @BindView(R.id.tvDayOfWeek)
    TextView tvDayOfTheWeek;
    @BindView(R.id.tvLastUpdateLaunch)
    TextView tvLastUpdateLaunch;
    @BindView(R.id.tvTimeLeftLabel)
    TextView tvTimeLeftLabel;
    @BindView(R.id.tvDayLabel)
    TextView tvDayLabel;
    @BindView(R.id.tvHrLabel)
    TextView tvHrLabel;
    @BindView(R.id.tvMinLabel)
    TextView tvMinLabel;
    @BindView(R.id.tvSecLabel)
    TextView tvSecLabel;
    @BindView(R.id.tvDay1)
    TextView tvDay1;
    @BindView(R.id.tvDay2)
    TextView tvDay2;
    @BindView(R.id.tvDay3)
    TextView tvDay3;
    @BindView(R.id.tvDay4)
    TextView tvDay4;
    @BindView(R.id.tvDay5)
    TextView tvDay5;

    @BindView(R.id.btWatchLive)
    Button btWatchLive;

    @BindView(R.id.imMainIcon)
    WeatherIconView imMainIcon;

    @BindView(R.id.imWeather1)
    WeatherIconView imWeather1;
    @BindView(R.id.imWeather2)
    WeatherIconView imWeather2;
    @BindView(R.id.imWeather3)
    WeatherIconView imWeather3;
    @BindView(R.id.imWeather4)
    WeatherIconView imWeather4;
    @BindView(R.id.imWeather5)
    WeatherIconView imWeather5;

    @BindView(R.id.ivHumidity)
    ImageView ivHumidity;
    @BindView(R.id.ivWindSpeed)
    ImageView ivWindSpeed;
    @BindView(R.id.ivCloudiness)
    ImageView ivCloudiness;
    @BindView(R.id.ivPressure)
    ImageView ivPressure;

    @BindView(R.id.cvConfirmedLaunch)
    CardView cvConfirmedLaunch;
    @BindView(R.id.cvTimer)
    CardView cvTimer;
    @BindView(R.id.cvWeather)
    CardView cvWeather;

    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    private Unbinder unbinder;

    private int pos = 0;

    private Handler handler;

    private SharedPreference sharedPreference;
    private SharedPreferences sharedPref;
    private Calendar calendar;

    //Don't use butter knife on this view since we are referencing MainActivity's toolbar
    private Toolbar toolbar;

    private boolean stopTimer = false;

    private CountDownTimer blink;
    private CountDownTimer countDownTimer1;
    private CountDownTimer countDownTimer2;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                final String action = intent.getAction();

                if (Constants.ACTION_SUCCESS_UPCOMING_LAUNCHES.equals(action)) {

                    updateNextLaunchUI();
                    updateTimerUI();
                    updateWeather();
                } else if (Constants.ACTION_SUCCESS_GET_WEATHER_NOW.equals(action))
                    updateWeatherUI();
            }
        }
    };

    public NextLaunchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() != null) {
            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

            if (toolbar != null)
                toolbar.setTitle(getText(R.string.next_launch));
        }

        handler = new Handler();

        sharedPreference = SharedPreference.getInstance(getActivity());
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        calendar = Calendar.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_next_launch, container, false);
        unbinder = ButterKnife.bind(this, view);

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLaunches();
            }
        });

        cvTimer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //do nothing
                return true;
            }
        });

        cvWeather.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //do nothing
                return true;
            }
        });

        cvConfirmedLaunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreference.saveCurrentPosition(pos);

                Intent intent = new Intent(v.getContext(), DetailActivity.class);

                ActivityOptionsCompat activityOptionsCompat =
                        ActivityOptionsCompat.makeScaleUpAnimation(v, (int) v.getX(),
                                (int) v.getY(), v.getWidth(), v.getHeight());

                v.getContext().startActivity(intent, activityOptionsCompat.toBundle());
            }
        });

        cvConfirmedLaunch.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //do nothing

                return true;
            }
        });

        cvWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), WeatherForecastActivity.class);

                ActivityOptionsCompat activityOptionsCompat =
                        ActivityOptionsCompat.makeScaleUpAnimation(v, (int) v.getX(),
                                (int) v.getY(), v.getWidth(), v.getHeight());

                v.getContext().startActivity(intent, activityOptionsCompat.toBundle());
            }
        });

        cvWeather.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                return true;
            }
        });

        btWatchLive.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    new LinkDialogFragment().show(getFragmentManager(), "Links");
            }
        });

    /*    btWatchLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = sharedPreference.getUpcomingLaunches().get(pos)
                        .getVidURL().replace("\\", "").toLowerCase();

                if (url.contains("http") || url.contains("https")) {
                    Uri uri = Uri.parse(sharedPreference.getUpcomingLaunches()
                            .get(pos).getVidURL().replace("\\", ""));

                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } else {
                    showToast();
                }
            }
        });*/

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        boolean dark_theme = sharedPref.getBoolean("theme", false);

        tvLastUpdateLaunch.setText(String.format("Last Update : %s",
                sharedPreference.getPrefsLastUpcomingLaunchUpdate()));

        if (dark_theme) {

            imMainIcon.setIconColor(Color.WHITE);

            imWeather1.setIconColor(Color.WHITE);
            imWeather2.setIconColor(Color.WHITE);
            imWeather3.setIconColor(Color.WHITE);
            imWeather4.setIconColor(Color.WHITE);
            imWeather5.setIconColor(Color.WHITE);

            ivHumidity.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.water_white));
            ivWindSpeed.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.weather_windy_white));
            ivCloudiness.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.weather_cloudy_white));
            ivPressure.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.weather_pressure_white));

            cvConfirmedLaunch.setCardBackgroundColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_card));
            cvTimer.setCardBackgroundColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_card));
            cvWeather.setCardBackgroundColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_card));

            tvDayOfTheWeek.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));

            tvDescription.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));

            tvTimeLeft.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_primary_text_color));
            tvDay.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_primary_text_color));
            tvHr.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_primary_text_color));
            tvMin.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_primary_text_color));
            tvSec.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_primary_text_color));

            tvTimeLeftLabel.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));
            tvDayLabel.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));
            tvHrLabel.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));
            tvMinLabel.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));
            tvSecLabel.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));

            tvTemp.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));
            tvWindSpeed.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));
            tvHumidity.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));
            tvPressure.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));

            //tvLastUpdate.setTextColor(ContextCompat.getColor(
            //        getActivity(), R.color.dark_theme_secondary_text_color));

            tvLastUpdateLaunch.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));

            tvName.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_primary_text_color));
            tvLocation.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));

            tvFirstColon.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_primary_text_color));
            tvSecondColon.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_primary_text_color));
            tvThirdColon.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_primary_text_color));

            btWatchLive.setTextColor(ContextCompat.getColor(
                    getActivity(), R.color.dark_theme_secondary_text_color));
        }

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        checkFirstBoot();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_SUCCESS_UPCOMING_LAUNCHES);
        intentFilter.addAction(Constants.ACTION_SUCCESS_GET_WEATHER_NOW);

        getActivity().registerReceiver(broadcastReceiver, intentFilter);

        MyApplication.getInstance().trackScreenView("NextLaunch Fragment");
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().unregisterReceiver(broadcastReceiver);

        if (blink != null)
            blink.cancel();

        if (countDownTimer1 != null)
            countDownTimer1.cancel();

        if (countDownTimer2 != null)
            countDownTimer2.cancel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }

    public void checkFirstBoot() {
        if (sharedPreference.getPrefsFirstBoot()) {
            sharedPreference.setPrefsFirstBoot(false);

            getLaunches();
        } else {
            if (sharedPreference.getUpcomingLaunches() != null) {

                updateNextLaunchUI();
                updateTimerUI();
                updateWeatherUI();

                // blinking animation for timer
                blink();
            }
        }
    }

    public void getLaunches() {
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
            }
        });

        new NextLaunchesPresenter(getActivity(), sharedPreference, sharedPref, swipeContainer, calendar);

        // TODO: 9/17/2016 schedule upcoming launches
    }

    private void blink() {

        blink = new CountDownTimer(1000000000, 500) {

            @Override
            public void onTick(long millisUntilFinished) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (tvFirstColon != null || tvSecondColon != null ||
                                tvThirdColon != null || tvTimeLeft != null) {

                            if (tvFirstColon.getVisibility() == View.VISIBLE)
                                tvFirstColon.setVisibility(View.INVISIBLE);
                            else
                                tvFirstColon.setVisibility(View.VISIBLE);

                            if (tvSecondColon.getVisibility() == View.VISIBLE)
                                tvSecondColon.setVisibility(View.INVISIBLE);
                            else
                                tvSecondColon.setVisibility(View.VISIBLE);

                            if (tvThirdColon.getVisibility() == View.VISIBLE)
                                tvThirdColon.setVisibility(View.INVISIBLE);
                            else
                                tvThirdColon.setVisibility(View.VISIBLE);

                            if (tvTimeLeft.getVisibility() == View.VISIBLE)
                                tvTimeLeft.setVisibility(View.INVISIBLE);
                            else
                                tvTimeLeft.setVisibility(View.VISIBLE);
                        }
                    }

                });
            }

            @Override
            public void onFinish() {

            }
        };

        blink.start();
    }

    public void makeStatusRequest() {
        new LaunchStatusPresenter(getActivity(), sharedPreference, toolbar);
    }

    public void updateNextLaunchUI() {
        for (int i = 0; i < sharedPreference.getUpcomingLaunches().size(); i++) {
            if (sharedPreference.getUpcomingLaunches().get(i).getNet() != 0) {
                pos = i;

                break;
            }
        }

        tvName.setText(sharedPreference.getUpcomingLaunches().get(pos).getName());
        tvLocation.setText(sharedPreference.getUpcomingLaunches().get(pos).getLocation());

        tvLastUpdateLaunch.setText(String.format("Last update - %s", sharedPreference.getPrefsLastUpcomingLaunchUpdate()));

        if ((sharedPreference.getUpcomingLaunches().get(pos).getNet() * 1000 - 15 * 60 * 1000)
                < System.currentTimeMillis())
            makeStatusRequest();
        else {
            if (!sharedPreference.getPrefsStatus())
                makeStatusRequest();
            else {
                if (toolbar != null)
                    toolbar.setTitle(sharedPreference.getPrefsStatusValue());
            }
        }
    }

    public void updateTimerUI() {
        TimerMinus timer = new TimerMinus();
        timer.setLaunchTime(sharedPreference.getUpcomingLaunches().get(pos).getNet());

        tvTimeLeft.setText("-"); // making sure that initial sign is minus, it will be updating accordingly below

        countDownTimer1 = new CountDownTimer(timer.timeLeft(), 1000) {
            private long last_day = -1;
            private long last_hour = -1;
            private long last_minute = -1;
            private long last_second = -1;

            public void onTick(long millisUntilFinished) {

                long days = millisUntilFinished / (24 * 3600000);
                long hours = millisUntilFinished / (3600000) % 24;
                long minutes = millisUntilFinished / (60000) % 60;
                long seconds = millisUntilFinished / 1000 % 60;

                if (last_day != days && tvDay != null)
                    tvDay.setText(String.format(Locale.getDefault(), "%d", days));
                if (last_hour != hours && tvHr != null)
                    tvHr.setText(String.format(Locale.getDefault(), "%d", hours));
                if (last_minute != minutes && tvMin != null)
                    tvMin.setText(String.format(Locale.getDefault(), "%d", minutes));
                if (last_second != seconds && tvSec != null)
                    tvSec.setText(String.format(Locale.getDefault(), "%d", seconds));

                last_day = days;
                last_hour = hours;
                last_minute = minutes;
                last_second = seconds;
            }

            public void onFinish() {
                final TimerPlus timerPlus = new TimerPlus();

                for (int i = 0; i < sharedPreference.getUpcomingLaunches().size(); i++) {
                    if (sharedPreference.getUpcomingLaunches().get(i).getNet() != 0) {
                        timerPlus.setRefreshTime(sharedPreference.getUpcomingLaunches().get(i).getNet());

                        break;
                    }
                }

                if (tvTimeLeft != null)
                    tvTimeLeft.setText("+");

                countDownTimer2 = new CountDownTimer(timerPlus.timeLeft(), 1000) {
                    private long last_day = -1;
                    private long last_hour = -1;
                    private long last_minute = -1;
                    private long last_second = -1;

                    @Override
                    public void onTick(long millisUntilFinished) {

                        if (stopTimer)
                            this.cancel();

                        long time = timerPlus.getPassedTime();

                        long days = time / (24 * 3600000);
                        long hours = time / (3600000) % 24;
                        long minutes = time / (60000) % 60;
                        long seconds = time / 1000 % 60;

                        if (last_day != days && tvDay != null)
                            tvDay.setText(String.format(Locale.getDefault(), "%d", days));
                        if (last_hour != hours && tvHr != null)
                            tvHr.setText(String.format(Locale.getDefault(), "%d", hours));
                        if (last_minute != minutes && tvMin != null)
                            tvMin.setText(String.format(Locale.getDefault(), "%d", minutes));
                        if (last_second != seconds && tvSec != null)
                            tvSec.setText(String.format(Locale.getDefault(), "%d", seconds));

                        last_day = days;
                        last_hour = hours;
                        last_minute = minutes;
                        last_second = seconds;
                    }

                    @Override
                    public void onFinish() {
                        // make a network call to update the list
                        // or let the user update himself / herself
                    }
                };

                countDownTimer2.start();
            }
        };

        countDownTimer1.start();
    }


    public void updateWeather() {

        if (sharedPreference.getPrefsFetchedWeather()) {
            updateWeatherUI();

        } else {
            new WeatherNowPresenter(getActivity(), sharedPreference);
        }
    }

    public void updateWeatherUI() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:
                tvDayOfTheWeek.setText(getString(R.string.sunday));

                tvDay1.setText("MON");
                tvDay2.setText("TUE");
                tvDay3.setText("WED");
                tvDay4.setText("THU");
                tvDay5.setText("FRI");

                break;

            case Calendar.MONDAY:
                tvDayOfTheWeek.setText(getString(R.string.monday));

                tvDay1.setText("TUE");
                tvDay2.setText("WED");
                tvDay3.setText("THU");
                tvDay4.setText("FRI");
                tvDay5.setText("SAT");

                break;

            case Calendar.TUESDAY:
                tvDayOfTheWeek.setText(getString(R.string.tuesday));

                tvDay1.setText("WED");
                tvDay2.setText("THU");
                tvDay3.setText("FRI");
                tvDay4.setText("SAT");
                tvDay5.setText("SUN");

                break;

            case Calendar.WEDNESDAY:
                tvDayOfTheWeek.setText(getString(R.string.wednesday));

                tvDay1.setText("THU");
                tvDay2.setText("FRI");
                tvDay3.setText("SAT");
                tvDay4.setText("SUN");
                tvDay5.setText("MON");

                break;

            case Calendar.THURSDAY:
                tvDayOfTheWeek.setText(getString(R.string.thursday));

                tvDay1.setText("FRI");
                tvDay2.setText("SAT");
                tvDay3.setText("SUN");
                tvDay4.setText("MON");
                tvDay5.setText("TUE");

                break;

            case Calendar.FRIDAY:
                tvDayOfTheWeek.setText(getString(R.string.friday));

                tvDay1.setText("SAT");
                tvDay2.setText("SUN");
                tvDay3.setText("MON");
                tvDay4.setText("TUE");
                tvDay5.setText("WED");

                break;

            case Calendar.SATURDAY:
                tvDayOfTheWeek.setText(getString(R.string.saturday));

                tvDay1.setText("SUN");
                tvDay2.setText("MON");
                tvDay3.setText("TUE");
                tvDay4.setText("WED");
                tvDay5.setText("THU");

                break;
        }

        if (sharedPreference.getPrefsFetchedWeather()) {

            tvDescription.setText(sharedPreference.getWeather().getDescription());
            tvWindSpeed.setText(String.format("%s m/s", sharedPreference.getWeather().getWindSpeed()));
            tvHumidity.setText(String.format("%s %%", sharedPreference.getWeather().getHumidity()));
            tvCloudiness.setText(String.format("%s %%", String.valueOf(sharedPreference.getWeather().getClouds())));
            tvPressure.setText(String.format("%s hp", sharedPreference.getWeather().getPressure()));
            tvTemp.setText(String.format("%s" + (char) 0x00B0, sharedPreference.getWeather().getTemp()));


            String iconName = sharedPreference.getWeather().getIconName();

            if (iconName.equals("01d"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_day_sunny));
            if (iconName.equals("01n"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_night_clear));
            if (iconName.equals("02d"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_day_cloudy));
            if (iconName.equals("03d") || iconName.equals("03n"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_cloud));
            if (iconName.equals("02n"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_night_alt_cloudy));
            if (iconName.equals("04d") || iconName.equals("04n"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_cloudy));
            if (iconName.equals("09d") || iconName.equals("09n"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_showers));
            if (iconName.equals("10d"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_day_rain));
            if (iconName.equals("10n"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_night_alt_rain));
            if (iconName.equals("11d") || iconName.equals("11n"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_thunderstorm));
            if (iconName.equals("13d") || iconName.equals("13n"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_snow));
            if (iconName.equals("50d") || iconName.equals("50n"))
                imMainIcon.setIconResource(getActivity().getResources().getString(R.string.wi_fog));
        }

        /*
        tvLastUpdate.setText(String.format("Last Update : %s",
                sharedPreference.getWeather().getLast_update_time()));
        */
    }
}
