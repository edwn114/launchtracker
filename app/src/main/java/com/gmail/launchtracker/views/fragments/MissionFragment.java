package com.gmail.launchtracker.views.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.rocketDetail.Agency;
import com.gmail.launchtracker.models.rocketDetail.Mission;
import com.gmail.launchtracker.storage.SharedPreference;

public class MissionFragment extends Fragment {
    private SharedPreference sharedPreference;

    int pos;

    private boolean dark_theme;

    public MissionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreference = SharedPreference.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mission, container, false);

        pos = sharedPreference.getCurrentPosition();

        boolean is_upcoming = getArguments().getBoolean("IS_UPCOMING");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        dark_theme = sharedPref.getBoolean("theme", false);

        if (is_upcoming)
            display_upcoming(view);

        else
            display_previous(view);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Mission Fragment");
    }

    private void display_upcoming(View view) {

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());

        LinearLayout viewToInsertLayouts = (LinearLayout) view.findViewById(R.id.layoutToInflate);

        try {
            if (sharedPreference.getPositionsUpcoming() != null) {
                if (sharedPreference.getPositionsUpcoming().get(pos).getChecked()) {
                    View payloadTitle = layoutInflater.inflate(R.layout.payload_title,
                            viewToInsertLayouts, false);
                    viewToInsertLayouts.addView(payloadTitle);


                    if (dark_theme) {
                        TextView tvPayloadTitle = (TextView) payloadTitle.findViewById(R.id.tvPayLoadTitle);
                        tvPayloadTitle.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.dark_theme_primary_text_color));
                    }

                    for (int i = 0; i < sharedPreference.getMissionsUpcoming().get(pos).size(); i++) {

                        final Mission mission = sharedPreference.getMissionsUpcoming().get(pos).get(i);

                        View heading_layout = layoutInflater.inflate(R.layout.mission_heading_layout,
                                viewToInsertLayouts, false);
                        viewToInsertLayouts.addView(heading_layout);

                        TextView tvNameMission = (TextView) heading_layout.findViewById(R.id.tvName);
                        tvNameMission.setText(mission.getName());

                        TextView tvDescription = (TextView) heading_layout.findViewById(R.id.tvDescription);
                        tvDescription.setText(mission.getDescription());

                        Button infoMission = (Button) heading_layout.findViewById(R.id.btInfo);

                        infoMission.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (mission.getInfoURL().length() < 5)
                                    showToast();
                                else {
                                    Uri uri = Uri.parse(mission.getInfoURL());
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }
                        });

                        Button wikiMission = (Button) heading_layout.findViewById(R.id.btWiki);

                        wikiMission.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (mission.getWikiURL().length() < 5)
                                    showToast();
                                else {
                                    Uri uri = Uri.parse(mission.getWikiURL());
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }
                        });


                        if (dark_theme) {
                            CardView headingCardView = (CardView) heading_layout.findViewById(
                                    R.id.mission_heading);
                            headingCardView.setCardBackgroundColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_card));

                            tvNameMission.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_primary_text_color));
                            tvDescription.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                            infoMission.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                            wikiMission.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                        }

                        if (mission.getAgencies() != null) {
                            if (mission.getAgencies().size() > 0) {
                                View agencyTitle = layoutInflater.inflate(
                                        R.layout.fragment_rocket_agency, viewToInsertLayouts, false);
                                viewToInsertLayouts.addView(agencyTitle);

                                if (dark_theme) {
                                    TextView tvRocketAgency = (TextView)
                                            agencyTitle.findViewById(R.id.tvRocketAgency);
                                    tvRocketAgency.setTextColor(ContextCompat.getColor(
                                            getActivity(), R.color.dark_theme_primary_text_color));
                                }
                            }

                            for (int j = 0; j < mission.getAgencies().size(); j++) {

                                final Agency agency = mission.getAgencies().get(j);

                                View agency_layout = layoutInflater.inflate(
                                        R.layout.mission_agency_layout, viewToInsertLayouts, false);
                                viewToInsertLayouts.addView(agency_layout);

                                TextView tvNameAgency = (TextView) agency_layout.findViewById(R.id.tvName);
                                tvNameAgency.setText(agency.getName());

                                TextView tvAbbrevAgency = (TextView) agency_layout.findViewById(R.id.tvAbbrev);
                                tvAbbrevAgency.setText(String.format("Abbrev : %s", agency.getAbbrev()));

                                TextView tvCountryCodeAgency = (TextView)
                                        agency_layout.findViewById(R.id.tvCountryCode);
                                tvCountryCodeAgency.setText(
                                        String.format("Country Code : %s", agency.getCountryCode()));

                                TextView tvTypeAgency = (TextView) agency_layout.findViewById(R.id.tvType);
                                tvTypeAgency.setText(String.format("Type : %s",
                                        Constants.getAgencyType(agency.getType())));

                                Button infoAgency = (Button) agency_layout.findViewById(R.id.btInfo);

                                infoAgency.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        if (agency.getInfoURL().length() < 5)
                                            showToast();
                                        else {
                                            Uri uri = Uri.parse(agency.getInfoURL());
                                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                            startActivity(intent);
                                        }
                                    }
                                });

                                Button wikiAgency = (Button) agency_layout.findViewById(R.id.btWiki);

                                wikiAgency.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (agency.getWikiURL().length() < 5)
                                            showToast();
                                        else {
                                            Uri uri = Uri.parse(agency.getWikiURL());
                                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                            startActivity(intent);
                                        }
                                    }
                                });

                                if (dark_theme) {
                                    CardView agencyCardView = (CardView)
                                            agency_layout.findViewById(R.id.mission_agency);
                                    agencyCardView.setCardBackgroundColor(
                                            ContextCompat.getColor(getActivity(), R.color.dark_theme_card));

                                    tvNameAgency.setTextColor(ContextCompat.getColor(
                                            getActivity(), R.color.dark_theme_primary_text_color));
                                    tvAbbrevAgency.setTextColor(ContextCompat.getColor(
                                            getActivity(), R.color.dark_theme_secondary_text_color));
                                    tvCountryCodeAgency.setTextColor(ContextCompat.getColor(
                                            getActivity(), R.color.dark_theme_secondary_text_color));
                                    tvTypeAgency.setTextColor(ContextCompat.getColor(
                                            getActivity(), R.color.dark_theme_secondary_text_color));
                                    infoAgency.setTextColor(ContextCompat.getColor(
                                            getActivity(), R.color.dark_theme_secondary_text_color));
                                    wikiAgency.setTextColor(ContextCompat.getColor(
                                            getActivity(), R.color.dark_theme_secondary_text_color));
                                }
                            }
                            View margin_blank = layoutInflater.inflate(
                                    R.layout.margin_blank, viewToInsertLayouts, false);
                            viewToInsertLayouts.addView(margin_blank);
                        }
                    }
                } else if (sharedPreference.getWebData().get(pos).length() > 10) {
                    View payloadTitle = layoutInflater.inflate(
                            R.layout.payload_title, viewToInsertLayouts, false);
                    viewToInsertLayouts.addView(payloadTitle);

                    if (dark_theme) {
                        TextView tvPayloadTitle = (TextView) payloadTitle.findViewById(R.id.tvPayLoadTitle);
                        tvPayloadTitle.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_primary_text_color));
                    }

                    View heading_layout = layoutInflater.inflate(
                            R.layout.mission_heading_layout, viewToInsertLayouts, false);
                    viewToInsertLayouts.addView(heading_layout);

                    TextView tvNameMission = (TextView) heading_layout.findViewById(R.id.tvName);
                    tvNameMission.setText(sharedPreference.getWebMissionList().get(pos));

                    TextView tvDescription = (TextView) heading_layout.findViewById(R.id.tvDescription);
                    tvDescription.setText(sharedPreference.getWebData().get(pos));

                    Button infoMission = (Button) heading_layout.findViewById(R.id.btInfo);

                    infoMission.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });

                    Button wikiMission = (Button) heading_layout.findViewById(R.id.btWiki);

                    wikiMission.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });

                    if (dark_theme) {
                        CardView headingCardView = (CardView) heading_layout.findViewById(R.id.mission_heading);
                        headingCardView.setCardBackgroundColor(
                                ContextCompat.getColor(getActivity(), R.color.dark_theme_card));

                        tvNameMission.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_primary_text_color));
                        tvDescription.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                        infoMission.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                        wikiMission.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                    }
                }
            }
        } catch (NullPointerException ignored) {

        }
    }

    private void display_previous(View view) {

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());

        LinearLayout viewToInsertLayouts = (LinearLayout) view.findViewById(R.id.layoutToInflate);

        if (sharedPreference.getPositionsPrevious().get(pos).getChecked()) {

            View payloadTitle = layoutInflater.inflate(R.layout.payload_title, viewToInsertLayouts, false);
            viewToInsertLayouts.addView(payloadTitle);


            if (dark_theme) {
                TextView tvPayloadTitle = (TextView) payloadTitle.findViewById(R.id.tvPayLoadTitle);
                tvPayloadTitle.setTextColor(ContextCompat.getColor(
                        getActivity(), R.color.dark_theme_primary_text_color));
            }

            for (int i = 0; i < sharedPreference.getMissionsPrevious().get(pos).size(); i++) {

                final Mission mission = sharedPreference.getMissionsPrevious().get(pos).get(i);

                View heading_layout = layoutInflater.inflate(
                        R.layout.mission_heading_layout, viewToInsertLayouts, false);
                viewToInsertLayouts.addView(heading_layout);

                TextView tvNameMission = (TextView) heading_layout.findViewById(R.id.tvName);
                tvNameMission.setText(mission.getName());

                TextView tvDescription = (TextView) heading_layout.findViewById(R.id.tvDescription);
                tvDescription.setText(mission.getDescription());

                Button infoMission = (Button) heading_layout.findViewById(R.id.btInfo);

                infoMission.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (mission.getInfoURL().length() < 5)
                            showToast();
                        else {
                            Uri uri = Uri.parse(mission.getInfoURL());
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    }
                });

                Button wikiMission = (Button) heading_layout.findViewById(R.id.btWiki);

                wikiMission.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (mission.getWikiURL().length() < 5)
                            showToast();
                        else {
                            Uri uri = Uri.parse(mission.getWikiURL());
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    }
                });


                if (dark_theme) {
                    CardView headingCardView = (CardView) heading_layout.findViewById(R.id.mission_heading);
                    headingCardView.setCardBackgroundColor(
                            ContextCompat.getColor(getActivity(), R.color.dark_theme_card));

                    tvNameMission.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_primary_text_color));
                    tvDescription.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_secondary_text_color));
                    infoMission.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_secondary_text_color));
                    wikiMission.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_secondary_text_color));
                }

                if (mission.getAgencies().size() > 0) {
                    View agencyTitle = layoutInflater.inflate(
                            R.layout.fragment_rocket_agency, viewToInsertLayouts, false);
                    viewToInsertLayouts.addView(agencyTitle);

                    if (dark_theme) {
                        TextView tvRocketAgency = (TextView) agencyTitle.findViewById(R.id.tvRocketAgency);
                        tvRocketAgency.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_primary_text_color));
                    }
                }

                for (int j = 0; j < mission.getAgencies().size(); j++) {

                    final Agency agency = mission.getAgencies().get(j);

                    View agency_layout = layoutInflater.inflate(
                            R.layout.mission_agency_layout, viewToInsertLayouts, false);
                    viewToInsertLayouts.addView(agency_layout);

                    TextView tvNameAgency = (TextView) agency_layout.findViewById(R.id.tvName);
                    tvNameAgency.setText(agency.getName());

                    TextView tvAbbrevAgency = (TextView) agency_layout.findViewById(R.id.tvAbbrev);
                    tvAbbrevAgency.setText(String.format("Abbrev : %s", agency.getAbbrev()));

                    TextView tvCountryCodeAgency = (TextView) agency_layout.findViewById(R.id.tvCountryCode);
                    tvCountryCodeAgency.setText(String.format("Country Code : %s", agency.getCountryCode()));

                    TextView tvTypeAgency = (TextView) agency_layout.findViewById(R.id.tvType);
                    tvTypeAgency.setText(String.format("Type : %s", Constants.getAgencyType(agency.getType())));

                    Button infoAgency = (Button) agency_layout.findViewById(R.id.btInfo);

                    infoAgency.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (agency.getInfoURL().length() < 5)
                                showToast();
                            else {
                                Uri uri = Uri.parse(agency.getInfoURL());
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        }
                    });

                    Button wikiAgency = (Button) agency_layout.findViewById(R.id.btWiki);

                    wikiAgency.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (agency.getWikiURL().length() < 5)
                                showToast();
                            else {
                                Uri uri = Uri.parse(agency.getWikiURL());
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        }
                    });

                    if (dark_theme) {
                        CardView agencyCardView = (CardView) agency_layout.findViewById(R.id.mission_agency);
                        agencyCardView.setCardBackgroundColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_card));

                        tvNameAgency.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.dark_theme_primary_text_color));
                        tvAbbrevAgency.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.dark_theme_secondary_text_color));
                        tvCountryCodeAgency.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.dark_theme_secondary_text_color));
                        tvTypeAgency.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.dark_theme_secondary_text_color));
                        infoAgency.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.dark_theme_secondary_text_color));
                        wikiAgency.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.dark_theme_secondary_text_color));
                    }
                }
                View margin_blank = layoutInflater.inflate(R.layout.margin_blank, viewToInsertLayouts, false);
                viewToInsertLayouts.addView(margin_blank);
            }
        }
    }


    public void showToast() {
        Toast.makeText(getActivity().getApplicationContext(), "Link not available", Toast.LENGTH_SHORT).show();
    }
}
