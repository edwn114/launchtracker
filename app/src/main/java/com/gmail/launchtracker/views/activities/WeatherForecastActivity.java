package com.gmail.launchtracker.views.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;

import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.adapter.WeatherForecastAdapter;
import com.gmail.launchtracker.greenDao.DaoSession;
import com.gmail.launchtracker.greenDao.Forecast;
import com.gmail.launchtracker.greenDao.ForecastDao;
import com.gmail.launchtracker.models.DayOfWeek;
import com.gmail.launchtracker.mvp_presenters.WeatherForecastPresenter;
import com.gmail.launchtracker.storage.SharedPreference;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

public class WeatherForecastActivity extends AppCompatActivity {
    private WeatherForecastAdapter weatherForecastAdapter;

    private SwipeRefreshLayout swipeContainer;

    private ForecastDao forecastDao;

    private boolean dark_theme;
    private boolean firstBoot = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        configureTheme();

        super.onCreate(savedInstanceState);

        configureLayout();

        DaoSession daoSession = ((MyApplication) getApplicationContext()).getDaoSession();
        forecastDao = daoSession.getForecastDao();

        loadFromDataBase();
    }

    public void configureTheme() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        dark_theme = sharedPref.getBoolean("theme", false);

        int m_theme;

        if (dark_theme) {
            m_theme = R.style.DarkTheme;
        } else {
            m_theme = R.style.LightTheme;
        }

        setTheme(m_theme);
    }

    public void configureLayout() {
        setContentView(R.layout.activity_weather_forecast);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.weather_forecast));
        }

        LayoutInflater layoutInflater = getLayoutInflater();
        weatherForecastAdapter = new WeatherForecastAdapter(this, layoutInflater, dark_theme);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvWeatherForecast);

        if (recyclerView != null) {
            recyclerView.setLayoutManager(layoutManager);

            recyclerView.addItemDecoration(
                    new HorizontalDividerItemDecoration.Builder(this)
                            .marginResId(R.dimen.leftmargin, R.dimen.rightmargin)
                            .build());

            recyclerView.setAdapter(weatherForecastAdapter);

            recyclerView.setHasFixedSize(true);
        }

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        // Configure the refreshing colors
        if (swipeContainer != null) {
            swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);
        }

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getForecast();
            }
        });

        if (dark_theme) {

            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
    }

    public void getForecast() {

        if (firstBoot)
            swipeContainer.post(new Runnable() {
                @Override
                public void run() {
                    swipeContainer.setRefreshing(true);
                }
            });
        else
            swipeContainer.setRefreshing(true);

        new WeatherForecastPresenter(getApplicationContext(), SharedPreference.getInstance(getApplicationContext()),
                weatherForecastAdapter, swipeContainer);
    }

    public void loadFromDataBase() {
        ArrayList<Forecast> forecastList = (ArrayList<Forecast>) forecastDao.loadAll();

        if (forecastList.size() <= 0) {

            firstBoot = true;
            getForecast();
            return;
        }

        ArrayList<Object> objects = new ArrayList<>();

        int i = 0;
        int j = forecastList.size();

        DayOfWeek day_full;

        if (j != 0) {
            day_full = new DayOfWeek(forecastList.get(0).getDay_full(),
                    forecastList.get(0).getDate());

            while (true) {
                if (!forecastList.get(i).getDay_full().equals(day_full.getmDayOfWeek())) {
                    day_full.setmDayOfWeek(forecastList.get(i).getDay_full());

                    objects.add(new DayOfWeek(forecastList.get(i).getDay_full(),
                            forecastList.get(i).getDate()));
                    objects.add(forecastList.get(i));
                } else {
                    objects.add(forecastList.get(i));
                }

                i++;

                if (i >= j)
                    break;
            }
        }

        weatherForecastAdapter.clear();
        weatherForecastAdapter.setObjects(objects);
    }

    @Override
    public void onResume() {
        super.onResume();

        MyApplication.getInstance().trackScreenView("WeatherForecast Activity");
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("POS", 1);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("POS", 1);
        startActivity(intent);

    }
}

