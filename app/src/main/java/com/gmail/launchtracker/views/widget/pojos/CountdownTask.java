package com.gmail.launchtracker.views.widget.pojos;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.widget.RemoteViews;

import com.gmail.launchtracker.R;
import com.gmail.launchtracker.storage.SharedPreference;

/**
 * Created by edwn112 on 3/24/16.
 */
public class CountdownTask {

    private final Context m_context;
    private final RemoteViews m_views;
    private final int m_widgetId;

    private CountDownTimer countDownTimer;

    private Handler handler;

    private boolean stop = false;

    private SharedPreference sharedPreference;

    public CountdownTask(Context context, RemoteViews views, int widgetId) {
        m_context = context;
        m_views = views;
        m_widgetId = widgetId;

        sharedPreference = SharedPreference.getInstance(context);

        handler = new Handler();
    }

    public void start(long timeLeft) {

        m_views.setTextViewText(R.id.tvTimeLeftWidget, "-");

        countDownTimer = new CountDownTimer(timeLeft, 1000) {

            private int lastDay = -1;
            private int lastHour = -1;
            private int lastMinute = -1;
            private int lastSecond = -1;

            public void onTick(long millisUntilFinished) {

                int days = (int) millisUntilFinished / (24 * 3600000);
                int hours = (int) millisUntilFinished / (3600000) % 24;
                int minutes = (int) millisUntilFinished / (60000) % 60;
                int seconds = (int) millisUntilFinished / 1000 % 60;

                if (days != lastDay)
                    m_views.setTextViewText(R.id.tvDayWidget, String.format("%d", days));

                if (hours != lastHour)
                    m_views.setTextViewText(R.id.tvHrWidget, String.format("%d", hours));

                if (minutes != lastMinute)
                    m_views.setTextViewText(R.id.tvMinWidget, String.format("%d", minutes));

                if (seconds != lastSecond)
                    m_views.setTextViewText(R.id.tvSecWidget, String.format("%d", seconds));

                lastDay = days;
                lastHour = hours;
                lastMinute = minutes;
                lastSecond = seconds;

                AppWidgetManager.getInstance(m_context).updateAppWidget(m_widgetId, m_views);
            }

            public void onFinish() {

                m_views.setTextViewText(R.id.tvTimeLeftWidget, "+");

                long temp = 0;

                for (int i = 0; i < sharedPreference.getUpcomingLaunches().size(); i++) {
                    if (sharedPreference.getUpcomingLaunches().get(i).getNet() != 0) {

                        temp = sharedPreference.getUpcomingLaunches().get(i).getNet();
                        break;
                    }
                }

                final long netTime = temp * 1000;

                new Thread() {
                    private int lastDay = -1;
                    private int lastHour = -1;
                    private int lastMinute = -1;
                    private int lastSecond = -1;

                    private long lastRemainingTime = netTime;

                    public void run() {

                        while (!stop) {
                            try {
                                Thread.sleep(1000);
                            } catch (Exception ignored) {
                            }

                            final long time = System.currentTimeMillis() - lastRemainingTime;

                            handler.post(new Runnable() {

                                @Override
                                public void run() {
                                    int days = (int) time / (24 * 3600000);
                                    int hours = (int) time / (3600000) % 24;
                                    int minutes = (int) time / (60000) % 60;
                                    int seconds = (int) time / 1000 % 60;

                                    if (days != lastDay)
                                        m_views.setTextViewText(R.id.tvDayWidget,
                                                String.format("%d", days));

                                    if (hours != lastHour)
                                        m_views.setTextViewText(R.id.tvHrWidget,
                                                String.format("%d", hours));

                                    if (minutes != lastMinute)
                                        m_views.setTextViewText(R.id.tvMinWidget,
                                                String.format("%d", minutes));

                                    if (seconds != lastSecond)
                                        m_views.setTextViewText(R.id.tvSecWidget,
                                                String.format("%d", seconds));

                                    lastDay = days;
                                    lastHour = hours;
                                    lastMinute = minutes;
                                    lastSecond = seconds;

                                    AppWidgetManager.getInstance(m_context)
                                            .updateAppWidget(m_widgetId, m_views);
                                }
                            });
                        }
                    }
                }.start();
            }
        };

        countDownTimer.start();
    }

    public void stop() {
        if (countDownTimer != null)
            countDownTimer.cancel();

        stop = true;
    }
}
