package com.gmail.launchtracker.views.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.adapter.UpcomingLaunchAdapter;
import com.gmail.launchtracker.models.RocketLaunch;
import com.gmail.launchtracker.storage.SharedPreference;
import com.gmail.launchtracker.views.activities.FilterUpcomingLaunchesActivity;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class UpcomingLaunchesFragment extends Fragment implements SearchView.OnQueryTextListener {
    private SharedPreference sharedPreference;

    private UpcomingLaunchAdapter adapterLocal, adapterGlobal;

    private RecyclerView rvLaunches;

    private boolean isLocal;
    private boolean dark_theme;
    private String filterTimeRange, filterLocation, filterRocketName;

    private ArrayList<RocketLaunch> rocketLaunches;

    private Toolbar toolbar;

    public UpcomingLaunchesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() != null) {
            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        dark_theme = sharedPref.getBoolean("theme", false);

        isLocal = sharedPref.getBoolean("localTime", true);

        boolean isSubTitleTime = sharedPref.getBoolean("subTitle", false);

        sharedPreference = SharedPreference.getInstance(getContext());

        filterTimeRange = getResources().getStringArray(R.array.filter_array)
                [sharedPreference.getUpcomingTimeRangePosition()].toLowerCase();

        if (sharedPreference.getUpcomingLocationPosition() != 0)
            filterLocation = getResources().getStringArray(R.array.locations_value)
                    [sharedPreference.getUpcomingLocationPosition()].toLowerCase();
        else
            filterLocation = null;

        if (sharedPreference.getUpcomingRocketPosition() != 0)
            filterRocketName = getResources().getStringArray(R.array.rocket_values)
                    [sharedPreference.getUpcomingRocketPosition()].toLowerCase();
        else
            filterRocketName = null;

        adapterLocal = new UpcomingLaunchAdapter((AppCompatActivity) getActivity());
        adapterGlobal = new UpcomingLaunchAdapter((AppCompatActivity) getActivity());

        adapterLocal.setLocalTime(true);
        adapterGlobal.setLocalTime(false);

        if (isSubTitleTime) {
            adapterLocal.setSubTitleTime(true);
            adapterGlobal.setSubTitleTime(true);
        } else {
            adapterLocal.setSubTitleTime(false);
            adapterGlobal.setSubTitleTime(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming_launches, container, false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        rvLaunches = (RecyclerView) view.findViewById(R.id.rvLaunches);
        rvLaunches.setLayoutManager(layoutManager);

        rvLaunches.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getContext())
                        .marginResId(R.dimen.leftmargin, R.dimen.rightmargin)
                        .build());
        if (isLocal)
            rvLaunches.setAdapter(adapterLocal);
        else
            rvLaunches.setAdapter(adapterGlobal);

        rvLaunches.setHasFixedSize(true);

        displayLaunches();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        MyApplication.getInstance().trackScreenView("UpcomingLaunches Fragment");
    }

    public void displayLaunches() {
        rocketLaunches = sharedPreference.getUpcomingLaunches();

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);

        try {
            if (filterTimeRange != null) {
                if (filterTimeRange.equals("This Week".toLowerCase())) {

                    toolbar.setTitle(getText(R.string.this_week));

                    calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
                    calendar.add(Calendar.WEEK_OF_YEAR, 1);

                    long next_week = calendar.getTimeInMillis();

                    ArrayList<RocketLaunch> arrayList = new ArrayList<>();

                    for (int i = 0; i < rocketLaunches.size(); i++) {
                        if (rocketLaunches.get(i).getNet() * 1000
                                < next_week && rocketLaunches.get(i).getNet() != 0)
                            arrayList.add(rocketLaunches.get(i));
                    }

                    filterData(arrayList);
                } else if (filterTimeRange.equals("Next Week".toLowerCase())) {
                    toolbar.setTitle(getText(R.string.next_week));

                    calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
                    calendar.add(Calendar.WEEK_OF_YEAR, 1);

                    long next_week = calendar.getTimeInMillis();

                    calendar.add(Calendar.WEEK_OF_YEAR, 1);

                    long next_next_week = calendar.getTimeInMillis();

                    ArrayList<RocketLaunch> arrayList = new ArrayList<>();

                    for (int i = 0; i < rocketLaunches.size(); i++) {
                        if (rocketLaunches.get(i).getNet() * 1000
                                > next_week && rocketLaunches.get(i).getNet()
                                * 1000 < next_next_week && rocketLaunches.get(i).getNet() != 0)
                            arrayList.add(rocketLaunches.get(i));
                    }

                    filterData(arrayList);
                } else if (filterTimeRange.equals("This Month".toLowerCase())) {

                    toolbar.setTitle(getText(R.string.this_month));

                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                    calendar.add(Calendar.MONTH, 1);

                    long next_month = calendar.getTimeInMillis();
                    ArrayList<RocketLaunch> arrayList = new ArrayList<>();

                    for (int i = 0; i < rocketLaunches.size(); i++) {
                        if (rocketLaunches.get(i).getNet() * 1000
                                < next_month && rocketLaunches.get(i).getNet() != 0)
                            arrayList.add(rocketLaunches.get(i));
                    }

                    filterData(arrayList);
                } else if (filterTimeRange.equals("Next Month".toLowerCase())) {

                    toolbar.setTitle(getText(R.string.next_month));

                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                    calendar.add(Calendar.MONTH, 1);

                    long next_month = calendar.getTimeInMillis();

                    calendar.add(Calendar.MONTH, 1);

                    long next_next_month = calendar.getTimeInMillis();

                    ArrayList<RocketLaunch> arrayList = new ArrayList<>();

                    for (int i = 0; i < rocketLaunches.size(); i++) {
                        if (rocketLaunches.get(i).getNet() * 1000
                                > next_month && rocketLaunches.get(i).getNet()
                                * 1000 < next_next_month && rocketLaunches.get(i).getNet() != 0)
                            arrayList.add(rocketLaunches.get(i));
                    }

                    filterData(arrayList);
                } else if (filterTimeRange.equals("All".toLowerCase())) {

                    toolbar.setTitle(getText(R.string.all));

                    filterData(rocketLaunches);
                }
            }
        } catch (NullPointerException ignored) {

        }
    }

    public void filterData(ArrayList<RocketLaunch> rocketLaunchList) {
        String text_to_filter = sharedPreference.getPrefsUpcomingFilterText().toLowerCase();

        final ArrayList<RocketLaunch> filteredData = new ArrayList<>();
        final ArrayList<RocketLaunch> filteredLocation = new ArrayList<>();
        final ArrayList<RocketLaunch> filteredRocketName = new ArrayList<>();

        for (RocketLaunch rocketLaunch : rocketLaunchList) {
            final String launch_name = rocketLaunch.getName().toLowerCase();
            final String location_name = rocketLaunch.getLocation().toLowerCase();
            if (launch_name.contains(text_to_filter)
                    || location_name.contains(text_to_filter)) {
                filteredData.add(rocketLaunch);
            }
        }

        if (filterLocation != null) {
            if (filterLocation.equals("All".toLowerCase()))
                filteredLocation.addAll(filteredData);
            else {
                for (int i = 0; i < filteredData.size(); i++) {
                    if (filteredData.get(i).getLocation().toLowerCase().contains(filterLocation))
                        filteredLocation.add(filteredData.get(i));
                }
            }
        } else
            filteredLocation.addAll(filteredData);

        if (filterRocketName != null) {
            if (filterRocketName.equals("All".toLowerCase()))
                filteredRocketName.addAll(filteredLocation);
            else {
                for (int i = 0; i < filteredLocation.size(); i++) {
                    if (filteredLocation.get(i).getName().toLowerCase().contains(filterRocketName))
                        filteredRocketName.add(filteredLocation.get(i));
                }
            }
        } else
            filteredRocketName.addAll(filteredLocation);

        if (isLocal) {
            adapterLocal.clear();
            adapterLocal.setRockets(filteredRocketName);
        } else {
            adapterGlobal.clear();
            adapterGlobal.setRockets(filteredRocketName);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        if (dark_theme)
            inflater.inflate(R.menu.menu_upcoming_fragment_dark_theme, menu);
        else
            inflater.inflate(R.menu.menu_upcoming_fragment_light_theme, menu);

        final MenuItem item_search = menu.findItem(R.id.action_search);
        final MenuItem item_filter = menu.findItem(R.id.action_filter);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item_search);

        searchView.setOnQueryTextListener(this);

        item_filter.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Intent intent = new Intent(getActivity(), FilterUpcomingLaunchesActivity.class);
                startActivity(intent);

                return true;
            }
        });
    }

    @Override
    public boolean onQueryTextChange(String query) {
        // Here is where we are going to implement our filter logic

        query = query.toLowerCase();

        final List<RocketLaunch> filteredModelList = new ArrayList<>();

        for (RocketLaunch rocketLaunch : rocketLaunches) {
            final String launch_name = rocketLaunch.getName().toLowerCase();
            final String location_name = rocketLaunch.getLocation().toLowerCase();

            if (launch_name.contains(query)
                    || location_name.contains(query)) {
                filteredModelList.add(rocketLaunch);
            }
        }

        if (isLocal)
            adapterLocal.animateTo(filteredModelList);
        else
            adapterGlobal.animateTo(filteredModelList);

        rvLaunches.scrollToPosition(0);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }
}
