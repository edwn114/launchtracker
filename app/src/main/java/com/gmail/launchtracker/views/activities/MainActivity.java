package com.gmail.launchtracker.views.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.views.fragments.NextLaunchFragment;
import com.gmail.launchtracker.views.fragments.PreviousLaunchesFragment;
import com.gmail.launchtracker.views.fragments.UpcomingLaunchesFragment;
import com.gmail.launchtracker.receiver.BootReceiver;
import com.squareup.leakcanary.RefWatcher;

import java.util.Calendar;

import hotchemi.android.rate.AppRate;

import static com.gmail.launchtracker.R.color;
import static com.gmail.launchtracker.R.id;
import static com.gmail.launchtracker.R.layout;
import static com.gmail.launchtracker.R.string;
import static com.gmail.launchtracker.R.style;
import static com.gmail.launchtracker.R.xml;

public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {
    private Context context;

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;

    static Calendar calendar;

    private SharedPreferences sharedPref;

    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        boolean dark_theme = sharedPref.getBoolean("theme", false);

        int m_theme;

        if (dark_theme) {
            m_theme = style.DarkTheme;
        } else {
            m_theme = style.LightTheme;
        }

        setTheme(m_theme);

        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main);

        //Rate the app
        AppRate.with(this)
                .setInstallDays(0) // default 10, 0 means install day.
                .setLaunchTimes(3) // default 10
                .setRemindInterval(2) // default 1
                .setShowLaterButton(true) // default true
                .setDebug(false) // default false
                .monitor();

        // Show a dialog if meets conditions
        AppRate.showRateDialogIfMeetsConditions(this);

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(id.toolbar);
        toolbar.setTitle(getString(string.app_name));

        setSupportActionBar(toolbar);

        PreferenceManager.setDefaultValues(this, xml.settings_dark_theme, false);

        context = getApplicationContext();

        ActionBar mActionBar = getSupportActionBar();

        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
        }

        int pos = getIntent().getIntExtra("POS", 1);

        fragmentManager = getSupportFragmentManager();
        swapFragments(pos);

        calendar = Calendar.getInstance();

        PreferenceManager.setDefaultValues(this, xml.settings_dark_theme, false);

        // Find our drawer view
        mDrawer = (DrawerLayout) findViewById(id.drawer_layout);
        drawerToggle = setupDrawerToggle();

        drawerToggle.setDrawerIndicatorEnabled(true);
        mDrawer.addDrawerListener(drawerToggle);

        // Find our drawer view
        NavigationView nvDrawer = (NavigationView) findViewById(id.nvView);

        // Setup drawer view
        setupDrawerContent(nvDrawer);

        TextView tvTitle = (TextView) findViewById(id.tvTitle);

        if (dark_theme) {
            if (nvDrawer != null) {
                nvDrawer.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),
                        color.dark_theme_background));

                nvDrawer.setItemTextColor(ContextCompat.getColorStateList(getApplicationContext(),
                        color.dark_theme_state_list));
                nvDrawer.setItemIconTintList(ContextCompat.getColorStateList(getApplicationContext(),
                        color.dark_theme_state_list));
            }

            if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        color.dark_theme_primary_text_color));
        } else {

            if (nvDrawer != null) {
                nvDrawer.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),
                        color.light_theme_background));

                nvDrawer.setItemTextColor(ContextCompat.getColorStateList(getApplicationContext(),
                        color.light_theme_state_list));
                nvDrawer.setItemIconTintList(ContextCompat.getColorStateList(getApplicationContext(),
                        color.light_theme_state_list));
            }

            if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        color.light_theme_primary_text_color));

            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp);

            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
    }

    public void swapFragments(int pos) {
        Fragment fragment = null;

        Class fragmentClass = null;

        if (pos == 1)
            fragmentClass = NextLaunchFragment.class;
        else if (pos == 2)
            fragmentClass = UpcomingLaunchesFragment.class;
        else if (pos == 3)
            fragmentClass = PreviousLaunchesFragment.class;
        else
            fragmentClass = NextLaunchFragment.class;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception ignored) {
        }

        fragmentManager.beginTransaction().replace(id.flContent, fragment).commit();
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, string.drawer_open, string.drawer_closed);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(

                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position
        Fragment fragment = null;

        Class fragmentClass = null;
        switch (menuItem.getItemId()) {
            case id.nav_first_fragment:
                fragmentClass = NextLaunchFragment.class;
                break;
            case id.nav_second_fragment:
                fragmentClass = UpcomingLaunchesFragment.class;
                break;
            case id.nav_third_fragment:
                fragmentClass = PreviousLaunchesFragment.class;
                break;
            case id.nav_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return;
            default:
                fragmentClass = NextLaunchFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(id.flContent, fragment).commit();

        // Highlight the selected item, update the title, and close the drawer
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());

        mDrawer.closeDrawers();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    // Make sure this is the method with just `Bundle` as the signature
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getSharedPreferences("theme_changed", MODE_PRIVATE).getBoolean("recreate", false)) {
            SharedPreferences.Editor editor = getSharedPreferences("theme_changed", MODE_PRIVATE).edit();
            editor.putBoolean("recreate", false);
            editor.apply();

            // To avoid : “RuntimeException: Performing pause of activity that is not resumed”

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                        finish();
                        startActivity(getIntent());
                    } else recreate();
                }
            }, 1);
        }

        boolean is_update_enabled_on_reboot = sharedPref.getBoolean("boot", false);

        if (is_update_enabled_on_reboot)
            enableBootReceiver();
        else
            disableBootReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        RefWatcher refWatcher = MyApplication.getRefWatcher(getApplicationContext());
        refWatcher.watch(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onBackStackChanged() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.getBackStackEntryCount() == 0) {
            drawerToggle.setDrawerIndicatorEnabled(true);
        } else {
            drawerToggle.setDrawerIndicatorEnabled(false);

        }
    }

    // for enabling / disabling updates when system boots
    public void enableBootReceiver() {
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public void disableBootReceiver() {
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
