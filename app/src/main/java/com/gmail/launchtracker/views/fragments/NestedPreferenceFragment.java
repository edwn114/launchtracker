package com.gmail.launchtracker.views.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;

public class NestedPreferenceFragment extends PreferenceFragment {

    public static final int NESTED_SCREEN_1_KEY = 1;
    public static final int NESTED_SCREEN_2_KEY = 2;
    public static final int NESTED_SCREEN_3_KEY = 3;
    public static final int NESTED_SCREEN_4_KEY = 4;
    public static final int NESTED_SCREEN_5_KEY = 5;

    private static final String TAG_KEY = "NESTED_KEY";

    private TextView tvTitle;

    public static NestedPreferenceFragment newInstance(int key) {
        NestedPreferenceFragment fragment = new NestedPreferenceFragment();

        // supply arguments to bundle.
        Bundle args = new Bundle();
        args.putInt(TAG_KEY, key);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getActivity() != null) {
            tvTitle = (TextView) getActivity().findViewById(R.id.title_text);
        }

        final SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getActivity());

        prefs.registerOnSharedPreferenceChangeListener(
                new SharedPreferences.OnSharedPreferenceChangeListener() {
                    @Override
                    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

                        try {
                            if (key.equals("theme")) {
                                if (getActivity() != null) {
                                    SharedPreferences themePreferences = getActivity()
                                            .getSharedPreferences("theme_changed", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor themeEditor = themePreferences.edit();

                                    themeEditor.putBoolean("recreate", true);

                                    themeEditor.apply();

                                    // To avoid : “RuntimeException:
                                    // Performing pause of activity that is not resumed”

                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                                                getActivity().finish();
                                                startActivity(getActivity().getIntent());
                                            } else
                                                getActivity().recreate();
                                        }
                                    }, 1);
                                }
                            }

                            if (key.equals("notifications")) {
                                if (!prefs.getBoolean(key, false)) {
                                    getPreferenceScreen()
                                            .findPreference("vibration").setEnabled(false);
                                    getPreferenceScreen()
                                            .findPreference("day_notification").setEnabled(false);
                                    getPreferenceScreen()
                                            .findPreference("x_hours_notification").setEnabled(false);
                                    getPreferenceScreen()
                                            .findPreference("five_min_notification").setEnabled(false);
                                    getPreferenceScreen()
                                            .findPreference("x_minutes_notification").setEnabled(false);
                                } else {
                                    getPreferenceScreen()
                                            .findPreference("vibration").setEnabled(true);
                                    getPreferenceScreen()
                                            .findPreference("day_notification").setEnabled(true);
                                    getPreferenceScreen()
                                            .findPreference("x_hours_notification").setEnabled(true);
                                    getPreferenceScreen()
                                            .findPreference("five_min_notification").setEnabled(true);
                                    getPreferenceScreen()
                                            .findPreference("x_minutes_notification").setEnabled(true);
                                }
                            }

                            if (key.equals("weather_updates")) {
                                if (!prefs.getBoolean(key, false))
                                    getPreferenceScreen()
                                            .findPreference("weather_update_frequency").setEnabled(false);
                                else
                                    getPreferenceScreen()
                                            .findPreference("weather_update_frequency").setEnabled(true);
                            }

                            if (key.equals("launch_updates")) {
                                if (!prefs.getBoolean(key, false))
                                    getPreferenceScreen()
                                            .findPreference("launch_update_frequency").setEnabled(false);
                                else
                                    getPreferenceScreen()
                                            .findPreference("launch_update_frequency").setEnabled(true);
                            }

                            if (key.equals("images")) {
                                if (!prefs.getBoolean(key, false))
                                    getPreferenceScreen()
                                            .findPreference("images_use_palette").setEnabled(false);
                                else
                                    getPreferenceScreen()
                                            .findPreference("images_use_palette").setEnabled(true);
                            }
                        } catch (NullPointerException ignored) {
                        }
                    }
                }
        );

        checkPreferenceResource();
    }

    @Override
    public void onResume() {
        super.onResume();

        MyApplication.getInstance().trackScreenView("NestedPreference Fragment");
    }

    private void checkPreferenceResource() {
        int key = getArguments().getInt(TAG_KEY);

        // Load the preferences from an XML resource

        switch (key) {
            case NESTED_SCREEN_1_KEY:
                addPreferencesFromResource(R.xml.nested_screen1_preferences);

                if (tvTitle != null)
                    tvTitle.setText(getText(R.string.appearance_title));

                break;

            case NESTED_SCREEN_2_KEY:
                addPreferencesFromResource(R.xml.nested_screen2_preferences);

                if (tvTitle != null)
                    tvTitle.setText(getText(R.string.notifications_title));

                break;

            case NESTED_SCREEN_3_KEY:
                addPreferencesFromResource(R.xml.nested_screen3_preferences);

                if (tvTitle != null)
                    tvTitle.setText(getString(R.string.updates_title));

                break;

            case NESTED_SCREEN_4_KEY:
                addPreferencesFromResource(R.xml.nested_screen4_preferences);

                if (tvTitle != null)
                    tvTitle.setText(getString(R.string.more_title));

                break;

            case NESTED_SCREEN_5_KEY:
                addPreferencesFromResource(R.xml.nested_screen5_preferences);

                if (tvTitle != null)
                    tvTitle.setText(getString(R.string.other_title));

                break;

            default:
                break;
        }
    }
}