package com.gmail.launchtracker.views.activities;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.gmail.launchtracker.R;
import com.gmail.launchtracker.views.fragments.NestedPreferenceFragment;
import com.gmail.launchtracker.views.fragments.SettingsFragment;

/**
 * Created by edwn112 on 06-10-2015.
 */

public class SettingsActivity extends AppCompatActivity implements SettingsFragment.Callback {

    private static final String TAG_NESTED = "TAG_NESTED";
    private TextView tvTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean dark_theme = sharedPref.getBoolean("theme", false);

        int m_theme;

        if (dark_theme) {
            m_theme = R.style.DarkTheme;
        } else {
            m_theme = R.style.LightTheme;
        }

        setTheme(m_theme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        tvTitle = (TextView) findViewById(R.id.title_text);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }

        if (tvTitle != null)
            tvTitle.setText(getText(R.string.settings));

        Fragment fragment = null;

        Class fragmentClass;

        fragmentClass = SettingsFragment.class;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception ignored) {
        }

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.content_frame, fragment)
                    .commit();
        }

        if (!dark_theme) {
            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

            if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        R.color.light_theme_primary_text_color));
        } else {
            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

            if (tvTitle != null)
                tvTitle.setTextColor(ContextCompat.getColor(getApplicationContext(),
                        R.color.dark_theme_primary_text_color));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if (getFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else if (getFragmentManager().getBackStackEntryCount() == 1) {
                getFragmentManager().popBackStack();

                if (tvTitle != null)
                    tvTitle.setText(getString(R.string.settings));
            } else {
                getFragmentManager().popBackStack();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // this if statement is necessary to navigate through nested and main fragments
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else if (getFragmentManager().getBackStackEntryCount() == 1) {
            getFragmentManager().popBackStack();

            if (tvTitle != null)
                tvTitle.setText(getString(R.string.settings));
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onNestedPreferenceSelected(int key) {
        getFragmentManager().beginTransaction().replace(R.id.content_frame,
                NestedPreferenceFragment.newInstance(key), TAG_NESTED).addToBackStack(TAG_NESTED).commit();
    }
}