package com.gmail.launchtracker.views.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.gmail.launchtracker.storage.SharedPreference;

import java.util.List;

public class LinkDialogFragment extends DialogFragment {

    private SharedPreference sharedPreference;
    private int pos;

    public LinkDialogFragment() {

        // Required empty public constructor

        sharedPreference = SharedPreference.getInstance(getActivity());
        for (int i = 0; i < sharedPreference.getUpcomingLaunches().size(); i++) {
            if (sharedPreference.getUpcomingLaunches().get(i).getNet() != 0) {
                pos = i;
                break;
            }
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final List<String> links = sharedPreference.getLaunchesUpcoming().get(pos).getVidURLs();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Links")
                .setItems(links.toArray(new String[links.size()]), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item

                        String url = links.get(which).replace("\\", "").toLowerCase();

                        if (url.contains("http") || url.contains("https")) {
                            Uri uri = Uri.parse(links.get(which).replace("\\", ""));

                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        } else {
                            showToast();
                        }
                    }
                });

        return builder.create();
    }

    public void showToast() {
        Toast.makeText(getActivity(),
                "Link not available", Toast.LENGTH_SHORT).show();
    }

}
