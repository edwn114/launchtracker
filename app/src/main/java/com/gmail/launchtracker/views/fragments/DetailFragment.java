package com.gmail.launchtracker.views.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.RocketLaunch;
import com.gmail.launchtracker.storage.SharedPreference;

import java.util.Arrays;
import java.util.List;

public class DetailFragment extends Fragment {

    private String lat_long;
    private String base_map_url = "https://www.google.com/maps/?q=";

    private TextView tvDate;
    private TextView tvWindowStart;
    private TextView tvWindowEnd;
    private TextView tvNet;
    private TextView tvRocket;
    private TextView tvVehicle;
    private TextView tvPayload;
    private TextView tvLocationName;
    private ImageView imMap;

    private CardView dateView;
    private CardView summaryView;
    private CardView locationView;

    private String vehicle = "";
    private String payload = "";

    private RocketLaunch rocketLaunch;

    private Boolean is_upcoming;

    public DetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreference sharedPreference = SharedPreference.getInstance(getContext());

        int pos = sharedPreference.getCurrentPosition();

        is_upcoming = getArguments().getBoolean("IS_UPCOMING");

        try {
            if (is_upcoming)
                rocketLaunch = sharedPreference.getUpcomingLaunches().get(pos);
            else
                rocketLaunch = sharedPreference.getPreviousLaunches().get(pos);

            String name_to_split = rocketLaunch.getName();


            String[] parts = name_to_split.split(" ");

            List<String> ar = Arrays.asList(parts);

            int getPosOFl = 0;

            for (int i = 0; i < ar.size(); i++) {
                if (ar.get(i).equals("|")) {
                    getPosOFl = i;
                    break;
                }
            }

            for (int j = 0; j < ar.size(); j++) {

                if (j < getPosOFl) {
                    vehicle += " " + ar.get(j);
                } else if (j > getPosOFl) {
                    payload += " " + ar.get(j);
                }
            }
        } catch (NullPointerException ignored) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = null;

        try {
            if (is_upcoming)
                view = inflater.inflate(R.layout.fragment_detail_upcoming, container, false);
            else {
                view = inflater.inflate(R.layout.fragment_detail_previous, container, false);
                TextView tvStatus = (TextView) view.findViewById(R.id.tvStatus);

                tvStatus.setText(Constants.getStatusType(rocketLaunch.getStatus()));
            }

            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvWindowStart = (TextView) view.findViewById(R.id.tvWindowStart);
            tvWindowEnd = (TextView) view.findViewById(R.id.tvWindowEnd);
            tvNet = (TextView) view.findViewById(R.id.tvNet);
            tvRocket = (TextView) view.findViewById(R.id.tvRocket);
            tvVehicle = (TextView) view.findViewById(R.id.tvVechile);
            tvPayload = (TextView) view.findViewById(R.id.tvPayLoad);
            tvLocationName = (TextView) view.findViewById(R.id.tvLocationName);
            imMap = (ImageView) view.findViewById(R.id.imMap);

            dateView = (CardView) view.findViewById(R.id.dateView);
            summaryView = (CardView) view.findViewById(R.id.summaryView);
            locationView = (CardView) view.findViewById(R.id.locationView);

            initialise();

            locationView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lat_long = rocketLaunch.getLatitude() + "," + rocketLaunch.getLongitude();
                    final String map_url = base_map_url + lat_long;

                    Uri uri = Uri.parse(map_url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });

            locationView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });
        } catch (NullPointerException ignored) {

        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Detail Fragment");
    }

    public void initialise() {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean dark_theme = sharedPref.getBoolean("theme", false);

        // for dark mode
        if (dark_theme) {
            dateView.setCardBackgroundColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_card));
            summaryView.setCardBackgroundColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_card));
            locationView.setCardBackgroundColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_card));

            tvDate.setTextColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_primary_text_color));
            tvWindowStart.setTextColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_secondary_text_color));
            tvWindowEnd.setTextColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_secondary_text_color));
            tvNet.setTextColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_secondary_text_color));

            tvRocket.setTextColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_primary_text_color));
            tvVehicle.setTextColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_secondary_text_color));
            tvPayload.setTextColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_secondary_text_color));

            tvLocationName.setTextColor(ContextCompat.getColor(getActivity(),
                    R.color.dark_theme_primary_text_color));
        }

        try {
            if (sharedPref.getBoolean("localTime", true)) {
                tvDate.setText(rocketLaunch.getLocal_time_without_hours());

                tvWindowStart.setText(String.format("Window Start : %s",
                        rocketLaunch.getLocal_window_start()));
                tvWindowEnd.setText(String.format("Window End : %s",
                        rocketLaunch.getLocal_window_end()));
                tvNet.setText(String.format("Current T - 0 : %s",
                        rocketLaunch.getLocal_net()));
            } else {
                tvDate.setText(rocketLaunch.getGlobal_time_without_hours());

                tvWindowStart.setText(String.format("Window Start : %s",
                        rocketLaunch.getGlobal_window_start()));
                tvWindowEnd.setText(String.format("Window End : %s",
                        rocketLaunch.getGlobal_window_end()));
                tvNet.setText(String.format("Current T - 0 : %s",
                        rocketLaunch.getGlobal_net()));
            }

            tvVehicle.setText(String.format("Vehicle :%s", vehicle));
            tvPayload.setText(String.format("PayLoad :%s", payload));

            tvLocationName.setText(rocketLaunch.getLocation());

            lat_long = rocketLaunch.getLatitude() + "," + rocketLaunch.getLongitude();
        } catch (NullPointerException ignored) {

        }

        String map_type = sharedPref.getString("map_type", "hybrid");

        String base_url = "https://maps.googleapis.com/maps/api/staticmap?center=";
        String middle_url = "&zoom=5&size=600x400&scale=1&maptype=" + map_type +
                "&markers=size:mid%7Ccolor:red%7C";
        String end_url = "&key=";

        final String static_map = base_url + lat_long + middle_url + lat_long + end_url + Constants.KEY;

        if (getActivity() != null) {
            Glide.with(this).load(static_map).into(imMap);
        }
    }
}
