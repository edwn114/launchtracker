package com.gmail.launchtracker.views.widget.services;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.widget.RemoteViews;

import com.gmail.launchtracker.utilities.TimerMinus;
import com.gmail.launchtracker.storage.SharedPreference;
import com.gmail.launchtracker.views.widget.pojos.CountdownTask;
import com.gmail.launchtracker.views.widget.receivers.WidgetProvider;

public class TimerService extends Service {

    private SharedPreference sharedPreference;

    private BroadcastReceiver m_receiver;

    private CountdownTask m_countdownTask;
    private RemoteViews m_views;
    private AppWidgetManager appWidgetManager;
    private ComponentName thisAppWidget;

    private TimerMinus timer;

    public TimerService() {
    }

    @Override
    public void onCreate() {
        timer = new TimerMinus();

        sharedPreference = SharedPreference.getInstance(this);

        m_views = WidgetProvider.getRemoteView(this);
        appWidgetManager = AppWidgetManager.getInstance(this);
        thisAppWidget = new ComponentName(this, WidgetProvider.class);

        IntentFilter filter = new IntentFilter();

        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);

        m_receiver = new ScreenBroadcastReceiver();
        registerReceiver(m_receiver, filter);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(m_receiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        stopCountdownTask();
        startCountdownTask();

        return START_STICKY;
    }

    private void startCountdownTask() {

        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);

        for (int i = 0; i < sharedPreference.getUpcomingLaunches().size(); i++) {
            if (sharedPreference.getUpcomingLaunches().get(i).getNet() != 0) {
                timer.setLaunchTime(sharedPreference.getUpcomingLaunches().get(i).getNet());
                break;
            }
        }

        for (int appWidgetId : appWidgetIds) {
            m_countdownTask = new CountdownTask(this, m_views, appWidgetId);

            long timeLeft = timer.timeLeft();

            m_countdownTask.start(timeLeft);
        }
    }

    private void stopCountdownTask() {
        if (m_countdownTask != null)
            m_countdownTask.stop();

        m_countdownTask = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("This service cannot be bound.");
    }

    private class ScreenBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                startCountdownTask();
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                stopCountdownTask();
            }
        }
    }
}
