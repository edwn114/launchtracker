package com.gmail.launchtracker.views.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.rocket.Agency;
import com.gmail.launchtracker.models.rocket.Pad;
import com.gmail.launchtracker.models.rocket.Rocket;
import com.gmail.launchtracker.storage.SharedPreference;

import java.util.List;

public class RocketFragment extends Fragment {

    private String lat_long;
    private String base_map_url = "https://www.google.com/maps/?q=";

    private SharedPreference sharedPreference;

    int pos;

    private boolean dark_theme;

    public RocketFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreference = SharedPreference.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_rocket, container, false);

        pos = sharedPreference.getCurrentPosition();

        boolean is_upcoming = getArguments().getBoolean("IS_UPCOMING");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        dark_theme = sharedPref.getBoolean("theme", false);

        if (is_upcoming)
            display_upcoming(view);

        else
            display_previous(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Rocket Fragment");
    }

    private void display_upcoming(View view) {

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        LinearLayout viewToInsertLayouts = (LinearLayout) view.findViewById(R.id.layoutToInflate);

        try {
            if (sharedPreference.getRocketPositionsUpcoming().get(pos).getChecked()) {
                final Rocket rocket = sharedPreference.getDetailRocketsUpcoming().get(pos);

                View title = layoutInflater.inflate(
                        R.layout.fragment_rocket_title, viewToInsertLayouts, false);
                viewToInsertLayouts.addView(title);

                if (dark_theme) {
                    TextView tvRocketTitle = (TextView) title.findViewById(R.id.tvRocketTitle);
                    tvRocketTitle.setTextColor(ContextCompat.getColor(getActivity(),
                            R.color.dark_theme_primary_text_color));
                }

                View heading_layout = layoutInflater.inflate(
                        R.layout.rocket_heading_layout, viewToInsertLayouts, false);
                viewToInsertLayouts.addView(heading_layout);

                TextView name = (TextView) heading_layout.findViewById(R.id.tvName);
                name.setText(rocket.getName());

                TextView configuration = (TextView) heading_layout.findViewById(R.id.tvConfiguration);
                configuration.setText(String.format("Configuration : %s", rocket.getConfiguration()));

                TextView familyName = (TextView) heading_layout.findViewById(R.id.tvFamilyName);
                familyName.setText(String.format("Family : %s", rocket.getFamily().getName()));

                Button info = (Button) heading_layout.findViewById(R.id.btInfo);

                info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (rocket.getInfoURL().length() < 5)
                                showToast();
                            else {
                                Uri uri = Uri.parse(rocket.getInfoURL());
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        } catch (NullPointerException e) {
                            showToast();
                        }
                    }
                });

                Button wiki = (Button) heading_layout.findViewById(R.id.btWiki);

                wiki.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (rocket.getWikiURL().length() < 5)
                                showToast();
                            else {
                                Uri uri = Uri.parse(rocket.getWikiURL());
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        } catch (NullPointerException e) {
                            showToast();
                        }
                    }
                });

                if (dark_theme) {
                    CardView headingCardView =
                            (CardView) heading_layout.findViewById(R.id.rocket_heading);
                    headingCardView.setCardBackgroundColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_card));

                    name.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_primary_text_color));
                    configuration.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_secondary_text_color));
                    familyName.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_secondary_text_color));
                    info.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_secondary_text_color));
                    wiki.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_secondary_text_color));
                }

                if (sharedPreference.getAgencyPositionsUpcoming().get(pos).getChecked()) {
                    List<Agency> agencyList = sharedPreference.getAgenciesUpcoming().get(pos);

                    View agencyTitle = layoutInflater.inflate(
                            R.layout.fragment_rocket_agency, viewToInsertLayouts, false);
                    viewToInsertLayouts.addView(agencyTitle);

                    if (dark_theme) {
                        TextView tvRocketAgency = (TextView) agencyTitle.findViewById(R.id.tvRocketAgency);
                        tvRocketAgency.setTextColor(
                                ContextCompat.getColor(getActivity(), R.color.dark_theme_primary_text_color));
                    }

                    for (int i = 0; i < agencyList.size(); i++) {
                        final Agency agency = agencyList.get(i);

                        View agency_layout = layoutInflater.inflate(
                                R.layout.rocket_agency_layout, viewToInsertLayouts, false);
                        viewToInsertLayouts.addView(agency_layout);

                        TextView tvName = (TextView) agency_layout.findViewById(R.id.tvName);
                        tvName.setText(agency.getName());

                        TextView tvCountryCode = (TextView) agency_layout.findViewById(R.id.tvCountryCode);
                        tvCountryCode.setText(String.format("CountryCode : %s", agency.getCountryCode()));

                        TextView tvType = (TextView) agency_layout.findViewById(R.id.tvType);
                        tvType.setText(String.format("Type : %s", Constants.getAgencyType(agency.getType())));

                        Button infoAgency = (Button) agency_layout.findViewById(R.id.btInfo);

                        infoAgency.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (agency.getInfoURL().length() < 5)
                                    showToast();
                                else {
                                    Uri uri = Uri.parse(agency.getInfoURL());
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }
                        });

                        Button wikiAgency = (Button) agency_layout.findViewById(R.id.btWiki);

                        wikiAgency.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (agency.getWikiURL().length() < 5)
                                    showToast();
                                else {
                                    Uri uri = Uri.parse(agency.getWikiURL());
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }
                        });

                        if (dark_theme) {
                            CardView agencyCardView = (CardView) agency_layout.findViewById(R.id.rocket_agency);
                            agencyCardView.setCardBackgroundColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_card));

                            tvName.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_primary_text_color));
                            tvCountryCode.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                            tvType.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                            infoAgency.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                            wikiAgency.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                        }
                    }
                }

                if (sharedPreference.getPadPositionsUpcoming().get(pos).getChecked()) {
                    List<Pad> padList = sharedPreference.getPadsUpcoming().get(pos);

                    View padTitle = layoutInflater.inflate(
                            R.layout.fragment_rocket_pads, viewToInsertLayouts, false);
                    viewToInsertLayouts.addView(padTitle);

                    if (dark_theme) {
                        TextView tvRocketPad = (TextView) padTitle.findViewById(R.id.tvRocketPad);
                        tvRocketPad.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_primary_text_color));
                    }

                    for (int i = 0; i < padList.size(); i++) {
                        final Pad pad = padList.get(i);

                        View pad_layout = layoutInflater.inflate(R.layout.rocket_pad_layout, viewToInsertLayouts, false);
                        viewToInsertLayouts.addView(pad_layout);

                        CardView padCardView = (CardView) pad_layout.findViewById(R.id.rocket_pad);

                        padCardView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                lat_long = String.format("%.3f", Float.parseFloat(pad.getLatitude())) + ","
                                        + String.format("%.3f", Float.parseFloat(pad.getLongitude()));

                                final String map_url = base_map_url + lat_long;

                                Uri uri = Uri.parse(map_url);
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        });

                        TextView tvName = (TextView) pad_layout.findViewById(R.id.tvName);
                        tvName.setText(pad.getName());

                        TextView tvLatitude = (TextView) pad_layout.findViewById(R.id.tvLatitude);

                        if (pad.getLatitude() != null)
                            tvLatitude.setText(String.format("Latitude : %.3f",
                                    Float.parseFloat(pad.getLatitude())));

                        TextView tvLongitude = (TextView) pad_layout.findViewById(R.id.tvLongitude);

                        if (pad.getLongitude() != null)
                            tvLongitude.setText(String.format("Longitude : %.3f",
                                    Float.parseFloat(pad.getLongitude())));

                        Button infoPad = (Button) pad_layout.findViewById(R.id.btInfo);

                        infoPad.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (pad.getInfoURL().length() < 5)
                                    showToast();
                                else {
                                    Uri uri = Uri.parse(pad.getInfoURL());
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }
                        });

                        Button wikiPad = (Button) pad_layout.findViewById(R.id.btWiki);

                        wikiPad.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (pad.getWikiURL().length() < 5)
                                    showToast();
                                else {
                                    Uri uri = Uri.parse(pad.getWikiURL());
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }
                        });

                        if (dark_theme) {
                            padCardView.setCardBackgroundColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_card));

                            tvName.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_primary_text_color));
                            tvLatitude.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                            tvLongitude.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                            infoPad.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                            wikiPad.setTextColor(ContextCompat.getColor(
                                    getActivity(), R.color.dark_theme_secondary_text_color));
                        }
                    }
                }


                View margin_blank = layoutInflater.inflate(R.layout.margin_blank, viewToInsertLayouts, false);
                viewToInsertLayouts.addView(margin_blank);
            }
        } catch (NullPointerException ignored) {

        }
    }

    private void display_previous(View view) {

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        LinearLayout viewToInsertLayouts = (LinearLayout) view.findViewById(R.id.layoutToInflate);

        if (sharedPreference.getRocketPositionsPrevious().get(pos).getChecked()) {
            final Rocket rocket = sharedPreference.getDetailRocketsPrevious().get(pos);

            View title = layoutInflater.inflate(R.layout.fragment_rocket_title, viewToInsertLayouts, false);
            viewToInsertLayouts.addView(title);

            if (dark_theme) {
                TextView tvRocketTitle = (TextView) title.findViewById(R.id.tvRocketTitle);
                tvRocketTitle.setTextColor(ContextCompat.getColor(
                        getActivity(), R.color.dark_theme_primary_text_color));
            }

            View heading_layout = layoutInflater.inflate(
                    R.layout.rocket_heading_layout, viewToInsertLayouts, false);
            viewToInsertLayouts.addView(heading_layout);

            TextView name = (TextView) heading_layout.findViewById(R.id.tvName);
            name.setText(rocket.getName());

            TextView configuration = (TextView) heading_layout.findViewById(R.id.tvConfiguration);
            configuration.setText(String.format("Configuration : %s", rocket.getConfiguration()));

            TextView familyName = (TextView) heading_layout.findViewById(R.id.tvFamilyName);
            familyName.setText(String.format("Family : %s", rocket.getFamily().getName()));

            Button info = (Button) heading_layout.findViewById(R.id.btInfo);

            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (rocket.getInfoURL().length() < 5)
                            showToast();
                        else {
                            Uri uri = Uri.parse(rocket.getInfoURL());
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    } catch (NullPointerException e) {
                        showToast();
                    }
                }
            });

            Button wiki = (Button) heading_layout.findViewById(R.id.btWiki);

            wiki.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (rocket.getWikiURL().length() < 5)
                            showToast();
                        else {
                            Uri uri = Uri.parse(rocket.getWikiURL());
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    } catch (NullPointerException e) {
                        showToast();
                    }
                }
            });

            if (dark_theme) {
                CardView headingCardView = (CardView) heading_layout.findViewById(R.id.rocket_heading);
                headingCardView.setCardBackgroundColor(ContextCompat.getColor(
                        getActivity(), R.color.dark_theme_card));

                name.setTextColor(ContextCompat.getColor(
                        getActivity(), R.color.dark_theme_primary_text_color));
                configuration.setTextColor(ContextCompat.getColor(
                        getActivity(), R.color.dark_theme_secondary_text_color));
                familyName.setTextColor(ContextCompat.getColor(
                        getActivity(), R.color.dark_theme_secondary_text_color));
                info.setTextColor(ContextCompat.getColor(
                        getActivity(), R.color.dark_theme_secondary_text_color));
                wiki.setTextColor(ContextCompat.getColor(
                        getActivity(), R.color.dark_theme_secondary_text_color));
            }

            if (sharedPreference.getAgencyPositionsPrevious().get(pos).getChecked()) {
                List<Agency> agencyList = sharedPreference.getAgenciesPrevious().get(pos);

                View agencyTitle = layoutInflater.inflate(
                        R.layout.fragment_rocket_agency, viewToInsertLayouts, false);
                viewToInsertLayouts.addView(agencyTitle);

                if (dark_theme) {
                    TextView tvRocketAgency = (TextView) agencyTitle.findViewById(R.id.tvRocketAgency);
                    tvRocketAgency.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_primary_text_color));
                }

                for (int i = 0; i < agencyList.size(); i++) {
                    final Agency agency = agencyList.get(i);

                    View agency_layout = layoutInflater.inflate(
                            R.layout.rocket_agency_layout, viewToInsertLayouts, false);
                    viewToInsertLayouts.addView(agency_layout);

                    TextView tvName = (TextView) agency_layout.findViewById(R.id.tvName);
                    tvName.setText(agency.getName());

                    TextView tvCountryCode = (TextView) agency_layout.findViewById(R.id.tvCountryCode);
                    tvCountryCode.setText(String.format("CountryCode : %s", agency.getCountryCode()));

                    TextView tvType = (TextView) agency_layout.findViewById(R.id.tvType);
                    tvType.setText(String.format("Type : %s", Constants.getAgencyType(agency.getType())));

                    Button infoAgency = (Button) agency_layout.findViewById(R.id.btInfo);

                    infoAgency.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (agency.getInfoURL().length() < 5)
                                showToast();
                            else {
                                Uri uri = Uri.parse(agency.getInfoURL());
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        }
                    });

                    Button wikiAgency = (Button) agency_layout.findViewById(R.id.btWiki);

                    wikiAgency.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (agency.getWikiURL().length() < 5)
                                showToast();
                            else {
                                Uri uri = Uri.parse(agency.getWikiURL());
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        }
                    });

                    if (dark_theme) {
                        CardView agencyCardView = (CardView) agency_layout.findViewById(R.id.rocket_agency);
                        agencyCardView.setCardBackgroundColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_card));

                        tvName.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_primary_text_color));
                        tvCountryCode.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                        tvType.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                        infoAgency.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                        wikiAgency.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                    }
                }
            }

            if (sharedPreference.getPadPositionsPrevious().get(pos).getChecked()) {
                List<Pad> padList = sharedPreference.getPadsPrevious().get(pos);

                View padTitle = layoutInflater.inflate(
                        R.layout.fragment_rocket_pads, viewToInsertLayouts, false);
                viewToInsertLayouts.addView(padTitle);

                if (dark_theme) {
                    TextView tvRocketPad = (TextView) padTitle.findViewById(R.id.tvRocketPad);
                    tvRocketPad.setTextColor(ContextCompat.getColor(
                            getActivity(), R.color.dark_theme_primary_text_color));
                }

                for (int i = 0; i < padList.size(); i++) {
                    final Pad pad = padList.get(i);

                    View pad_layout = layoutInflater.inflate(
                            R.layout.rocket_pad_layout, viewToInsertLayouts, false);
                    viewToInsertLayouts.addView(pad_layout);

                    CardView padCardView = (CardView) pad_layout.findViewById(R.id.rocket_pad);


                    padCardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lat_long = String.format("%.3f", Float.parseFloat(pad.getLatitude())) + ","
                                    + String.format("%.3f", Float.parseFloat(pad.getLongitude()));

                            final String map_url = base_map_url + lat_long;

                            Uri uri = Uri.parse(map_url);
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    });

                    TextView tvName = (TextView) pad_layout.findViewById(R.id.tvName);
                    tvName.setText(pad.getName());

                    TextView tvLatitude = (TextView) pad_layout.findViewById(R.id.tvLatitude);
                    TextView tvLongitude = (TextView) pad_layout.findViewById(R.id.tvLongitude);

                    try {
                        tvLatitude.setText(String.format("Latitude : %.3f",
                                Float.parseFloat(pad.getLatitude())));
                        tvLongitude.setText(String.format("Longitude : %.3f",
                                Float.parseFloat(pad.getLongitude())));
                    } catch (NullPointerException ignored) {
                    }

                    Button infoPad = (Button) pad_layout.findViewById(R.id.btInfo);

                    infoPad.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (pad.getInfoURL().length() < 5)
                                showToast();
                            else {
                                Uri uri = Uri.parse(pad.getInfoURL());
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        }
                    });

                    Button wikiPad = (Button) pad_layout.findViewById(R.id.btWiki);

                    wikiPad.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (pad.getWikiURL().length() < 5)
                                showToast();
                            else {
                                Uri uri = Uri.parse(pad.getWikiURL());
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        }
                    });

                    if (dark_theme) {
                        padCardView.setCardBackgroundColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_card));

                        tvName.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_primary_text_color));
                        tvLatitude.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                        tvLongitude.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                        infoPad.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                        wikiPad.setTextColor(ContextCompat.getColor(
                                getActivity(), R.color.dark_theme_secondary_text_color));
                    }
                }
            }

            View margin_blank = layoutInflater.inflate(R.layout.margin_blank, viewToInsertLayouts, false);
            viewToInsertLayouts.addView(margin_blank);
        }
    }

    public void showToast() {
        Toast.makeText(getActivity().getApplicationContext(),
                "Link not available", Toast.LENGTH_SHORT).show();
    }
}
