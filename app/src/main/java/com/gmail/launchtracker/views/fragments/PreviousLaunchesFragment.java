package com.gmail.launchtracker.views.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.adapter.PreviousLaunchAdapter;
import com.gmail.launchtracker.mvp_presenters.PreviousLaunchesPresenter;
import com.gmail.launchtracker.network.services.GetLaunchesIntentService;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.RocketLaunch;
import com.gmail.launchtracker.storage.SharedPreference;
import com.gmail.launchtracker.views.activities.FilterPreviousLaunchesActivity;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PreviousLaunchesFragment extends Fragment
        implements DatePickerDialog.OnDateSetListener, SearchView.OnQueryTextListener {
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeRefreshLayout;

    private Unbinder unbinder;

    private String start_date;
    private String end_date;

    private SharedPreference sharedPreference;

    private RecyclerView rvLaunches;

    private PreviousLaunchAdapter adapterLocal, adapterGlobal;

    private boolean isLocal;
    private boolean dark_theme;
    private ArrayList<RocketLaunch> rocketLaunches;

    private Toolbar toolbar;

    private String filterLocation, filterRocketName;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {

                final String action = intent.getAction();

                if (Constants.ACTION_SUCCESS_PREVIOUS_LAUNCHES.equals(action)) {
                    swipeRefreshLayout.setRefreshing(false);

                    rocketLaunches.clear();
                    displayLaunches();
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_SUCCESS_PREVIOUS_LAUNCHES);

        getActivity().registerReceiver(broadcastReceiver, intentFilter);

        MyApplication.getInstance().trackScreenView("PreviousLaunches Fragment");
    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().unregisterReceiver(broadcastReceiver);
    }

    public PreviousLaunchesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        sharedPreference = SharedPreference.getInstance(getContext());

        if (getActivity() != null) {
            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            if (toolbar != null)
                toolbar.setTitle(sharedPreference.getPrefsCurrentYearRange());
        }

        isLocal = sharedPref.getBoolean("localTime", true);
        dark_theme = sharedPref.getBoolean("theme", false);

        boolean isSubTitleTime = sharedPref.getBoolean("subTitle", false);

        adapterLocal = new PreviousLaunchAdapter((AppCompatActivity) getActivity());
        adapterGlobal = new PreviousLaunchAdapter((AppCompatActivity) getActivity());

        adapterLocal.setLocalTime(true);
        adapterGlobal.setLocalTime(false);

        if (isSubTitleTime) {
            adapterLocal.setSubTitleTime(true);
            adapterGlobal.setSubTitleTime(true);
        } else {
            adapterLocal.setSubTitleTime(false);
            adapterGlobal.setSubTitleTime(false);
        }

        rocketLaunches = new ArrayList<>();

        if (sharedPreference.getPreviousLocationPosition() != 0)
            filterLocation = getResources().getStringArray(R.array.locations_value)
                    [sharedPreference.getPreviousLocationPosition()].toLowerCase();
        else
            filterLocation = null;

        if (sharedPreference.getPreviousRocketPosition() != 0)
            filterRocketName = getResources().getStringArray(R.array.rocket_values)
                    [sharedPreference.getPreviousRocketPosition()].toLowerCase();
        else
            filterRocketName = null;

        // initialize start and end date

        getDefaultDateRange();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_previous_launches, container, false);
        unbinder = ButterKnife.bind(this, view);

        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData();
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        rvLaunches = (RecyclerView) view.findViewById(R.id.rvItems);
        rvLaunches.setLayoutManager(layoutManager);

        rvLaunches.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getContext())
                        .marginResId(R.dimen.leftmargin, R.dimen.rightmargin)
                        .build());
        if (isLocal)
            rvLaunches.setAdapter(adapterLocal);
        else
            rvLaunches.setAdapter(adapterGlobal);

        rvLaunches.setHasFixedSize(true);

        final FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);

        if (dark_theme)
            fab.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                    R.drawable.ic_date_range_white_24dp));
        else
            fab.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                    R.drawable.ic_date_range_black_24dp));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        if (sharedPreference.getPrefsPreviousFirstBoot()) {
            sharedPreference.setPrefsPreviousFirstBoot(false);

            getDefaultDateRange();
            fetchData();
        } else {
            rocketLaunches.clear();
            displayLaunches();
        }

        return view;
    }

    public void displayLaunches() {
        rocketLaunches = sharedPreference.getPreviousLaunches();

        Collections.reverse(rocketLaunches);
        filterData(rocketLaunches);
    }

    public void filterData(ArrayList<RocketLaunch> rocketLaunchList) {
        String text_to_filter = sharedPreference.getPrefsPreviousFilterText().toLowerCase();

        final ArrayList<RocketLaunch> filteredData = new ArrayList<>();
        final ArrayList<RocketLaunch> filteredLocation = new ArrayList<>();
        final ArrayList<RocketLaunch> filteredRocketName = new ArrayList<>();

        for (RocketLaunch rocketLaunch : rocketLaunchList) {
            final String launch_name = rocketLaunch.getName().toLowerCase();
            final String location_name = rocketLaunch.getLocation().toLowerCase();
            if (launch_name.contains(text_to_filter)
                    || location_name.contains(text_to_filter)) {
                filteredData.add(rocketLaunch);
            }
        }

        if (filterLocation != null) {
            if (filterLocation.equals("All".toLowerCase()))
                filteredLocation.addAll(filteredData);
            else {
                for (int i = 0; i < filteredData.size(); i++) {
                    if (filteredData.get(i).getLocation().toLowerCase().contains(filterLocation))
                        filteredLocation.add(filteredData.get(i));
                }
            }
        } else
            filteredLocation.addAll(filteredData);

        if (filterRocketName != null) {
            if (filterRocketName.equals("All".toLowerCase()))
                filteredRocketName.addAll(filteredLocation);
            else {
                for (int i = 0; i < filteredLocation.size(); i++) {
                    if (filteredLocation.get(i).getName().toLowerCase().contains(filterRocketName))
                        filteredRocketName.add(filteredLocation.get(i));
                }
            }
        } else
            filteredRocketName.addAll(filteredLocation);

        if (isLocal) {
            adapterLocal.clear();
            adapterLocal.setRockets(filteredRocketName);
        } else {
            adapterGlobal.clear();
            adapterGlobal.setRockets(filteredRocketName);
        }
    }

    public void fetchData() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        sharedPreference.removePreviousLaunches();

        new PreviousLaunchesPresenter(getActivity(), sharedPreference, start_date, end_date);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear,
                          int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

        String dayStart = "";
        String dayEnd = "";

        String monthStart = "";
        String monthEnd = "";

        if (dayOfMonth < 10) {
            dayStart = dayStart + "0" + String.valueOf(dayOfMonth);
        } else {
            dayStart = String.valueOf(dayOfMonth);
        }

        if (dayOfMonthEnd < 10) {
            dayEnd = dayEnd + "0" + String.valueOf(dayOfMonthEnd);
        } else {
            dayEnd = String.valueOf(dayOfMonthEnd);
        }

        if (++monthOfYear < 10) {
            monthStart = monthStart + "0" + String.valueOf(monthOfYear);
        } else {
            monthStart = String.valueOf(monthOfYear);
        }

        if (++monthOfYearEnd < 10) {
            monthEnd = monthEnd + "0" + String.valueOf(monthOfYearEnd);
        } else {
            monthEnd = String.valueOf(monthOfYearEnd);
        }

        start_date = String.valueOf(year) + "-" + String.valueOf(monthStart) + "-" + dayStart;
        end_date = String.valueOf(yearEnd) + "-" + String.valueOf(monthEnd) + "-" + dayEnd;

        try {
            if (year == yearEnd) {
                sharedPreference.setPrefsCurrentYearRange(String.valueOf(year));
                toolbar.setTitle(String.valueOf(year));
            } else {
                sharedPreference.setPrefsCurrentYearRange(
                        String.valueOf(year) + " - " + String.valueOf(yearEnd));
                toolbar.setTitle(
                        String.format("%s - %s", String.valueOf(year), String.valueOf(yearEnd)));
            }
        } catch (NullPointerException ignored) {

        }

        // update the launch list
        rocketLaunches = new ArrayList<>();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        if (dark_theme)
            inflater.inflate(R.menu.menu_previous_fragment_dark_theme, menu);
        else
            inflater.inflate(R.menu.menu_previous_fragment_light_theme, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final MenuItem item_filter = menu.findItem(R.id.action_filter);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        searchView.setOnQueryTextListener(this);

        item_filter.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(getActivity(), FilterPreviousLaunchesActivity.class);
                startActivity(intent);

                return true;
            }
        });
    }

    @Override
    public boolean onQueryTextChange(String query) {
        // Here is where we are going to implement our filter logic
        query = query.toLowerCase();

        final List<RocketLaunch> filteredModelList = new ArrayList<>();
        for (RocketLaunch rocketLaunch : rocketLaunches) {
            final String launch_name = rocketLaunch.getName().toLowerCase();
            final String location_name = rocketLaunch.getLocation().toLowerCase();
            if (launch_name.contains(query)
                    || location_name.contains(query)) {
                filteredModelList.add(rocketLaunch);
            }
        }

        if (isLocal)
            adapterLocal.animateTo(filteredModelList);
        else
            adapterGlobal.animateTo(filteredModelList);

        rvLaunches.scrollToPosition(0);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public void showDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                PreviousLaunchesFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        if (dark_theme)
            dpd.setThemeDark(true);

        dpd.vibrate(false);
        dpd.show(getActivity().getFragmentManager(), "Date Picker");
    }

    public void getDefaultDateRange() {
        Calendar now = Calendar.getInstance();

        int day = now.get(Calendar.DAY_OF_MONTH);
        int month = now.get(Calendar.MONTH);
        int year = now.get(Calendar.YEAR);

        String dayStart = "";
        String dayEnd = "";

        String monthStart = "";
        String monthEnd = "";

        if (day < 10) {
            dayStart = dayStart + "0" + String.valueOf(day);
            dayEnd = dayEnd + "0" + String.valueOf(day);
        } else {
            dayStart = String.valueOf(day);
            dayEnd = String.valueOf(day);
        }

        if (++month < 10) {
            if (month != 1)
                monthStart = monthStart + "0" + String.valueOf(month - 1);
            else
                monthStart = "12";

            monthEnd = monthEnd + "0" + String.valueOf(month);
        } else {
            monthStart = String.valueOf(month - 1);
            monthEnd = String.valueOf(month);
        }

        start_date = String.valueOf(year) + "-" + String.valueOf(monthStart) + "-" + dayStart;
        end_date = String.valueOf(year) + "-" + String.valueOf(monthEnd) + "-" + dayEnd;

        sharedPreference.setPrefsCurrentYearRange(String.valueOf(year));

        try {
            toolbar.setTitle(String.valueOf(year));
        } catch (NullPointerException ignored) {

        }
    }

}