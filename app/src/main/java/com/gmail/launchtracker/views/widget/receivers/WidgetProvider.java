package com.gmail.launchtracker.views.widget.receivers;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.gmail.launchtracker.views.activities.MainActivity;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.storage.SharedPreference;
import com.gmail.launchtracker.views.widget.services.TimerService;

/**
 * Created by edwn112 on 3/23/16.
 */
public class WidgetProvider extends AppWidgetProvider {

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        SharedPreference sharedPreference = SharedPreference.getInstance(context);

        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int appWidgetId : appWidgetIds) {

            sharedPreference.saveAppWidgetId(appWidgetId);

            // Create an Intent to launch an Activity
            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            // Get the layout for the App Widget and attach an on-click listener
            // to the text view

            RemoteViews views = new RemoteViews(context.getPackageName(),
                    R.layout.appwidget);
            views.setOnClickPendingIntent(R.id.rlWidget, pendingIntent);

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views);

            Intent intent1 = new Intent(context, TimerService.class);
            context.startService(intent1);
        }
    }

    @Override
    public void onEnabled(Context context) {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

    public static RemoteViews getRemoteView(Context context) {
        return new RemoteViews(context.getPackageName(), R.layout.appwidget);
    }
}
