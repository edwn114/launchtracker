package com.gmail.launchtracker.mvp_services;

import com.gmail.launchtracker.models.weatherFiveDayForecast.MainModel;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by edwn112 on 7/14/2016.
 */
public class WeatherForecastService {

    private WeatherForecastApi mWeatherForecastApi;

    public WeatherForecastService() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://api.openweathermap.org/")
                .build();

        mWeatherForecastApi = retrofit.create(WeatherForecastApi.class);
    }

    public WeatherForecastApi getApi() {

        return mWeatherForecastApi;
    }

    public interface WeatherForecastApi {

        @GET("data/2.5/forecast?mode=json&units=metric")
        Observable<MainModel> getForecast(@Query("lat") String lat, @Query("lon") String lon, @Query("APPID") String appId);
    }
}
