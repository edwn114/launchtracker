package com.gmail.launchtracker.mvp_services;

import com.gmail.launchtracker.models.launchlibrary_1_2.LaunchObjects;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by edwn112 on 9/19/2016.
 */
public class PreviousLaunchesService {

    private PreviousLaunchesApi previousLaunchesApi;

    public PreviousLaunchesService() {

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://launchlibrary.net/")
                .build();

        previousLaunchesApi = retrofit.create(PreviousLaunchesApi.class);
    }

    public PreviousLaunchesApi getPreviousLaunchesApi() {
        return previousLaunchesApi;
    }

    public interface PreviousLaunchesApi {
        @GET("1.2/launch/{start_date}/{end_date}")
        Observable<LaunchObjects> getLaunches(@Path("start_date") String start_date, @Path("end_date") String end_date);
    }
}
