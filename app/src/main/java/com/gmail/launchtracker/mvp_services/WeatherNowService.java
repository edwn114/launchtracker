package com.gmail.launchtracker.mvp_services;

import com.gmail.launchtracker.models.weatherNow.MainRequest;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by edwn112 on 7/17/2016.
 */
public class WeatherNowService {

    private WeatherNowApi mWeatherNowApi;

    public WeatherNowService() {

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://api.openweathermap.org/")
                .build();

        mWeatherNowApi = retrofit.create(WeatherNowApi.class);
    }

    public WeatherNowApi getApi() {
        return mWeatherNowApi;
    }

    public interface WeatherNowApi {

        @GET("data/2.5/weather?&mode=json&units=metric")
        Observable<MainRequest> getWeatherNow
                (@Query("lat") String lat, @Query("lon") String lon, @Query("APPID") String appId);
    }
}
