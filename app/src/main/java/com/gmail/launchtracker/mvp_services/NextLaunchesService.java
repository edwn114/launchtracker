package com.gmail.launchtracker.mvp_services;

import com.gmail.launchtracker.models.launchlibrary_1_2.LaunchObjects;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by edwn112 on 9/16/2016.
 */
public class NextLaunchesService {

    private NextLaunchesApi nextLaunchesApi;

    public NextLaunchesService() {

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://launchlibrary.net/")
                .build();

        nextLaunchesApi = retrofit.create(NextLaunchesApi.class);
    }

    public NextLaunchesApi getNextLaunchesApi() {
        return nextLaunchesApi;
    }

    public interface NextLaunchesApi {
        @GET("1.2/launch/next/{value}")
        Observable<LaunchObjects> getLaunches(@Path("value") int value);
    }
}
