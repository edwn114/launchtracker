package com.gmail.launchtracker.mvp_services;

import com.gmail.launchtracker.models.LaunchStatus;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by edwn112 on 7/17/2016.
 */
public class LaunchStatusService {

    private LaunchStatusApi mLaunchStatusApi;

    public LaunchStatusService() {

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://launchlibrary.net/")
                .build();

        mLaunchStatusApi = retrofit.create(LaunchStatusApi.class);
    }

    public LaunchStatusApi getApi() {
        return mLaunchStatusApi;
    }

    public interface LaunchStatusApi {
        @GET("1.1/launchstatus/{value}")
        Observable<LaunchStatus> getStatus(@Path("value") int value);
    }
}
