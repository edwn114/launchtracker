package com.gmail.launchtracker.models.weatherNow;

/**
 * Created by edwn112 on 11/14/15.
 */
public class MainWeather {
    private float temp;
    private float pressure;
    private int humidity;
    private float temp_min;
    private float temp_max;
    private float sea_level;
    private float grnd_level;

    public float getTemp() {
        return temp;
    }

    public float getPressure() {
        return pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public float getTemp_min() {
        return temp_min;
    }

    public float getTemp_max() {
        return temp_max;
    }

    public float getSea_level() {
        return sea_level;
    }

    public float getGrnd_level() {
        return grnd_level;
    }
}
