package com.gmail.launchtracker.models.weatherNow;

/**
 * Created by edwn112 on 11/14/15.
 */
public class Coord {
    private float lon;
    private float lat;

    public float getLon() {
        return lon;
    }

    public float getLat() {
        return lat;
    }
}
