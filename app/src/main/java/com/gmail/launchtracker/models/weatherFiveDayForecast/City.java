package com.gmail.launchtracker.models.weatherFiveDayForecast;

/**
 * Created by edwn112 on 5/29/2016.
 */
public class City {
    private long id;
    private String name;
    private Coord coord;
    private String country;
    private int population;
    private Sys sys;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Coord getCoord() {
        return coord;
    }

    public String getCountry() {
        return country;
    }

    public int getPopulation() {
        return population;
    }

    public Sys getSys() {
        return sys;
    }
}
