package com.gmail.launchtracker.models.launchlibrary_1_2;

/**
 * Created by edwn112 on 9/16/2016.
 */
public class Mission {

    private int id;
    private String name;
    private String description;
    private int type;
    private String typeName;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }
}
