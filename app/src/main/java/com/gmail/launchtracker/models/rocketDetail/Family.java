package com.gmail.launchtracker.models.rocketDetail;

/**
 * Created by edwn112 on 10/30/15.
 */
public class Family {
    private String id;
    private String name;
    private String agencies;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAgencies() {
        return agencies;
    }
}
