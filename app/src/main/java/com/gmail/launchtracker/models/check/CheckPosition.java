package com.gmail.launchtracker.models.check;

/**
 * Created by edwn112 on 10/23/15.
 */
public class CheckPosition {
    private boolean isChecked;

    public CheckPosition(boolean value) {
        isChecked = value;
    }

    public void setChecked(boolean value) {
        isChecked = value;
    }

    public boolean getChecked() {
        return  isChecked;
    }
}
