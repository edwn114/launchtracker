package com.gmail.launchtracker.models.launchlibrary_1_2;

import java.util.List;

/**
 * Created by edwn112 on 9/16/2016.
 */
public class Pad {

    private int id;
    private String name;

    private String infoURL;
    private String wikiURL;

    private String mapURL;

    private String latitude;
    private String longitude;

    private List<PadAgency> agencies;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getInfoURL() {
        return infoURL;
    }

    public String getWikiURL() {
        return wikiURL;
    }

    public String getMapURL() {
        return mapURL;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public List<PadAgency> getAgencies() {
        return agencies;
    }
}
