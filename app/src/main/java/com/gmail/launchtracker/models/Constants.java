package com.gmail.launchtracker.models;

/**
 * Created by edwn112 on 10/26/15.
 */
public final class Constants {
    public static final String NETWORK_REQUESTS = "TAG";
    public static final String ACTION_UPDATE_WEATHER_STATUS = "UPDATE_WEATHER_STATUS";
    public static final String ACTION_UPDATE_UPCOMING_LAUNCHES = "UPDATE_UPCOMING_LAUNCHES";

    public static final String ACTION_GET_UPCOMING_MISSION_DATA = "GET_UPCOMING_MISSION_DATA";
    public static final String ACTION_GET_PREVIOUS_MISSION_DATA = "GET_PREVIOUS_MISSION_DATA";

    public static final String ACTION_GET_UPCOMING_LAUNCHES = "GET_UPCOMING_LAUNCHES";
    public static final String ACTION_GET_PREVIOUS_LAUNCHES = "GET_PREVIOUS_LAUNCHES";

    public static final String ACTION_SUCCESS_UPCOMING_LAUNCHES = "SUCCESS_UPCOMING_LAUNCHES";
    public static final String ACTION_SUCCESS_PREVIOUS_LAUNCHES = "SUCCESS_PREVIOUS_LAUNCHES";
    public static final String ACTION_FAILURE_UPCOMING_LAUNCHES = "FAILURE_UPCOMING_LAUNCHES";
    public static final String ACTION_FAILURE_PREVIOUS_LAUNCHES = "FAILURE_PREVIOUS_LAUNCHES";

    public static final String ACTION_GET_UPCOMING_PADS = "GET_UPCOMING_PADS";
    public static final String ACTION_GET_PREVIOUS_PADS = "GET_PREVIOUS_PADS";

    public static final String ACTION_GET_UPCOMING_AGENCIES = "GET_UPCOMING_AGENCIES";
    public static final String ACTION_GET_PREVIOUS_AGENCIES = "GET_PREVIOUS_AGENCIES";

    public static final String ACTION_NOTIFY_USER = "NOTIFY_USER";

    public static final String ACTION_SUCCESS_GET_WEATHER_NOW = "SUCCESS_GET_WEATHER_NOW";

    public static final String INTENT_NEW_TIMER = "com.gmail.launchtracker.intent.ACTION_NEW_TIMER";

    public static final String INTENT_ADD_WIDGET = "com.gmail.launchtracker.intent.ACTION_SERVICE_ADD_WIDGET";
    public static final String INTENT_REMOVE_WIDGET = "com.gmail.launchtracker.intent.ACTION_SERVICE_REMOVE_WIDGET";

    public static final String INTENT_DATA_WIDGET_ID = "WIDGET_ID";
    public static final String REFRESH_INTERVAL_KEY = "CTW_REFRESH_INTERVAL";

    public static final String INTENT_DATA_DURATION = "DURATION";

    public static final String APPID = "2ea3711ca2f25f185cf956036c17b606";
    public static final String KEY = "AIzaSyBOYgZlBMrEr1AN49n7BmPVISTe3_j0x4w";

    public static final int WEATHER = 1;
    public static final int UPCOMING_LAUNCHES = 2;
    public static final int NOTIFY_USER = 3;
    public static final int NOTIFY_USER_BEFORE_ONE_DAY = 4;
    public static final int NOTIFY_USER_BEFORE_X_HOURS = 5;
    public static final int NOTIFY_USER_BEFORE_FIVE_MINUTES = 6;
    public static final int NOTIFY_USER_BEFORE_X_MINUTES = 7;

    private Constants() {

    }

    public static String getAgencyType(String type) {
        String agencyType;

        if(type.equals("1")) {
            agencyType = "Government";
        } else if(type.equals("2")) {
            agencyType = "Multinational";
        } else if(type.equals("3")) {
            agencyType = "Commercial";
        } else if(type.equals("4")) {
            agencyType = "Educational";
        } else if(type.equals("5")) {
            agencyType = "Private";
        } else if(type.equals("6")) {
            agencyType = "Unknown";
        } else {
            agencyType = "Unknown";
        }

        return agencyType;
    }

    public static String getStatusType(int type) {
        String statusType;

        if(type == 1) {
            statusType = "Launch is GO";
        } else if(type == 2) {
            statusType = "Launch is NO-GO";
        } else if(type ==  3) {
            statusType = "Launch was a success";
        } else if(type == 4) {
            statusType = "Launch failed";
        } else {
            statusType = "Status not available";
        }

        return statusType;
    }
}
