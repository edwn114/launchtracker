package com.gmail.launchtracker.models;

public class RocketAgency {
	private String id;
	private String name;
	private String abbrev;
	private String countryCode;
	private String type;
	private String infoURL;

	public String getId() {
		return id;
	} 
	
	public String getName() {
		return name;
	}	
	
	public String getAbbrev() {	
		return abbrev;
	}	
	
	public String getCountryCode() {	
		return  countryCode;
	}	
	
	public String getType() {	
		return type;
	}
	
	public String getInfoURL() {
		return infoURL;
	} 
}