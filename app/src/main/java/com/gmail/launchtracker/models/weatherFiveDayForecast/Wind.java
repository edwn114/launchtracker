package com.gmail.launchtracker.models.weatherFiveDayForecast;

/**
 * Created by edwn112 on 5/29/2016.
 */
public class Wind {
    private double speed;
    private double deg;

    public double getSpeed() {
        return speed;
    }

    public double getDeg() {
        return deg;
    }
}
