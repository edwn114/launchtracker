package com.gmail.launchtracker.models;

/**
 * Created by edwn112 on 10/19/15.
 */
public class Type {
    private String id;
    private String name;
    private String description;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
