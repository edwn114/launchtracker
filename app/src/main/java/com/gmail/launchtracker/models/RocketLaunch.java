package com.gmail.launchtracker.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by edwn112 on 05-10-2015.
 */

public class RocketLaunch {
    private int id;
    private String name;

    private List<String> vidURLs;

    private String location;

    private String latitude;
    private String longitude;

    private String local_time_without_hours;
    private String global_time_without_hours;

    private String local_time_without_day_month;
    private String global_time_without_day_month;

    private String local_time_with_hours;
    private String global_time_with_hours;

    private String local_window_start;
    private String global_window_start;

    private String local_window_end;
    private String global_window_end;

    private String local_net;
    private String global_net;

    private int pos;

    private int status;

    private String first_three_month;

    private String local_notification;
    private String global_notification;

    private String local_time_next_launch;
    private String global_time_next_launch;

    private long net;

    public RocketLaunch() {

    }

    public void setValues(int pos, int id, String name, int status, long ws_stamp,
                          long we_stamp, long net_stamp, int tbd_time, List<String> vidURLs,
                          String location, String lat, String lon) {

        this.pos = pos;
        this.id = id;
        this.name = name;

        this.status = status;

        this.net = net_stamp;

        SimpleDateFormat format1 = new SimpleDateFormat("EEEE, d MMM yyyy");
        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm aa");
        SimpleDateFormat format3 = new SimpleDateFormat("d MMM HH:mm aa");
        SimpleDateFormat format4 = new SimpleDateFormat("EEEE, d MMM yyyy HH:mm aa");
        SimpleDateFormat format5 = new SimpleDateFormat("EEEE, yyyy HH:mm aa");
        SimpleDateFormat format6 = new SimpleDateFormat("MMM d, yyyy HH:mm aa");

        if (tbd_time == 0) {
            this.local_notification = format3
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000")));

            this.local_time_without_hours = format1
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000")));

            this.local_time_without_day_month = format5
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000")));

            this.local_time_with_hours = format4
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000")));

            this.local_window_start = format2
                    .format(new java.util.Date(Long.parseLong(ws_stamp + "000")));

            this.local_window_end = format2
                    .format(new java.util.Date(Long.parseLong(we_stamp + "000")));

            this.local_net = format2
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000")));

            this.local_time_next_launch = format6
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000")));

            format1.setTimeZone(TimeZone.getTimeZone("UTC"));
            format2.setTimeZone(TimeZone.getTimeZone("UTC"));
            format3.setTimeZone(TimeZone.getTimeZone("UTC"));
            format4.setTimeZone(TimeZone.getTimeZone("UTC"));
            format5.setTimeZone(TimeZone.getTimeZone("UTC"));
            format6.setTimeZone(TimeZone.getTimeZone("UTC"));

            this.global_notification = format3
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000"))) + " UTC";

            this.global_time_without_hours = format1
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000"))) + " UTC";

            this.global_time_without_day_month = format5
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000"))) + " UTC";

            this.global_time_with_hours = format4
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000"))) + " UTC";

            this.global_window_start = format2
                    .format(new java.util.Date(Long.parseLong(ws_stamp + "000"))) + " UTC";

            this.global_window_end = format2
                    .format(new java.util.Date(Long.parseLong(we_stamp + "000"))) + " UTC";

            this.global_net = format2
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000"))) + " UTC";

            this.global_time_next_launch = format6
                    .format(new java.util.Date(Long.parseLong(net_stamp + "000"))) + " UTC";

            this.first_three_month =
                    new SimpleDateFormat("MMM").format(new Date(Long.parseLong(ws_stamp + "000")));
        } else {
            this.local_time_without_hours = "To be determined";
            this.global_time_without_hours = "To be determined";

            this.local_time_without_day_month = "To be determined";
            this.global_time_without_day_month = "To be determined";

            this.local_time_with_hours = "To be determined";
            this.global_time_with_hours = "To be determined";

            this.local_window_start = "Tbd";
            this.local_window_end = "Tbd";
            this.local_net = "Tbd";

            this.global_window_start = "Tbd";
            this.global_window_end = "Tbd";
            this.global_net = "Tbd";

            this.local_time_next_launch = "Tbd";
            this.local_time_next_launch = "Tbd";

            this.first_three_month = "TBD";
        }

        if (vidURLs != null)
            this.vidURLs = vidURLs;
        else
            this.vidURLs = null;

        this.location = location;

        if (lat != null)
            this.latitude = String.format(Locale.getDefault(), "%.3f", Float.parseFloat(lat));

        if (lon != null)
            this.longitude = String.format(Locale.getDefault(), "%.3f", Float.parseFloat(lon));
    }

    public String getLocal_time_without_hours() {
        return local_time_without_hours;
    }

    public String getGlobal_time_without_hours() {
        return global_time_without_hours;
    }

    public String getLocal_window_start() {
        return local_window_start;
    }

    public String getLocal_window_end() {
        return local_window_end;
    }

    public String getLocal_net() {
        return local_net;
    }

    public String getGlobal_window_start() {
        return global_window_start;
    }

    public String getGlobal_window_end() {
        return global_window_end;
    }

    public String getGlobal_net() {
        return global_net;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getVidURLs() {
        return vidURLs;
    }

    public String getLocation() {
        return location;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public int getPosition() {
        return pos;
    }

    public int getStatus() {
        return status;
    }

    public String getFirst_three_month() {
        return first_three_month;
    }

    public String getLocal_notification() {
        return local_notification;
    }

    public String getGlobal_notification() {
        return global_notification;
    }

    public String getLocal_time_with_hours() {
        return local_time_with_hours;
    }

    public String getGlobal_time_with_hours() {
        return global_time_with_hours;
    }

    public String getLocal_time_without_day_month() {
        return local_time_without_day_month;
    }

    public String getGlobal_time_without_day_month() {
        return global_time_without_day_month;
    }

    public String getLocal_time_next_launch() {
        return local_time_next_launch;
    }

    public String getGlobal_time_next_launch() {
        return global_time_next_launch;
    }

    public long getNet() {
        return net;
    }
}
