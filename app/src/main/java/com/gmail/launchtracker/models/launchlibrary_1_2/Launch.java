package com.gmail.launchtracker.models.launchlibrary_1_2;

import java.util.List;

/**
 * Created by edwn112 on 9/16/2016.
 */
public class Launch {

    private int id;
    private String name;

    private String windowstart;
    private String windowend;
    private String net;

    private long wsstamp;
    private long westamp;
    private long netstamp;

    private String isostart;
    private String isoend;
    private String isonet;

    private int status;
    private int inhold;
    private int tbdtime;

    private List<String> vidURLs;
    private String vidURL;
    private List<String> infoURLs;
    private String infoURL;

    private String holdreason;
    private String failreason;

    private int tbddate;
    private int probability;

    private String hashtag;

    private LocationLaunch location;
    private Rocket rocket;
    private List<Mission> missions;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getWindowstart() {
        return windowstart;
    }

    public String getWindowend() {
        return windowend;
    }

    public String getNet() {
        return net;
    }

    public long getWsstamp() {
        return wsstamp;
    }

    public long getWestamp() {
        return westamp;
    }

    public long getNetstamp() {
        return netstamp;
    }

    public String getIsostart() {
        return isostart;
    }

    public String getIsoend() {
        return isoend;
    }

    public String getIsonet() {
        return isonet;
    }

    public int getStatus() {
        return status;
    }

    public int getInhold() {
        return inhold;
    }

    public int getTbdtime() {
        return tbdtime;
    }

    public List<String> getVidURLs() {
        return vidURLs;
    }

    public String getVidURL() {
        return vidURL;
    }

    public List<String> getInfoURLs() {
        return infoURLs;
    }

    public String getInfoURL() {
        return infoURL;
    }

    public String getHoldreason() {
        return holdreason;
    }

    public String getFailreason() {
        return failreason;
    }

    public int getTbddate() {
        return tbddate;
    }

    public int getProbability() {
        return probability;
    }

    public String getHashtag() {
        return hashtag;
    }

    public LocationLaunch getLocation() {
        return location;
    }

    public Rocket getRocket() {
        return rocket;
    }

    public List<Mission> getMissions() {
        return missions;
    }
}
