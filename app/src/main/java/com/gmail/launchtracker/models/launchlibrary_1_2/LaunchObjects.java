package com.gmail.launchtracker.models.launchlibrary_1_2;

import java.util.List;

/**
 * Created by edwn112 on 9/16/2016.
 */
public class LaunchObjects {

    private int total;
    private List<Launch> launches;
    private int offset;
    private int count;

    public int getTotal() {
        return total;
    }

    public List<Launch> getLaunches() {
        return launches;
    }

    public int getOffset() {
        return offset;
    }

    public int getCount() {
        return count;
    }
}
