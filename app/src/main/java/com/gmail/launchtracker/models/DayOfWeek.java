package com.gmail.launchtracker.models;

/**
 * Created by edwn112 on 6/14/2016.
 */
public class DayOfWeek {

    private String mDayOfWeek;
    private String mDate;

    public DayOfWeek(String dayOfWeek, String date) {
        this.mDayOfWeek = dayOfWeek;
        this.mDate = date;
    }

    public String getmDayOfWeek() {
        return mDayOfWeek;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDayOfWeek(String dayOfWeek) {
        this.mDayOfWeek = dayOfWeek;
    }

    public void setmDate(String date) {
        this.mDate = date;
    }
}
