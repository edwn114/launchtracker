package com.gmail.launchtracker.models.weatherFiveDayForecast;

import java.util.ArrayList;

/**
 * Created by edwn112 on 5/29/2016.
 */
public class MainModel {
    private City city;
    private String cod;
    private float message;
    private int cnt;
    private ArrayList<List> list;

    public City getCity() {
        return city;
    }

    public String getCod() {
        return cod;
    }

    public float getMessage() {
        return message;
    }

    public int getCnt() {
        return cnt;
    }

    public ArrayList<List> getLists() {
        return list;
    }
}
