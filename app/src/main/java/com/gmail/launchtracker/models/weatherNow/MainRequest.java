package com.gmail.launchtracker.models.weatherNow;

import java.util.List;

/**
 * Created by edwn112 on 11/14/15.
 */
public class MainRequest {
    private Coord coord;
    private List<Weather> weather;
    private String base;
    private MainWeather main;
    private Wind wind;
    private Rain rain;
    private Clouds clouds;
    private long dt;
    private Sys sys;
    private long id;
    private String name;
    private int cod;

    public Coord getCoord() {
        return coord;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public String getBase() {
        return base;
    }

    public MainWeather getMain() {
        return main;
    }

    public Wind getWind() {
        return wind;
    }

    public Rain getRain() {
        return rain;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public long getDt() {
        return dt;
    }

    public Sys getSys() {
        return sys;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return  name;
    }

    public int getCod() {
         return cod;
    }
}
