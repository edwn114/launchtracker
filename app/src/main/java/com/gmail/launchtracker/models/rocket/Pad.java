package com.gmail.launchtracker.models.rocket;

/**
 * Created by edwn112 on 10/31/15.
 */
public class Pad {
    private String id;
    private String name;
    private String latitude;
    private String longitude;
    private String mapURL;
    private String retired;
    private String locationid;
    private String padType;
    private String infoURL;
    private String wikiURL;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getMapURL() {
        return mapURL;
    }

    public String getRetired() {
        return retired;
    }

    public String getLocationid() {
        return locationid;
    }

    public String getPadType() {
        return padType;
    }

    public String getInfoURL() {
        return infoURL;
    }

    public String getWikiURL() {
        return wikiURL;
    }
}
