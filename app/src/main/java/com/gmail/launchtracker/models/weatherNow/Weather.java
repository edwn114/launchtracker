package com.gmail.launchtracker.models.weatherNow;

/**
 * Created by edwn112 on 11/14/15.
 */
public class Weather {
    private int id;
    private String main;
    private String description;
    private String icon;

    public int getId() {
        return id;
    }

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }
}
