package com.gmail.launchtracker.models.launchlibrary_1_2;

/**
 * Created by edwn112 on 9/16/2016.
 */
public class PadAgency {

    private int id;
    private String name;
    private String countryCode;
    private int type;
    private String infoURL;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public int getType() {
        return type;
    }

    public String getInfoURL() {
        return infoURL;
    }
}
