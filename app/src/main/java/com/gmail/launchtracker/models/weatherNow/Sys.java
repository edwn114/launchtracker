package com.gmail.launchtracker.models.weatherNow;

/**
 * Created by edwn112 on 11/14/15.
 */
public class Sys {
    private String message;
    private String country;
    private long sunrise;
    private long sunset;

    public String getMessage() {
        return message;
    }

    public String getCountry() {
        return country;
    }

    public long getSunrise() {
        return sunrise;
    }

    public long getSunset() {
        return sunset;
    }
}

