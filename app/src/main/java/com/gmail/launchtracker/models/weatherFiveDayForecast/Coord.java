package com.gmail.launchtracker.models.weatherFiveDayForecast;

/**
 * Created by edwn112 on 5/29/2016.
 */
public class Coord {
    private double lon;
    private double lat;

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }
}
