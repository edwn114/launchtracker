package com.gmail.launchtracker.models.launchlibrary_1_2;

import java.util.List;

/**
 * Created by edwn112 on 9/16/2016.
 */
public class Rocket {

    private int id;
    private String name;
    private String configuration;
    private String familyName;
    private List<RocketAgency> agencies;
    private List<Integer> imageSizes;
    private String imageURL;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getConfiguration() {
        return configuration;
    }

    public String getFamilyName() {
        return familyName;
    }

    public List<RocketAgency> getAgencies() {
        return agencies;
    }

    public List<Integer> getImageSizes() {
        return imageSizes;
    }

    public String getImageURL() {
        return imageURL;
    }
}
