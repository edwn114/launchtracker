package com.gmail.launchtracker.models.weatherFiveDayForecast;

import java.util.ArrayList;

/**
 * Created by edwn112 on 5/29/2016.
 */
public class List {
    private long dt;
    private Main main;
    private ArrayList<Weather> weather;
    private Clouds clouds;
    private Wind wind;
    private String dt_txt;

    public long getDt() {
        return dt;
    }

    public Main getMain() {
        return main;
    }

    public ArrayList<Weather> getWeathers() {
        return weather;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public String getDt_txt() {
        return dt_txt;
    }
}
