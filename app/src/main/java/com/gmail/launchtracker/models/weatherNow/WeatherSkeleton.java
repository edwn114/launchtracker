package com.gmail.launchtracker.models.weatherNow;

/**
 * Created by edwn112 on 11/14/15.
 */
public class WeatherSkeleton {
    private String last_update_time;
    private String description;
    private String temp;
    private String humidity;
    private String pressure;
    private String windSpeed;
    private String iconName;
    private int clouds;
    private long dt;

    public WeatherSkeleton(String description, String temp, String humidity, String pressure,
                           String windSpeed, int clouds, String iconName, long dt, String last_update_time) {
        this.description = description;
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;
        this.windSpeed = windSpeed;
        this.clouds = clouds;
        this.iconName = iconName;
        this.dt = dt;
        this.last_update_time = last_update_time;
    }

    public int getClouds() {
        return clouds;
    }

    public String getDescription() {
        return description;
    }

    public String getTemp() {
        return temp;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public String getIconName() {
        return iconName;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public long getDt() {
        return dt;
    }

    public String getLast_update_time() {
        return last_update_time;
    }
}
