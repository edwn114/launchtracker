package com.gmail.launchtracker.models.rocket;

/**
 * Created by edwn112 on 10/31/15.
 */
public class Agency {
    private String id;
    private String name;
    private String countryCode;
    private String type;
    private String infoURL;
    private String wikiURL;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getType() {
        return type;
    }

    public String getInfoURL() {
        return infoURL;
    }

    public String getWikiURL() {
        return wikiURL;
    }
}
