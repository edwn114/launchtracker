package com.gmail.launchtracker.models.launchlibrary_1_2;

import java.util.List;

/**
 * Created by edwn112 on 9/16/2016.
 */
public class RocketAgency {

    private int id;
    private String name;
    private String abbrev;
    private String countryCode;
    private int type;
    private String infoURL;
    private String wikiURL;
    private List<String> infoURLs;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public int getType() {
        return type;
    }

    public String getInfoURL() {
        return infoURL;
    }

    public String getWikiURL() {
        return wikiURL;
    }

    public List<String> getInfoURLs() {
        return infoURLs;
    }
}
