package com.gmail.launchtracker.models.rocketDetail;

/**
 * Created by edwn112 on 10/20/15.
 */
public class Launch {
    private String id;
    private String name;
    private String windowstart;
    private String windowend;
    private String net;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getWindowstart() {
        return  windowstart;
    }

    public String getWindowend() {
        return windowend;
    }

    public String getNet() {
        return net;
    }
}
