package com.gmail.launchtracker.models.rocket;

import com.gmail.launchtracker.models.rocketDetail.Family;

/**
 * Created by edwn112 on 10/30/15.
 */
public class Rocket {

    private String id;
    private String name;
    private String configuration;
    private String defaultPads;
    private Family family;
    private String infoURL;
    private String wikiURL;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getConfiguration() {
        return configuration;
    }

    public String getDefaultPads() {
        return defaultPads;
    }

    public Family getFamily() {
        return family;
    }

    public String getInfoURL() {
        return infoURL;
    }

    public String getWikiURL() {
        return wikiURL;
    }
}
