package com.gmail.launchtracker.models.rocketDetail;

import java.util.List;

/**
 * Created by edwn112 on 10/20/15.
 */
public class Mission {
    private String id;
    private String name;
    private String description;
    private List<Agency> agencies;
    private String type;
    private Launch launch;
    private String infoURL;
    private String wikiURL;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Agency> getAgencies() {
        return agencies;
    }

    public String getType() {
        return type;
    }

    public Launch getLaunch() {
        return launch;
    }

    public String getInfoURL() {
        return infoURL;
    }

    public String getWikiURL() {
        return wikiURL;
    }

    public void setDescription(String desc) {
        description = desc;
    }
}
