package com.gmail.launchtracker.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.gmail.launchtracker.models.launchlibrary_1_2.Launch;
import com.gmail.launchtracker.models.RocketLaunch;
import com.gmail.launchtracker.models.check.CheckPosition;
import com.gmail.launchtracker.models.rocket.Agency;
import com.gmail.launchtracker.models.rocket.Pad;
import com.gmail.launchtracker.models.rocketDetail.Mission;
import com.gmail.launchtracker.models.weatherNow.WeatherSkeleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by edwn112 on 07-10-2015.
 */
public class SharedPreference {

    public static String PREFS_NAME = "SAVED";
    public static String PREFS_FIRST_BOOT = "IS_FIRST_BOOT";
    public static String PREFS_PREVIOUS_FIRST_BOOT = "IS_PREVIOUS_FIRST_BOOT";
    public static String PREFS_POSITION = "POSITION_VALUE";
    public static String PREFS_ROCKET_POSITION_UPCOMING = "ROCKET_POSITION_VALUE_UPCOMING";
    public static String PREFS_ROCKET_POSITION_PREVIOUS = "ROCKET_POSITION_VALUE_PREVIOUS";
    public static String PREFS_UPCOMING_ROCKET_LAUNCH_LIST = "UPCOMING_ROCKET_LAUNCH_LIST";
    public static String PREFS_PREVIOUS_ROCKET_LAUNCH_LIST = "PREVIOUS_ROCKET_LAUNCH_LIST";
    public static String PREFS_DETAIL_ROCKET_LIST_UPCOMING = "DETAIL_ROCKET_LIST_UPCOMING";
    public static String PREFS_DETAIL_ROCKET_LIST_PREVIOUS = "DETAIL_ROCKET_LIST_PREVIOUS";
    public static String PREFS_LAUNCH_LIST_UPCOMING = "LAUNCH_LIST_UPCOMING";
    public static String PREFS_LAUNCH_LIST_PREVIOUS = "LAUNCH_LIST_PREVIOUS";
    public static String PREFS_POSITION_LIST_UPCOMING = "POSITION_LIST_UPCOMING";
    public static String PREFS_POSITION_LIST_PREVIOUS = "POSITION_LIST_PREVIOUS";
    public static String PREFS_MISSIONS_LIST_UPCOMING = "MISSION_LIST_UPCOMING";
    public static String PREFS_MISSIONS_LIST_PREVIOUS = "MISSION_LIST_PREVIOUS";
    public static String PREFS_PAD_LIST_UPCOMING = "PAD_LIST_UPCOMING";
    public static String PREFS_PAD_LIST_PREVIOUS = "PAD_LIST_PREVIOUS";
    public static String PREFS_AGENCY_LIST_UPCOMING = "AGENCY_LIST_UPCOMING";
    public static String PREFS_AGENCY_LIST_PREVIOUS = "AGENCY_LIST_PREVIOUS";
    public static String PREFS_STATUS = "STATUS";
    public static String PREFS_STATUS_VALUE = "STATUS_VALUE";
    public static String PREFS_PAD_POSITION_UPCOMING = "PAD_POSITION_UPCOMING";
    public static String PREFS_PAD_POSITION_PREVIOUS = "PAD_POSITION_PREVIOUS";
    public static String PREFS_AGENCY_POSITION_UPCOMING = "AGENCY_POSITION_UPCOMING";
    public static String PREFS_AGENCY_POSITION_PREVIOUS = "AGENCY_POSITION_PREVIOUS";
    public static String PREFS_WEATHER = "WEATHER";
    public static String PREFS_WEB_DATA = "WEB_DATA";
    public static String PREFS_WEB_MISSION_LIST = "WEB_MISSION_LIST";
    public static String PREFS_PREVIOUS_FILTER_TEXT = "PREVIOUS_FILTER_TEXT";
    public static String PREFS_UPCOMING_FILTER_TEXT = "UPCOMING_FILTER_TEXT";
    public static String PREFS_UPCOMING_FILTER_POS = "UPCOMING_FILTER_POS";
    public static String PREFS_UPCOMING_LOCATION_POS = "UPCOMING_LOCATION_POS";
    public static String PREFS_UPCOMING_ROCKET_POS = "UPCOMING_ROCKET_POS";
    public static String PREFS_PREVIOUS_LOCATION_POS = "PREVIOUS_LOCATION_POS";
    public static String PREFS_PREVIOUS_ROCKET_POS = "PREVIOUS_ROCKET_POS";
    public static String PREFS_LAST_UPCOMING_LAUNCH_UPDATE = "LAST_UPCOMING_LAUNCH_UPDATE";
    public static String PREFS_FETCHED_WEATHER = "FETCHED_WEATHER";
    public static String PREFS_CURRENT_YEAR_RANGE = "CURRENT_YEAR_RANGE";
    public static String PREFS_APP_WIDGET_ID = "APP_WIDGET_ID";

    private static SharedPreference INSTANCE = null;
    private Context appContext;

    SharedPreferences settings = null;
    SharedPreferences.Editor editor = null;

    private SharedPreference(Context context) {
        this.appContext = context.getApplicationContext();
    }

    public static SharedPreference getInstance(Context context) {
        if (INSTANCE == null)
            INSTANCE = new SharedPreference(context);
        return INSTANCE;
    }

    public void saveCurrentPosition(int pos) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_POSITION, pos);
        editor.apply();
    }

    public void saveAppWidgetId(int appWidgetId) {
        settings = appContext.getSharedPreferences(PREFS_APP_WIDGET_ID,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_APP_WIDGET_ID, appWidgetId);
        editor.apply();
    }

    public void saveUpcomingTimeRangePosition(int pos) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_UPCOMING_FILTER_POS, pos);
        editor.apply();
    }

    public void saveUpcomingRocketPosition(int pos) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_UPCOMING_ROCKET_POS, pos);
        editor.apply();
    }


    public void savePreviousRocketPosition(int pos) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_PREVIOUS_ROCKET_POS, pos);
        editor.apply();
    }

    public void saveUpcomingLocationPosition(int pos) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_UPCOMING_LOCATION_POS, pos);
        editor.apply();
    }

    public void savePreviousLocationPosition(int pos) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_PREVIOUS_LOCATION_POS, pos);
        editor.apply();
    }

    public void setPrefsStatusValue(String statusValue) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putString(PREFS_STATUS_VALUE, statusValue);
        editor.apply();
    }

    public void setPrefsCurrentYearRange(String currentYearRange) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putString(PREFS_CURRENT_YEAR_RANGE, currentYearRange);
        editor.apply();
    }

    public void setPrefsLastUpcomingLaunchUpdate(String last_update) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putString(PREFS_LAST_UPCOMING_LAUNCH_UPDATE, last_update);
        editor.apply();
    }

    public void setPrefsFetchedWeather(boolean value) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putBoolean(PREFS_FETCHED_WEATHER, value);
        editor.apply();
    }

    public void setPrefsFirstBoot(boolean value) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putBoolean(PREFS_FIRST_BOOT, value);
        editor.apply();
    }

    public void setPrefsPreviousFirstBoot(boolean value) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putBoolean(PREFS_PREVIOUS_FIRST_BOOT, value);
        editor.apply();
    }

    public void setStatus(boolean value) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putBoolean(PREFS_STATUS, value);
        editor.apply();
    }

    public boolean getPrefsFirstBoot() {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getBoolean(PREFS_FIRST_BOOT, true);
    }

    public boolean getPrefsStatus() {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getBoolean(PREFS_STATUS, false);
    }

    public boolean getPrefsFetchedWeather() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getBoolean(PREFS_FETCHED_WEATHER, false);
    }

    public boolean getPrefsPreviousFirstBoot() {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getBoolean(PREFS_PREVIOUS_FIRST_BOOT, true);
    }

    public int getCurrentPosition() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getInt(PREFS_POSITION, 0);
    }


    public int getAppWidgetId() {
        settings = appContext.getSharedPreferences(PREFS_APP_WIDGET_ID,
                Context.MODE_PRIVATE);

        return settings.getInt(PREFS_POSITION, -1);
    }

    public int getUpcomingTimeRangePosition() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getInt(PREFS_UPCOMING_FILTER_POS, 0);
    }

    public int getUpcomingRocketPosition() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getInt(PREFS_UPCOMING_ROCKET_POS, 0);
    }

    public int getPreviousRocketPosition() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getInt(PREFS_PREVIOUS_ROCKET_POS, 0);
    }

    public int getUpcomingLocationPosition() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getInt(PREFS_UPCOMING_LOCATION_POS, 0);
    }

    public int getPreviousLocationPosition() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getInt(PREFS_PREVIOUS_LOCATION_POS, 0);
    }

    public void saveUpcomingLaunches(List<RocketLaunch> rocketLaunches) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(rocketLaunches);

        editor.putString(PREFS_UPCOMING_ROCKET_LAUNCH_LIST, jsonList);
        editor.apply();
    }

    public void savePreviousLaunches(List<RocketLaunch> rocketLaunches) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(rocketLaunches);

        editor.putString(PREFS_PREVIOUS_ROCKET_LAUNCH_LIST, jsonList);
        editor.apply();
    }

    public void saveWeather(WeatherSkeleton weather) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String json = gson.toJson(weather);

        editor.putString(PREFS_WEATHER, json);
        editor.apply();
    }

    public void saveDetailRocketsUpcoming(List<com.gmail.launchtracker.models.rocket.Rocket> rockets) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(rockets);

        editor.putString(PREFS_DETAIL_ROCKET_LIST_UPCOMING, jsonList);
        editor.apply();
    }

    public void saveDetailRocketsPrevious(List<com.gmail.launchtracker.models.rocket.Rocket> rockets) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(rockets);

        editor.putString(PREFS_DETAIL_ROCKET_LIST_PREVIOUS, jsonList);
        editor.apply();
    }

    public void saveLaunchesUpcoming(List<Launch> launches) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(launches);

        editor.putString(PREFS_LAUNCH_LIST_UPCOMING, jsonList);
        editor.apply();
    }

    public void saveLaunchesPrevious(List<Launch> launches) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(launches);

        editor.putString(PREFS_LAUNCH_LIST_PREVIOUS, jsonList);
        editor.apply();
    }

    public void savePositionsUpcoming(List<CheckPosition> positions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(positions);

        editor.putString(PREFS_POSITION_LIST_UPCOMING, jsonList);
        editor.apply();
    }

    public void savePositionsPrevious(List<CheckPosition> positions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(positions);

        editor.putString(PREFS_POSITION_LIST_PREVIOUS, jsonList);
        editor.apply();
    }

    public void saveRocketPositionsUpcoming(List<CheckPosition> positions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(positions);

        editor.putString(PREFS_ROCKET_POSITION_UPCOMING, jsonList);
        editor.apply();
    }

    public void saveRocketPositionsPrevious(List<CheckPosition> positions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(positions);

        editor.putString(PREFS_ROCKET_POSITION_PREVIOUS, jsonList);
        editor.apply();
    }

    public void savePadPositionsUpcoming(List<CheckPosition> positions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(positions);

        editor.putString(PREFS_PAD_POSITION_UPCOMING, jsonList);
        editor.apply();
    }

    public void savePadPositionsPrevious(List<CheckPosition> positions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(positions);

        editor.putString(PREFS_PAD_POSITION_PREVIOUS, jsonList);
        editor.apply();
    }

    public void saveAgencyPositionsUpcoming(List<CheckPosition> positions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(positions);
        editor.putString(PREFS_AGENCY_POSITION_UPCOMING, jsonList);
        editor.apply();
    }

    public void saveAgencyPositionsPrevious(List<CheckPosition> positions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(positions);
        editor.putString(PREFS_AGENCY_POSITION_PREVIOUS, jsonList);
        editor.apply();
    }

    public void saveWebData(ArrayList<String> webData) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(webData);
        editor.putString(PREFS_WEB_DATA, jsonList);
        editor.apply();
    }

    public void saveWebMissionList(ArrayList<String> webMissionL) {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonList = gson.toJson(webMissionL);
        editor.putString(PREFS_WEB_MISSION_LIST, jsonList);
        editor.apply();
    }

    public void saveMissionsUpcoming(int position, List<Mission> missions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Mission>>>() {
        }.getType();

        HashMap<Integer, List<Mission>> missionsL = getMissionsUpcoming();

        if (missionsL == null)
            missionsL = new HashMap<>();

        missionsL.put(position, missions);

        Gson gson = new Gson();

        String jsonMap = gson.toJson(missionsL, mapType);

        editor.putString(PREFS_MISSIONS_LIST_UPCOMING, jsonMap);
        editor.apply();
    }

    public void saveMissionsPrevious(int position, List<Mission> missions) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Mission>>>() {
        }.getType();

        HashMap<Integer, List<Mission>> missionsL = getMissionsPrevious();

        if (missionsL == null)
            missionsL = new HashMap<>();

        missionsL.put(position, missions);

        Gson gson = new Gson();

        String jsonMap = gson.toJson(missionsL, mapType);

        editor.putString(PREFS_MISSIONS_LIST_PREVIOUS, jsonMap);
        editor.apply();
    }

    public void savePadsUpcoming(int position, List<Pad> padList) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Pad>>>() {
        }.getType();

        HashMap<Integer, List<Pad>> padL = getPadsUpcoming();

        if (padL == null)
            padL = new HashMap<>();

        padL.put(position, padList);

        Gson gson = new Gson();

        String jsonMap = gson.toJson(padL, mapType);

        editor.putString(PREFS_PAD_LIST_UPCOMING, jsonMap);
        editor.apply();
    }

    public void savePadsPrevious(int position, List<Pad> padList) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Pad>>>() {
        }.getType();

        HashMap<Integer, List<Pad>> padL = getPadsPrevious();

        if (padL == null)
            padL = new HashMap<>();

        padL.put(position, padList);

        Gson gson = new Gson();

        String jsonMap = gson.toJson(padL, mapType);

        editor.putString(PREFS_PAD_LIST_PREVIOUS, jsonMap);
        editor.apply();
    }

    public void addPadUpcoming(int position, Pad pad) {

        HashMap<Integer, List<Pad>> padL = getPadsUpcoming();

        List<Pad> padList;

        if (padL == null) {
            padList = new ArrayList<>();
            padList.add(pad);
        } else {
            padList = padL.get(position);
            if (padList == null) {
                padList = new ArrayList<>();
                padList.add(pad);
            } else
                padList.add(pad);
        }
        savePadsUpcoming(position, padList);
    }

    public void addPadPrevious(int position, Pad pad) {

        HashMap<Integer, List<Pad>> padL = getPadsPrevious();

        List<Pad> padList;

        if (padL == null) {
            padList = new ArrayList<>();
            padList.add(pad);
        } else {
            padList = padL.get(position);
            if (padList == null) {
                padList = new ArrayList<>();
                padList.add(pad);
            } else
                padList.add(pad);
        }
        savePadsPrevious(position, padList);
    }

    public void addWebMission(int position, String webMission) {

        ArrayList<String> webMissionL = getWebMissionList();

        if (webMissionL == null) {
            webMissionL = new ArrayList<>();
        }

        webMissionL.remove(position);
        webMissionL.add(position, webMission);

        saveWebMissionList(webMissionL);
    }

    public void addWebData(int position, String webData) {

        ArrayList<String> webDataL = getWebData();

        if (webDataL == null) {
            webDataL = new ArrayList<>();
        }

        webDataL.remove(position);
        webDataL.add(position, webData);

        saveWebData(webDataL);
    }

    public void addAgencyUpcoming(int position, Agency agency) {

        HashMap<Integer, List<Agency>> agencyL = getAgenciesUpcoming();

        List<Agency> agencyList;

        if (agencyL == null) {
            agencyList = new ArrayList<>();
            agencyList.add(agency);
        } else {
            agencyList = agencyL.get(position);
            if (agencyList == null) {
                agencyList = new ArrayList<>();
                agencyList.add(agency);
            } else
                agencyList.add(agency);
        }
        saveAgenciesUpcoming(position, agencyList);
    }

    public void addAgencyPrevious(int position, Agency agency) {

        HashMap<Integer, List<Agency>> agencyL = getAgenciesPrevious();

        List<Agency> agencyList;

        if (agencyL == null) {
            agencyList = new ArrayList<>();
            agencyList.add(agency);
        } else {
            agencyList = agencyL.get(position);
            if (agencyList == null) {
                agencyList = new ArrayList<>();
                agencyList.add(agency);
            } else
                agencyList.add(agency);
        }
        saveAgenciesPrevious(position, agencyList);
    }

    public void saveAgenciesUpcoming(int position, List<Agency> agencyList) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Agency>>>() {
        }.getType();

        HashMap<Integer, List<Agency>> agencies = getAgenciesUpcoming();

        if (agencies == null)
            agencies = new HashMap<>();

        agencies.put(position, agencyList);

        Gson gson = new Gson();

        String jsonMap = gson.toJson(agencies, mapType);

        editor.putString(PREFS_AGENCY_LIST_UPCOMING, jsonMap);
        editor.apply();
    }

    public void saveAgenciesPrevious(int position, List<Agency> agencyList) {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Agency>>>() {
        }.getType();

        HashMap<Integer, List<Agency>> agencies = getAgenciesPrevious();

        if (agencies == null)
            agencies = new HashMap<>();

        agencies.put(position, agencyList);

        Gson gson = new Gson();

        String jsonMap = gson.toJson(agencies, mapType);

        editor.putString(PREFS_AGENCY_LIST_PREVIOUS, jsonMap);
        editor.apply();
    }

    public void addUpcomingLaunch(RocketLaunch rocketLaunch) {
        List<RocketLaunch> rocketLaunches = getUpcomingLaunches();

        if (rocketLaunches == null)
            rocketLaunches = new ArrayList<>();
        rocketLaunches.add(rocketLaunch);

        saveUpcomingLaunches(rocketLaunches);
    }

    public void addPreviousLaunch(RocketLaunch rocketLaunch) {
        List<RocketLaunch> rocketLaunches = getPreviousLaunches();

        if (rocketLaunches == null)
            rocketLaunches = new ArrayList<>();

        rocketLaunches.add(rocketLaunch);

        savePreviousLaunches(rocketLaunches);
    }

    public void addDetailRocketUpcoming(int position, com.gmail.launchtracker.models.rocket.Rocket rocket) {
        List<com.gmail.launchtracker.models.rocket.Rocket> rockets = getDetailRocketsUpcoming();

        if (rockets == null)
            rockets = new ArrayList<>();

        rockets.remove(position);
        rockets.add(position, rocket);

        saveDetailRocketsUpcoming(rockets);
    }


    public void addDetailRocketPrevious(int position, com.gmail.launchtracker.models.rocket.Rocket rocket) {
        List<com.gmail.launchtracker.models.rocket.Rocket> rockets = getDetailRocketsPrevious();

        if (rockets == null)
            rockets = new ArrayList<>();

        rockets.remove(position);
        rockets.add(position, rocket);

        saveDetailRocketsPrevious(rockets);
    }

    public void removeDetailRocketUpcoming() {
        ArrayList<com.gmail.launchtracker.models.rocket.Rocket> rockets
                = new ArrayList<>();
        saveDetailRocketsUpcoming(rockets);
    }

    public void removeDetailRocketPrevious() {
        ArrayList<com.gmail.launchtracker.models.rocket.Rocket> rockets
                = new ArrayList<>();
        saveDetailRocketsPrevious(rockets);
    }

    public void addPositionUpcoming(CheckPosition checkPosition) {
        List<CheckPosition> positions = getPositionsUpcoming();

        if (positions == null)
            positions = new ArrayList<>();
        positions.add(checkPosition);

        savePositionsUpcoming(positions);
    }

    public void addPositionPrevious(CheckPosition checkPosition) {
        List<CheckPosition> positions = getPositionsPrevious();

        if (positions == null)
            positions = new ArrayList<>();
        positions.add(checkPosition);

        savePositionsPrevious(positions);
    }

    public void addPadPositionUpcoming(CheckPosition checkPosition) {
        List<CheckPosition> positions = getPadPositionsUpcoming();

        if (positions == null)
            positions = new ArrayList<>();
        positions.add(checkPosition);

        savePadPositionsUpcoming(positions);
    }

    public void addPadPositionPrevious(CheckPosition checkPosition) {
        List<CheckPosition> positions = getPadPositionsPrevious();

        if (positions == null)
            positions = new ArrayList<>();
        positions.add(checkPosition);

        savePadPositionsPrevious(positions);
    }

    public void addAgencyPositionUpcoming(CheckPosition checkPosition) {
        List<CheckPosition> positions = getAgencyPositionsUpcoming();

        if (positions == null)
            positions = new ArrayList<>();
        positions.add(checkPosition);

        saveAgencyPositionsUpcoming(positions);
    }

    public void addAgencyPositionPrevious(CheckPosition checkPosition) {
        List<CheckPosition> positions = getAgencyPositionsPrevious();

        if (positions == null)
            positions = new ArrayList<>();
        positions.add(checkPosition);

        saveAgencyPositionsPrevious(positions);
    }

    public void addRocketPositionUpcoming(CheckPosition checkPosition) {
        List<CheckPosition> positions = getRocketPositionsUpcoming();

        if (positions == null)
            positions = new ArrayList<>();
        positions.add(checkPosition);

        saveRocketPositionsUpcoming(positions);
    }

    public void addRocketPositionPrevious(CheckPosition checkPosition) {
        List<CheckPosition> positions = getRocketPositionsPrevious();

        if (positions == null)
            positions = new ArrayList<>();
        positions.add(checkPosition);

        saveRocketPositionsPrevious(positions);
    }

    public void removePositionsUpcoming() {
        ArrayList<CheckPosition> positions = new ArrayList<>();

        savePositionsUpcoming(positions);
    }

    public void removePositionsPrevious() {
        ArrayList<CheckPosition> positions = new ArrayList<>();

        savePositionsPrevious(positions);
    }

    public void removePadPositionsUpcoming() {
        ArrayList<CheckPosition> positions = new ArrayList<>();

        savePadPositionsUpcoming(positions);
    }

    public void removePadPositionsPrevious() {
        ArrayList<CheckPosition> positions = new ArrayList<>();

        savePadPositionsPrevious(positions);
    }

    public void removeAgencyPositionsUpcoming() {
        ArrayList<CheckPosition> positions = new ArrayList<>();

        saveAgencyPositionsUpcoming(positions);
    }

    public void removeAgencyPositionsPrevious() {
        ArrayList<CheckPosition> positions = new ArrayList<>();

        saveAgencyPositionsPrevious(positions);
    }

    public void removeUpcomingLaunches() {
        ArrayList<RocketLaunch> rocketLaunches = new ArrayList<>();

        saveUpcomingLaunches(rocketLaunches);
    }

    public void removePreviousLaunches() {
        ArrayList<RocketLaunch> rocketLaunches = new ArrayList<>();

        savePreviousLaunches(rocketLaunches);
    }

    public void removeRocketPositionsUpcoming() {
        ArrayList<CheckPosition> rocketPosL = new ArrayList<>();

        saveRocketPositionsUpcoming(rocketPosL);
    }

    public void removeRocketPositionsPrevious() {
        ArrayList<CheckPosition> rocketPosL = new ArrayList<>();

        saveRocketPositionsPrevious(rocketPosL);
    }

    public void removeLaunchesUpcoming() {
        ArrayList<Launch> launches = new ArrayList<>();

        saveLaunchesUpcoming(launches);
    }

    public void removeLaunchesPrevious() {
        ArrayList<Launch> launches = new ArrayList<>();

        saveLaunchesPrevious(launches);
    }

    public void removePadsUpcoming() {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Pad>>>() {
        }.getType();

        HashMap<Integer, List<Pad>> padL = new HashMap<>();

        Gson gson = new Gson();

        String jsonMap = gson.toJson(padL, mapType);

        editor.putString(PREFS_PAD_LIST_UPCOMING, jsonMap);
        editor.apply();
    }

    public void removePadsPrevious() {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Pad>>>() {
        }.getType();

        HashMap<Integer, List<Pad>> padL = new HashMap<>();

        Gson gson = new Gson();

        String jsonMap = gson.toJson(padL, mapType);

        editor.putString(PREFS_PAD_LIST_PREVIOUS, jsonMap);
        editor.apply();
    }

    public void removeAgenciesUpcoming() {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Pad>>>() {
        }.getType();

        HashMap<Integer, List<Agency>> agencyL = new HashMap<>();

        Gson gson = new Gson();

        String jsonMap = gson.toJson(agencyL, mapType);

        editor.putString(PREFS_AGENCY_LIST_UPCOMING, jsonMap);
        editor.apply();
    }

    public void removeAgenciesPrevious() {

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Type mapType = new TypeToken<HashMap<Integer, List<Pad>>>() {
        }.getType();

        HashMap<Integer, List<Agency>> agencyL = new HashMap<>();

        Gson gson = new Gson();

        String jsonMap = gson.toJson(agencyL, mapType);

        editor.putString(PREFS_AGENCY_LIST_PREVIOUS, jsonMap);
        editor.apply();
    }

    public ArrayList<RocketLaunch> getUpcomingLaunches() {
        List<RocketLaunch> rocketLaunches;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_UPCOMING_ROCKET_LAUNCH_LIST)) {
            String jsonList = settings.getString(PREFS_UPCOMING_ROCKET_LAUNCH_LIST, null);

            Gson gson = new Gson();
            RocketLaunch[] rocketsList = gson.fromJson(jsonList,
                    RocketLaunch[].class);

            rocketLaunches = Arrays.asList(rocketsList);
            rocketLaunches = new ArrayList<>(rocketLaunches);
        } else
            return null;

        return (ArrayList<RocketLaunch>) rocketLaunches;
    }

    public ArrayList<RocketLaunch> getPreviousLaunches() {
        List<RocketLaunch> rocketLaunches;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_PREVIOUS_ROCKET_LAUNCH_LIST)) {
            String jsonList = settings.getString(PREFS_PREVIOUS_ROCKET_LAUNCH_LIST, null);

            Gson gson = new Gson();
            RocketLaunch[] rocketsList = gson.fromJson(jsonList,
                    RocketLaunch[].class);

            rocketLaunches = Arrays.asList(rocketsList);
            rocketLaunches = new ArrayList<>(rocketLaunches);
        } else
            return null;

        return (ArrayList<RocketLaunch>) rocketLaunches;
    }

    public WeatherSkeleton getWeather() {
        WeatherSkeleton weather;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_WEATHER)) {
            String json = settings.getString(PREFS_WEATHER, null);

            Gson gson = new Gson();
            weather = gson.fromJson(json,
                    WeatherSkeleton.class);

        } else
            return null;

        return weather;
    }

    public ArrayList<com.gmail.launchtracker.models.rocket.Rocket> getDetailRocketsUpcoming() {
        List<com.gmail.launchtracker.models.rocket.Rocket> rockets;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_DETAIL_ROCKET_LIST_UPCOMING)) {
            String jsonList = settings.getString(PREFS_DETAIL_ROCKET_LIST_UPCOMING, null);

            Gson gson = new Gson();

            com.gmail.launchtracker.models.rocket.Rocket[] rocketsList = gson.fromJson(jsonList,
                    com.gmail.launchtracker.models.rocket.Rocket[].class);

            rockets = Arrays.asList(rocketsList);
            rockets = new ArrayList<>(rockets);
        } else
            return null;

        return (ArrayList<com.gmail.launchtracker.models.rocket.Rocket>) rockets;
    }

    public ArrayList<com.gmail.launchtracker.models.rocket.Rocket> getDetailRocketsPrevious() {
        List<com.gmail.launchtracker.models.rocket.Rocket> rockets;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_DETAIL_ROCKET_LIST_PREVIOUS)) {
            String jsonList = settings.getString(PREFS_DETAIL_ROCKET_LIST_PREVIOUS, null);

            Gson gson = new Gson();

            com.gmail.launchtracker.models.rocket.Rocket[] rocketsList = gson.fromJson(jsonList,
                    com.gmail.launchtracker.models.rocket.Rocket[].class);

            rockets = Arrays.asList(rocketsList);
            rockets = new ArrayList<>(rockets);
        } else
            return null;

        return (ArrayList<com.gmail.launchtracker.models.rocket.Rocket>) rockets;
    }

    public ArrayList<Launch> getLaunchesUpcoming() {
        List<Launch> launches;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_LAUNCH_LIST_UPCOMING)) {
            String jsonList = settings.getString(PREFS_LAUNCH_LIST_UPCOMING, null);

            Gson gson = new Gson();
            Launch[] launchesList = gson.fromJson(jsonList,
                    Launch[].class);

            launches = Arrays.asList(launchesList);
            launches = new ArrayList<>(launches);
        } else
            return null;

        return (ArrayList<Launch>) launches;
    }

    public ArrayList<Launch> getLaunchesPrevious() {
        List<Launch> launches;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_LAUNCH_LIST_PREVIOUS)) {
            String jsonList = settings.getString(PREFS_LAUNCH_LIST_PREVIOUS, null);

            Gson gson = new Gson();
            Launch[] launchesList = gson.fromJson(jsonList,
                    Launch[].class);

            launches = Arrays.asList(launchesList);
            launches = new ArrayList<>(launches);
        } else
            return null;

        return (ArrayList<Launch>) launches;
    }

    public ArrayList<String> getWebData() {
        List<String> webData;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_WEB_DATA)) {
            String jsonList = settings.getString(PREFS_WEB_DATA, null);

            Gson gson = new Gson();
            String[] webDataL = gson.fromJson(jsonList,
                    String[].class);

            webData = Arrays.asList(webDataL);
            webData = new ArrayList<>(webData);
        } else
            return null;

        return (ArrayList<String>) webData;
    }

    public ArrayList<String> getWebMissionList() {
        List<String> webMissionList;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_WEB_MISSION_LIST)) {
            String jsonList = settings.getString(PREFS_WEB_MISSION_LIST, null);

            Gson gson = new Gson();
            String[] webMissionL = gson.fromJson(jsonList,
                    String[].class);

            webMissionList = Arrays.asList(webMissionL);
            webMissionList = new ArrayList<>(webMissionList);
        } else
            return null;

        return (ArrayList<String>) webMissionList;
    }

    public String getPrefsPreviousFilterText() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getString(PREFS_PREVIOUS_FILTER_TEXT, "");
    }

    public String getPrefsCurrentYearRange() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        return settings.getString(PREFS_CURRENT_YEAR_RANGE, String.valueOf(year));
    }

    public String getPrefsStatusValue() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        return settings.getString(PREFS_STATUS_VALUE, "Status not available");
    }

    public String getPrefsLastUpcomingLaunchUpdate() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getString(PREFS_LAST_UPCOMING_LAUNCH_UPDATE, "");
    }

    public String getPrefsUpcomingFilterText() {
        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        return settings.getString(PREFS_UPCOMING_FILTER_TEXT, "");
    }

    public ArrayList<CheckPosition> getPositionsUpcoming() {
        List<CheckPosition> positions;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_POSITION_LIST_UPCOMING)) {
            String jsonList = settings.getString(PREFS_POSITION_LIST_UPCOMING, null);

            Gson gson = new Gson();
            CheckPosition[] positions1 = gson.fromJson(jsonList,
                    CheckPosition[].class);

            positions = Arrays.asList(positions1);
            positions = new ArrayList<>(positions);
        } else
            return null;

        return (ArrayList<CheckPosition>) positions;
    }

    public ArrayList<CheckPosition> getPositionsPrevious() {
        List<CheckPosition> positions;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_POSITION_LIST_PREVIOUS)) {
            String jsonList = settings.getString(PREFS_POSITION_LIST_PREVIOUS, null);

            Gson gson = new Gson();
            CheckPosition[] positions1 = gson.fromJson(jsonList,
                    CheckPosition[].class);

            positions = Arrays.asList(positions1);
            positions = new ArrayList<>(positions);
        } else
            return null;

        return (ArrayList<CheckPosition>) positions;
    }

    public ArrayList<CheckPosition> getRocketPositionsUpcoming() {
        List<CheckPosition> positions;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_ROCKET_POSITION_UPCOMING)) {
            String jsonList = settings.getString(PREFS_ROCKET_POSITION_UPCOMING, null);

            Gson gson = new Gson();
            CheckPosition[] positions1 = gson.fromJson(jsonList,
                    CheckPosition[].class);

            positions = Arrays.asList(positions1);
            positions = new ArrayList<>(positions);
        } else
            return null;

        return (ArrayList<CheckPosition>) positions;
    }

    public ArrayList<CheckPosition> getRocketPositionsPrevious() {
        List<CheckPosition> positions;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_ROCKET_POSITION_PREVIOUS)) {
            String jsonList = settings.getString(PREFS_ROCKET_POSITION_PREVIOUS, null);

            Gson gson = new Gson();
            CheckPosition[] positions1 = gson.fromJson(jsonList,
                    CheckPosition[].class);

            positions = Arrays.asList(positions1);
            positions = new ArrayList<>(positions);
        } else
            return null;

        return (ArrayList<CheckPosition>) positions;
    }

    public ArrayList<CheckPosition> getPadPositionsUpcoming() {
        List<CheckPosition> positions;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_PAD_POSITION_UPCOMING)) {
            String jsonList = settings.getString(PREFS_PAD_POSITION_UPCOMING, null);

            Gson gson = new Gson();
            CheckPosition[] positions1 = gson.fromJson(jsonList,
                    CheckPosition[].class);

            positions = Arrays.asList(positions1);
            positions = new ArrayList<>(positions);
        } else
            return null;

        return (ArrayList<CheckPosition>) positions;
    }

    public ArrayList<CheckPosition> getPadPositionsPrevious() {
        List<CheckPosition> positions;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_PAD_POSITION_PREVIOUS)) {
            String jsonList = settings.getString(PREFS_PAD_POSITION_PREVIOUS, null);

            Gson gson = new Gson();
            CheckPosition[] positions1 = gson.fromJson(jsonList,
                    CheckPosition[].class);

            positions = Arrays.asList(positions1);
            positions = new ArrayList<>(positions);
        } else
            return null;

        return (ArrayList<CheckPosition>) positions;
    }

    public ArrayList<CheckPosition> getAgencyPositionsUpcoming() {
        List<CheckPosition> positions;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_AGENCY_POSITION_UPCOMING)) {
            String jsonList = settings.getString(PREFS_AGENCY_POSITION_UPCOMING, null);

            Gson gson = new Gson();
            CheckPosition[] positions1 = gson.fromJson(jsonList,
                    CheckPosition[].class);

            positions = Arrays.asList(positions1);
            positions = new ArrayList<>(positions);
        } else
            return null;

        return (ArrayList<CheckPosition>) positions;
    }

    public ArrayList<CheckPosition> getAgencyPositionsPrevious() {
        List<CheckPosition> positions;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_AGENCY_POSITION_PREVIOUS)) {
            String jsonList = settings.getString(PREFS_AGENCY_POSITION_PREVIOUS, null);

            Gson gson = new Gson();
            CheckPosition[] positions1 = gson.fromJson(jsonList,
                    CheckPosition[].class);

            positions = Arrays.asList(positions1);
            positions = new ArrayList<>(positions);
        } else
            return null;

        return (ArrayList<CheckPosition>) positions;
    }

    public HashMap<Integer, List<Mission>> getMissionsUpcoming() {
        HashMap<Integer, List<Mission>> missionsL;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_MISSIONS_LIST_UPCOMING)) {
            Type mapType = new TypeToken<HashMap<Integer, List<Mission>>>() {
            }.getType();

            String jsonMap = settings.getString(PREFS_MISSIONS_LIST_UPCOMING, null);

            Gson gson = new Gson();

            missionsL = gson.fromJson(jsonMap, mapType);

        } else
            return null;

        return missionsL;
    }

    public HashMap<Integer, List<Mission>> getMissionsPrevious() {
        HashMap<Integer, List<Mission>> missionsL;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_MISSIONS_LIST_PREVIOUS)) {
            Type mapType = new TypeToken<HashMap<Integer, List<Mission>>>() {
            }.getType();

            String jsonMap = settings.getString(PREFS_MISSIONS_LIST_PREVIOUS, null);

            Gson gson = new Gson();

            missionsL = gson.fromJson(jsonMap, mapType);

        } else
            return null;

        return missionsL;
    }

    public HashMap<Integer, List<Pad>> getPadsUpcoming() {
        HashMap<Integer, List<Pad>> padL;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_PAD_LIST_UPCOMING)) {
            Type mapType = new TypeToken<HashMap<Integer, List<Pad>>>() {
            }.getType();

            String jsonMap = settings.getString(PREFS_PAD_LIST_UPCOMING, null);

            Gson gson = new Gson();

            padL = gson.fromJson(jsonMap, mapType);

        } else
            return null;

        return padL;
    }

    public HashMap<Integer, List<Pad>> getPadsPrevious() {
        HashMap<Integer, List<Pad>> padL;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_PAD_LIST_PREVIOUS)) {
            Type mapType = new TypeToken<HashMap<Integer, List<Pad>>>() {
            }.getType();

            String jsonMap = settings.getString(PREFS_PAD_LIST_PREVIOUS, null);

            Gson gson = new Gson();

            padL = gson.fromJson(jsonMap, mapType);

        } else
            return null;

        return padL;
    }

    public HashMap<Integer, List<Agency>> getAgenciesUpcoming() {
        HashMap<Integer, List<Agency>> agencyL;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_AGENCY_LIST_UPCOMING)) {
            Type mapType = new TypeToken<HashMap<Integer, List<Agency>>>() {
            }.getType();

            String jsonMap = settings.getString(PREFS_AGENCY_LIST_UPCOMING, null);

            Gson gson = new Gson();

            agencyL = gson.fromJson(jsonMap, mapType);

        } else
            return null;

        return agencyL;
    }

    public HashMap<Integer, List<Agency>> getAgenciesPrevious() {
        HashMap<Integer, List<Agency>> agencyL;

        settings = appContext.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(PREFS_AGENCY_LIST_PREVIOUS)) {
            Type mapType = new TypeToken<HashMap<Integer, List<Agency>>>() {
            }.getType();

            String jsonMap = settings.getString(PREFS_AGENCY_LIST_PREVIOUS, null);

            Gson gson = new Gson();

            agencyL = gson.fromJson(jsonMap, mapType);

        } else
            return null;

        return agencyL;
    }
}
