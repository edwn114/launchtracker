package com.gmail.launchtracker.mvp_presenters;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.Toast;

import com.gmail.launchtracker.R;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.check.CheckPosition;
import com.gmail.launchtracker.models.launchlibrary_1_2.LaunchObjects;
import com.gmail.launchtracker.models.RocketLaunch;
import com.gmail.launchtracker.models.rocket.Rocket;
import com.gmail.launchtracker.mvp_services.NextLaunchesService;
import com.gmail.launchtracker.network.services.ScheduleUpcomingMissionIntentService;
import com.gmail.launchtracker.storage.SharedPreference;
import com.gmail.launchtracker.views.activities.MainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by edwn112 on 9/16/2016.
 */
public class NextLaunchesPresenter {

    private Context mContext;
    private SharedPreference mSharedPreference;
    private SharedPreferences mSharedPref;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private Calendar mCalendar;

    private int value = 10;
    private int next_launch_pos = 0;
    private boolean got_time = false;

    private AlarmManager alarmManager;
    private NotificationManager notificationManager;

    public NextLaunchesPresenter(Context context, SharedPreference sharedPreference,
                                  SharedPreferences sharedPref, Calendar calendar) {
        mContext = context;
        mSharedPreference = sharedPreference;
        mSharedPref = sharedPref;
        mSwipeRefreshLayout = null;
        mCalendar = calendar;

        alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        createObservable();
    }

    public NextLaunchesPresenter(Context context, SharedPreference sharedPreference,
                                  SharedPreferences sharedPref, SwipeRefreshLayout swipeRefreshLayout,
                                  Calendar calendar) {
        mContext = context;
        mSharedPreference = sharedPreference;
        mSharedPref = sharedPref;
        mSwipeRefreshLayout = swipeRefreshLayout;
        mCalendar = calendar;

        value = Integer.parseInt(mSharedPref.getString("value", "10"));

        alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        createObservable();
    }

    private void createObservable() {

        NextLaunchesService nextLaunchesService = new NextLaunchesService();

        Observable<LaunchObjects> observable = nextLaunchesService.getNextLaunchesApi().getLaunches(value);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LaunchObjects>() {
                    @Override
                    public void onCompleted() {

                        if (mSwipeRefreshLayout != null) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }

                        //notifying receivers that polling server was successful
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction(Constants.ACTION_SUCCESS_UPCOMING_LAUNCHES);
                        mContext.sendBroadcast(broadcastIntent);

                        saveLastUpdatedTime();

                        if (mSharedPref.getBoolean("notifications", true)) {
                            setNotifications();
                        }

                        //parseSpaceFlightNowLaunchWebPage();

                        scheduleUpcomingMissionDownloads();

                        if (mSharedPref.getBoolean("launch_updates", true)) {
                            scheduleUpcomingLaunchUpdates();
                        }

                        new WeatherNowPresenter(mContext, mSharedPreference);
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (mSwipeRefreshLayout != null) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }

                        e.printStackTrace();

                        Toast.makeText(mContext, "Error fetching results", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(LaunchObjects objects) {

                        cleanCacheUpcoming(String.valueOf(value));

                        RocketLaunch rocketLaunch = new RocketLaunch();

                        for (int i = 0; i < objects.getLaunches().size(); i++) {
                            rocketLaunch.setValues(i,
                                    objects.getLaunches().get(i).getId(),
                                    objects.getLaunches().get(i).getName(),
                                    objects.getLaunches().get(i).getStatus(),
                                    objects.getLaunches().get(i).getWsstamp(),
                                    objects.getLaunches().get(i).getWestamp(),
                                    objects.getLaunches().get(i).getNetstamp(),
                                    objects.getLaunches().get(i).getTbdtime(),
                                    objects.getLaunches().get(i).getVidURLs(),
                                    objects.getLaunches().get(i).getLocation().getPads().get(0).getName(),
                                    objects.getLaunches().get(i).getLocation().getPads().get(0).getLatitude(),
                                    objects.getLaunches().get(i).getLocation().getPads().get(0).getLongitude());

                            mSharedPreference.addUpcomingLaunch(rocketLaunch);

                            if (rocketLaunch.getNet() != 0 && !got_time) {
                                got_time = true;
                                next_launch_pos = i;
                            }
                        }

                        mSharedPreference.saveLaunchesUpcoming(objects.getLaunches());
                    }
                });
    }

    private void cleanCacheUpcoming(String value) {

        mSharedPreference.removeUpcomingLaunches();
        mSharedPreference.removeLaunchesUpcoming();

        mSharedPreference.removePositionsUpcoming();
        mSharedPreference.removeRocketPositionsUpcoming();
        mSharedPreference.removePadPositionsUpcoming();
        mSharedPreference.removeAgencyPositionsUpcoming();
        mSharedPreference.removePadsUpcoming();
        mSharedPreference.removeAgenciesUpcoming();
        mSharedPreference.removeDetailRocketUpcoming();
        mSharedPreference.setPrefsFetchedWeather(false);
        mSharedPreference.setStatus(false);

        CheckPosition checkPosition = new CheckPosition(false);

        ArrayList<Rocket> rockets = new ArrayList<>();
        ArrayList<String> dummy_web = new ArrayList<>();

        for (int i = 0; i < Integer.parseInt(value); i++) {
            mSharedPreference.addPositionUpcoming(checkPosition);
            mSharedPreference.addRocketPositionUpcoming(checkPosition);
            mSharedPreference.addPadPositionUpcoming(checkPosition);
            mSharedPreference.addAgencyPositionUpcoming(checkPosition);

            rockets.add(new com.gmail.launchtracker.models.rocket.Rocket());
            dummy_web.add(" ");
        }

        mSharedPreference.saveDetailRocketsUpcoming(rockets);
        mSharedPreference.saveWebData(dummy_web);
        mSharedPreference.saveWebMissionList(dummy_web);
    }

    private void saveLastUpdatedTime() {
        mCalendar.setTimeInMillis(System.currentTimeMillis());

        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("dd MMMM HH:mm aa", Locale.getDefault());
        simpleDateFormat.setTimeZone(mCalendar.getTimeZone());

        mSharedPreference.setPrefsLastUpcomingLaunchUpdate(simpleDateFormat.format(mCalendar.getTime()));
    }

    private void setNotifications() {

        RocketLaunch rocketLaunch = mSharedPreference.getUpcomingLaunches().get(next_launch_pos);

        Intent intent = new Intent(Constants.ACTION_NOTIFY_USER);

        int alarmType = AlarmManager.RTC;

        String x_hours_text = mSharedPref.getString("x_hours_notification", "0");
        String x_min_text = mSharedPref.getString("x_minutes_notification", "0");

        long notify_before_x_hours = 0;
        long notify_before_x_minutes = 0;

        boolean notify_x_hours, notify_x_minutes;

        if (x_hours_text.equals("0") || x_hours_text.equals(""))
            notify_x_hours = false;
        else {
            notify_before_x_hours = rocketLaunch.getNet() * 1000 -
                    (Integer.parseInt(x_hours_text) * 60 * 60 * 1000);
            notify_x_hours = true;
        }

        if (x_min_text.equals("0") || x_min_text.equals(""))
            notify_x_minutes = false;
        else {
            notify_before_x_minutes = rocketLaunch.getNet() * 1000 -
                    (Integer.parseInt(x_min_text) * 60 * 1000);
            notify_x_minutes = true;
        }

        long notify_before_one_day = rocketLaunch.getNet() * 1000 - (24 * 60 * 60 * 1000);
        long notify_before_five_min = rocketLaunch.getNet() * 1000 - (5 * 60 * 1000);

        PendingIntent pendingIntent_beforeOneDay = PendingIntent.getBroadcast(mContext,
                Constants.NOTIFY_USER_BEFORE_ONE_DAY, intent, 0);
        PendingIntent pendingIntent_beforeX_hours = PendingIntent.getBroadcast(mContext,
                Constants.NOTIFY_USER_BEFORE_X_HOURS, intent, 0);
        PendingIntent pendingIntent_beforeFiveMin = PendingIntent.getBroadcast(mContext,
                Constants.NOTIFY_USER_BEFORE_FIVE_MINUTES, intent, 0);
        PendingIntent pendingIntent_beforeX_min = PendingIntent.getBroadcast(mContext,
                Constants.NOTIFY_USER_BEFORE_X_MINUTES, intent, 0);

        boolean can_notify_user = false;

        if (notify_before_one_day > System.currentTimeMillis() &&
                mSharedPref.getBoolean("day_notification", true)) {
            alarmManager.set(alarmType, notify_before_one_day, pendingIntent_beforeOneDay);

            can_notify_user = true;
        } else {
            alarmManager.cancel(pendingIntent_beforeOneDay);
        }

        if (notify_before_x_hours > System.currentTimeMillis() && notify_x_hours) {
            alarmManager.set(alarmType, notify_before_x_hours, pendingIntent_beforeX_hours);

            can_notify_user = true;
        } else {
            alarmManager.cancel(pendingIntent_beforeX_hours);
        }

        if (notify_before_five_min > System.currentTimeMillis() &&
                mSharedPref.getBoolean("five_min_notification", true)) {
            alarmManager.set(alarmType, notify_before_five_min, pendingIntent_beforeFiveMin);

            can_notify_user = true;
        } else {
            alarmManager.cancel(pendingIntent_beforeFiveMin);
        }

        if (notify_before_x_minutes > System.currentTimeMillis() && notify_x_minutes) {
            alarmManager.set(alarmType, notify_before_x_minutes, pendingIntent_beforeX_min);

            can_notify_user = true;
        } else {
            alarmManager.cancel(pendingIntent_beforeX_min);
        }

        if (can_notify_user)
            notifyUser();
    }

    private void notifyUser() {
        CharSequence title = "Launch Tracker";
        CharSequence text = "Notification set for next launch";

        PendingIntent contentIntent = PendingIntent.getActivity(mContext,
                Constants.NOTIFY_USER, new Intent(mContext, MainActivity.class), 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_add_alert_white_24dp)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .setContentIntent(contentIntent);

        notificationManager.notify(Constants.NOTIFY_USER, notificationBuilder.build());
    }

    private void scheduleUpcomingLaunchUpdates() {

        String frequency = mSharedPref.getString("launch_update_frequency", "null");

        long time_to_add;

        switch (frequency) {
            case "twice_a_day":
                time_to_add = 11 * 60 * 60 * 1000;
                break;
            case "once_a_day":
                time_to_add = 23 * 60 * 60 * 1000;
                break;
            case "every_other_day":
                time_to_add = 2 * 23 * 60 * 60 * 1000;
                break;
            case "once_a_week":
                time_to_add = 6 * 23 * 60 * 60 * 1000;
                break;
            default:
                time_to_add = 23 * 60 * 60 * 1000;
                break;
        }

        Intent intent = new Intent(Constants.ACTION_UPDATE_UPCOMING_LAUNCHES);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext,
                Constants.UPCOMING_LAUNCHES, intent, 0);

        int alarmType = AlarmManager.ELAPSED_REALTIME;
        long time_to_update = SystemClock.elapsedRealtime() + time_to_add;

        alarmManager.setInexactRepeating(alarmType, time_to_update, time_to_add, pendingIntent);
    }

    // TODO: 9/17/2016 Implement multiple links for webcasts

    private void scheduleUpcomingMissionDownloads() {

        Intent intent = new Intent(mContext, ScheduleUpcomingMissionIntentService.class);
        mContext.startService(intent);
    }

}
