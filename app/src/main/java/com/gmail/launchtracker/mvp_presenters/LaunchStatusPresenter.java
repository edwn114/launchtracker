package com.gmail.launchtracker.mvp_presenters;

import android.content.Context;
import android.support.v7.widget.Toolbar;

import com.gmail.launchtracker.R;
import com.gmail.launchtracker.models.LaunchStatus;
import com.gmail.launchtracker.models.Type;
import com.gmail.launchtracker.mvp_services.LaunchStatusService;
import com.gmail.launchtracker.storage.SharedPreference;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by edwn112 on 7/17/2016.
 */
public class LaunchStatusPresenter {

    private Context mContext;
    private int mStatus;
    private SharedPreference mSharedPreference;
    private Toolbar mToolbar;

    public LaunchStatusPresenter(Context context, SharedPreference sharedPreference, Toolbar toolbar) {
        mContext = context;
        mSharedPreference = sharedPreference;
        mToolbar = toolbar;

        int size = 0;

        try {
            size = sharedPreference.getUpcomingLaunches().size();
        } catch (Exception ignored) {
        }

        if (size <= 0)
            return;

        int pos = 0;

        for (int i = 0; i < size; i++) {
            if (sharedPreference.getUpcomingLaunches().get(i).getNet() != 0) {
                pos = i;

                break;
            }
        }

        mStatus = sharedPreference.getUpcomingLaunches()
                .get(pos).getStatus();

        createObservable();
    }

    public void createObservable() {
        LaunchStatusService launchStatusService = new LaunchStatusService();

        Observable<LaunchStatus> status = launchStatusService.getApi().getStatus(mStatus);

        status.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LaunchStatus>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mSharedPreference.setPrefsStatusValue(
                                mContext.getString(R.string.status_not_available));
                        mToolbar.setTitle(mContext.getString(R.string.status_not_available));
                    }

                    @Override
                    public void onNext(LaunchStatus types) {
                        mSharedPreference.setStatus(true);

                        for (int i = 0; i < types.getTypes().size(); i++) {
                            Type type = types.getTypes().get(i);

                            mSharedPreference.setPrefsStatusValue(type.getDescription());

                            mToolbar.setTitle(type.getDescription());
                        }
                    }
                });
    }
}
