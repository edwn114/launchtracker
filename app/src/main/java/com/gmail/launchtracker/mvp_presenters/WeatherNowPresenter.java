package com.gmail.launchtracker.mvp_presenters;

import android.content.Context;
import android.content.Intent;

import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.weatherNow.MainRequest;
import com.gmail.launchtracker.models.weatherNow.WeatherSkeleton;
import com.gmail.launchtracker.network.services.GetWeatherIntentService;
import com.gmail.launchtracker.mvp_services.WeatherNowService;
import com.gmail.launchtracker.storage.SharedPreference;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by edwn112 on 7/17/2016.
 */
public class WeatherNowPresenter {
    private Context mContext;
    private SharedPreference mSharedPreference;
    private String lat, lon;

    public WeatherNowPresenter(Context context, SharedPreference sharedPreference) {

        mContext = context;
        mSharedPreference = sharedPreference;

        configureLatestLaunch();

        createObservable();
    }

    public void configureLatestLaunch() {

        int pos = 0;
        int size = 0;

        try {
            size = mSharedPreference.getUpcomingLaunches().size();
        } catch (Exception ignored) {

        }

        if (size <= 0)
            return;

        for (int i = 0; i < mSharedPreference.getUpcomingLaunches().size(); i++) {
            if (mSharedPreference.getUpcomingLaunches().get(i).getNet() != 0) {
                pos = i;

                break;
            }
        }

        lat = mSharedPreference.getUpcomingLaunches().get(pos).getLatitude();
        lon = mSharedPreference.getUpcomingLaunches().get(pos).getLongitude();
    }

    public void createObservable() {
        WeatherNowService weatherNowService = new WeatherNowService();
        Observable<MainRequest> weatherNow = weatherNowService.getApi().getWeatherNow(lat, lon, Constants.APPID);

        weatherNow.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MainRequest>() {
                    @Override
                    public void onCompleted() {

                        scheduleWeatherUpdates();
                    }

                    @Override
                    public void onError(Throwable e) {

                        mSharedPreference.setPrefsFetchedWeather(false);
                    }

                    @Override
                    public void onNext(MainRequest response) {
                        String description = null;
                        String iconName = null;

                        if (response.getWeather() != null) {
                            if (response.getWeather().size() > 0) {
                                description = response.getWeather().get(0).getDescription();
                                iconName = response.getWeather().get(0).getIcon();
                            }

                            int temp = (int) response.getMain().getTemp();
                            float pressure = response.getMain().getPressure();
                            float humidity = response.getMain().getHumidity();
                            float windSpeed = response.getWind().getSpeed();

                            int clouds = response.getClouds().getAll();

                            long dt = response.getDt();

                            Calendar calendar = Calendar.getInstance();
                            calendar.setTimeInMillis(System.currentTimeMillis());

                            SimpleDateFormat simpleDateFormat =
                                    new SimpleDateFormat("dd MMMM HH:mm aa");
                            simpleDateFormat.setTimeZone(calendar.getTimeZone());

                            WeatherSkeleton weather = new WeatherSkeleton(description,
                                    String.valueOf(temp), String.valueOf(round(humidity, 1)),
                                    String.valueOf(round(pressure, 1)), String.valueOf(round(windSpeed, 1)),
                                    clouds, iconName, dt, simpleDateFormat.format(calendar.getTime()));

                            mSharedPreference.saveWeather(weather);
                            mSharedPreference.setPrefsFetchedWeather(true);

                            sendBroadCast();
                        }
                    }
                });
    }

    private void sendBroadCast() {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Constants.ACTION_SUCCESS_GET_WEATHER_NOW);

        mContext.sendBroadcast(broadcastIntent);
    }

    private void scheduleWeatherUpdates() {
        Intent intent = new Intent(mContext, GetWeatherIntentService.class);
        intent.putExtra("is_weather_now", true);

        mContext.startService(intent);
    }

    private double round (float value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }
}
