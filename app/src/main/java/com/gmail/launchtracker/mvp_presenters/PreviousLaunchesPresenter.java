package com.gmail.launchtracker.mvp_presenters;

import android.content.Context;
import android.content.Intent;

import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.RocketLaunch;
import com.gmail.launchtracker.models.check.CheckPosition;
import com.gmail.launchtracker.models.launchlibrary_1_2.LaunchObjects;
import com.gmail.launchtracker.models.rocket.Rocket;
import com.gmail.launchtracker.mvp_services.PreviousLaunchesService;
import com.gmail.launchtracker.network.services.SchedulePreviousMissionIntentService;
import com.gmail.launchtracker.storage.SharedPreference;

import java.util.ArrayList;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by edwn112 on 9/19/2016.
 */
public class PreviousLaunchesPresenter {

    private Context mContext;
    private SharedPreference mSharedPreference;

    private String mStart_date, mEnd_date;

    public PreviousLaunchesPresenter(Context context, SharedPreference sharedPreference,
                                      String start_date, String end_date) {

        mContext = context;
        mSharedPreference = sharedPreference;

        mStart_date = start_date;
        mEnd_date = end_date;

        createObservable();
    }

    private void createObservable() {

        PreviousLaunchesService previousLaunchesService = new PreviousLaunchesService();

        Observable<LaunchObjects> observable = previousLaunchesService.getPreviousLaunchesApi()
                .getLaunches(mStart_date, mEnd_date);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LaunchObjects>() {
                    @Override
                    public void onCompleted() {

                        //notifying Main Activity that polling server was successful
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction(Constants.ACTION_SUCCESS_PREVIOUS_LAUNCHES);
                        mContext.sendBroadcast(broadcastIntent);

                        schedulePreviousMissionDownloads();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(LaunchObjects objects) {
                        cleanCachePrevious(objects.getLaunches().size());

                        RocketLaunch rocketLaunch = new RocketLaunch();

                        for (int i = 0; i < objects.getLaunches().size(); i++) {
                            rocketLaunch.setValues(i,
                                    objects.getLaunches().get(i).getId(),
                                    objects.getLaunches().get(i).getName(),
                                    objects.getLaunches().get(i).getStatus(),
                                    objects.getLaunches().get(i).getWsstamp(),
                                    objects.getLaunches().get(i).getWestamp(),
                                    objects.getLaunches().get(i).getNetstamp(),
                                    objects.getLaunches().get(i).getTbdtime(),
                                    objects.getLaunches().get(i).getVidURLs(),
                                    objects.getLaunches().get(i).getLocation().getPads().get(0).getName(),
                                    objects.getLaunches().get(i).getLocation().getPads().get(0).getLatitude(),
                                    objects.getLaunches().get(i).getLocation().getPads().get(0).getLongitude());

                            mSharedPreference.addPreviousLaunch(rocketLaunch);
                        }

                        mSharedPreference.saveLaunchesPrevious(objects.getLaunches());
                    }
                });
    }



    private void cleanCachePrevious(int size) {
        mSharedPreference.removePreviousLaunches();
        mSharedPreference.removeLaunchesPrevious();

        mSharedPreference.removePositionsPrevious();
        mSharedPreference.removeRocketPositionsPrevious();
        mSharedPreference.removeDetailRocketPrevious();
        mSharedPreference.removePadPositionsPrevious();
        mSharedPreference.removeAgencyPositionsPrevious();
        mSharedPreference.removePadsPrevious();
        mSharedPreference.removeAgenciesPrevious();

        CheckPosition checkPosition = new CheckPosition(false);

        ArrayList<Rocket> rockets = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            mSharedPreference.addPositionPrevious(checkPosition);
            mSharedPreference.addRocketPositionPrevious(checkPosition);
            mSharedPreference.addPadPositionPrevious(checkPosition);
            mSharedPreference.addAgencyPositionPrevious(checkPosition);

            rockets.add(new com.gmail.launchtracker.models.rocket.Rocket());
        }

        mSharedPreference.saveDetailRocketsPrevious(rockets);
    }

    private void schedulePreviousMissionDownloads() {
        Intent intent = new Intent(mContext, SchedulePreviousMissionIntentService.class);
        mContext.startService(intent);
    }
}
