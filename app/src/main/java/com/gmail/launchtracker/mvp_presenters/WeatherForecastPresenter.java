package com.gmail.launchtracker.mvp_presenters;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.Toast;

import com.gmail.launchtracker.MyApplication;
import com.gmail.launchtracker.greenDao.DaoSession;
import com.gmail.launchtracker.greenDao.Forecast;
import com.gmail.launchtracker.greenDao.ForecastDao;
import com.gmail.launchtracker.adapter.WeatherForecastAdapter;
import com.gmail.launchtracker.models.Constants;
import com.gmail.launchtracker.models.DayOfWeek;
import com.gmail.launchtracker.models.weatherFiveDayForecast.List;
import com.gmail.launchtracker.models.weatherFiveDayForecast.MainModel;
import com.gmail.launchtracker.mvp_services.WeatherForecastService;
import com.gmail.launchtracker.storage.SharedPreference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by edwn112 on 7/14/2016.
 */
public class WeatherForecastPresenter {

    private String lat, lon;

    private Context mContext;
    private SharedPreference mSharedPreference;
    private ForecastDao mForecastDao;
    private WeatherForecastAdapter mWeatherForecastAdapter;
    private SwipeRefreshLayout mSwipeContainer;

    public WeatherForecastPresenter(Context context, SharedPreference sharedPreference,
                                    WeatherForecastAdapter weatherForecastAdapter, SwipeRefreshLayout swipeRefreshLayout) {

        mContext = context;
        mSharedPreference = sharedPreference;
        mWeatherForecastAdapter = weatherForecastAdapter;
        mSwipeContainer = swipeRefreshLayout;

        DaoSession daoSession = ((MyApplication) context.getApplicationContext()).getDaoSession();
        mForecastDao = daoSession.getForecastDao();

        configureLatestLaunch();
        createObservableOne();
    }

    public WeatherForecastPresenter(Context context, SharedPreference sharedPreference) {
        mContext = context;
        mSharedPreference = sharedPreference;

        DaoSession daoSession = ((MyApplication) context.getApplicationContext()).getDaoSession();
        mForecastDao = daoSession.getForecastDao();

        configureLatestLaunch();
        createObservableTwo();
    }

    private void configureLatestLaunch() {
        int pos = 0;
        int size = 0;

        try {
            size = mSharedPreference.getUpcomingLaunches().size();
        } catch (Exception ignored) {

        }

        if (size <= 0)
            return;

        for (int i = 0; i < mSharedPreference.getUpcomingLaunches().size(); i++) {
            if (mSharedPreference.getUpcomingLaunches().get(i).getNet() != 0) {
                pos = i;

                break;
            }
        }

        lat = mSharedPreference.getUpcomingLaunches().get(pos).getLatitude();
        lon = mSharedPreference.getUpcomingLaunches().get(pos).getLongitude();
    }

    private void createObservableOne() {
        WeatherForecastService weatherForecastService = new WeatherForecastService();

        Observable<MainModel> forecast = weatherForecastService.getApi().getForecast(lat, lon, Constants.APPID);

        forecast.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MainModel>() {
                    @Override
                    public void onCompleted() {
                        mSwipeContainer.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSwipeContainer.setRefreshing(false);
                        Toast.makeText(mContext, "Unable to fetch forecast", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(MainModel mainModel) {
                        mForecastDao.deleteAll();

                        ArrayList<List> lists = mainModel.getLists();

                        SimpleDateFormat format1 = new SimpleDateFormat("EEE", Locale.getDefault());
                        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm", Locale.getDefault());
                        SimpleDateFormat format3 = new SimpleDateFormat("EEEE", Locale.getDefault());
                        SimpleDateFormat format4 = new SimpleDateFormat("dd.MM", Locale.getDefault());

                        for (int i = 0; i < lists.size(); i++) {
                            String desc = lists.get(i).getWeathers().get(0).getDescription();
                            // String time = lists.get(i).getDt_txt();
                            String tempMinMax = String.valueOf((int) lists.get(i).getMain().getTemp_max())
                                    + (char) 0x00B0 + " " + String.valueOf((int) lists.get(i).getMain().getTemp_min())
                                    + (char) 0x00B0;

                            String day = format1.format(new java.util.Date(lists.get(i).getDt() * 1000));
                            String day_full = format3.format(new java.util.Date(lists.get(i).getDt() * 1000));
                            String date = format4.format(new java.util.Date(lists.get(i).getDt() * 1000));
                            String time = format2.format(new java.util.Date(lists.get(i).getDt() * 1000));
                            String icon = lists.get(i).getWeathers().get(0).getIcon();

                            int temp = (int) lists.get(i).getMain().getTemp();
                            double pressure = lists.get(i).getMain().getPressure();
                            double humidity = lists.get(i).getMain().getHumidity();

                            double windSpeed = 0;

                            if (lists.get(i).getWind() != null)
                                windSpeed = lists.get(i).getWind().getSpeed();

                            int cloud = lists.get(i).getClouds().getAll();

                            mForecastDao.insert(new Forecast((long) i, desc, tempMinMax, day, day_full,
                                    date, time, icon, temp, pressure, humidity, windSpeed, cloud));
                        }

                        loadFromDataBase();
                    }
                });
    }

    private void createObservableTwo() {
        WeatherForecastService weatherForecastService = new WeatherForecastService();

        Observable<MainModel> forecast = weatherForecastService.getApi().getForecast(lat, lon, Constants.APPID);

        forecast.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MainModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(MainModel mainModel) {
                        mForecastDao.deleteAll();

                        ArrayList<List> lists = mainModel.getLists();

                        SimpleDateFormat format1 = new SimpleDateFormat("EEE", Locale.getDefault());
                        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm", Locale.getDefault());
                        SimpleDateFormat format3 = new SimpleDateFormat("EEEE", Locale.getDefault());
                        SimpleDateFormat format4 = new SimpleDateFormat("dd.MM", Locale.getDefault());

                        for (int i = 0; i < lists.size(); i++) {
                            String desc = lists.get(i).getWeathers().get(0).getDescription();
                            // String time = lists.get(i).getDt_txt();
                            String tempMinMax = String.valueOf((int) lists.get(i).getMain().getTemp_max())
                                    + (char) 0x00B0 + " " + String.valueOf((int) lists.get(i).getMain().getTemp_min())
                                    + (char) 0x00B0;

                            String day = format1.format(new java.util.Date(lists.get(i).getDt() * 1000));
                            String day_full = format3.format(new java.util.Date(lists.get(i).getDt() * 1000));
                            String date = format4.format(new java.util.Date(lists.get(i).getDt() * 1000));
                            String time = format2.format(new java.util.Date(lists.get(i).getDt() * 1000));
                            String icon = lists.get(i).getWeathers().get(0).getIcon();

                            int temp = (int) lists.get(i).getMain().getTemp();
                            double pressure = lists.get(i).getMain().getPressure();
                            double humidity = lists.get(i).getMain().getHumidity();

                            double windSpeed = 0;

                            if (lists.get(i).getWind() != null)
                                windSpeed = lists.get(i).getWind().getSpeed();

                            int cloud = lists.get(i).getClouds().getAll();

                            mForecastDao.insert(new Forecast((long) i, desc, tempMinMax, day, day_full,
                                    date, time, icon, temp, pressure, humidity, windSpeed, cloud));
                        }
                    }
                });
    }

    private void loadFromDataBase() {
        ArrayList<Forecast> forecastList = (ArrayList<Forecast>) mForecastDao.loadAll();
        ArrayList<Object> objects = new ArrayList<>();

        int i = 0;
        int j = forecastList.size();

        DayOfWeek day_full;

        if (j != 0) {
            day_full = new DayOfWeek(forecastList.get(0).getDay_full(),
                    forecastList.get(0).getDate());

            while (true) {
                if (!forecastList.get(i).getDay_full().equals(day_full.getmDayOfWeek())) {
                    day_full.setmDayOfWeek(forecastList.get(i).getDay_full());

                    objects.add(new DayOfWeek(forecastList.get(i).getDay_full(),
                            forecastList.get(i).getDate()));
                    objects.add(forecastList.get(i));
                } else {
                    objects.add(forecastList.get(i));
                }

                i++;

                if (i >= j)
                    break;
            }
        }

        mWeatherForecastAdapter.clear();
        mWeatherForecastAdapter.setObjects(objects);
    }
}




