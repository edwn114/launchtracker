package com.gmail.launchtracker.greenDao;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig forecastDaoConfig;

    private final ForecastDao forecastDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        forecastDaoConfig = daoConfigMap.get(ForecastDao.class).clone();
        forecastDaoConfig.initIdentityScope(type);

        forecastDao = new ForecastDao(forecastDaoConfig, this);

        registerDao(Forecast.class, forecastDao);
    }
    
    public void clear() {
        forecastDaoConfig.getIdentityScope().clear();
    }

    public ForecastDao getForecastDao() {
        return forecastDao;
    }

}
