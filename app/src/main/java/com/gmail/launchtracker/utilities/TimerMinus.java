package com.gmail.launchtracker.utilities;

/**
 * Created by edwn112 on 11/18/15.
 */
public class TimerMinus {
    private long launchTime;

    public TimerMinus() {
        this.launchTime = 0;
    }

    public void setLaunchTime(long time) {
        this.launchTime = time * 1000;
    }

    public long timeLeft() {
        return launchTime - System.currentTimeMillis();
    }
}
