package com.gmail.launchtracker.utilities;

/**
 * Created by edwn112 on 11/23/15.
 */
public class TimerPlus {

    private long refreshTime;
    private long launchTime;

    public TimerPlus() {
        this.refreshTime = 0;
        this.launchTime = 0;
    }

    public void setRefreshTime(long time) {
        this.refreshTime = time * 1000 + (24 * 60 * 60 * 1000);
        this.launchTime = time * 1000;
    }

    public long getPassedTime() {
        return System.currentTimeMillis() - launchTime;
    }

    public long timeLeft() {
        return refreshTime - System.currentTimeMillis();
    }
}
