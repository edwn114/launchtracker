package com.gmail.launchtracker.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gmail.launchtracker.views.activities.DetailActivity;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.models.RocketLaunch;
import com.gmail.launchtracker.storage.SharedPreference;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by edwin on 11/30/15.
 */
public class UpcomingLaunchAdapter extends RecyclerView.Adapter<UpcomingLaunchAdapter.ViewHolder> {

    private boolean isLocalTime = false;
    private boolean isSubTitleTime = false;

    private static ArrayList<RocketLaunch> rocketLaunches;
    private static AppCompatActivity mActivity;
    private static ActionMode mActionMode;

    private static View view;
    private static int pos;

    private static boolean dark_theme;

    private static ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();

            if (dark_theme)
                inflater.inflate(R.menu.context_menu_upcoming_dark_theme, menu);
            else
                inflater.inflate(R.menu.context_menu_upcoming_light_theme, menu);

            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_share:
                    shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB

                    return true;

                case R.id.add_calendar:
                    addToCalendar();
                    mode.finish();

                    return true;

                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    private static void shareCurrentItem() {
        SharedPreference sharedPreference = SharedPreference.getInstance(mActivity);

        RocketLaunch launch = sharedPreference.getUpcomingLaunches().get(pos);

        String title = launch.getName();

        String time_to_launch = launch.getGlobal_time_without_hours()
                + " " + launch.getGlobal_net();

        List<String> links = sharedPreference.getLaunchesUpcoming().get(pos).getVidURLs();

        // TODO: 10/4/2016 Share multiple links
        String url = "Watch live - " + links.toArray(new String[links.size()])[0];

        String launchDetails;

        if (url.contains("http") || url.contains("https")) {
            launchDetails = title + "\n\n"
                    + launch.getLocation() + "\n\n"
                    + time_to_launch + "\n\n"
                    + url;
        } else {
            launchDetails = title + "\n\n"
                    + launch.getLocation() + "\n\n"
                    + time_to_launch + "\n\n";
        }


        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, launchDetails);
        sendIntent.setType("text/plain");

        mActivity.startActivity(sendIntent);
    }

    private static void addToCalendar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                addCalendarEvent(view.getContext(), getItem(pos).getPosition());
            }
        });

        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setTitle("Add to calendar");

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private static void addCalendarEvent(Context context, int pos) {

        SharedPreference sharedPreference = SharedPreference.getInstance(context);

        RocketLaunch rocketLaunch = sharedPreference.getUpcomingLaunches().get(pos);

        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("title", rocketLaunch.getName());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            intent.putExtra(CalendarContract.Events.EVENT_LOCATION,
                    rocketLaunch.getLatitude() + "," + rocketLaunch.getLongitude());
        }

        intent.putExtra("beginTime", (rocketLaunch.getNet() * 1000) - (15 * 60 * 1000));
        intent.putExtra("endTime", rocketLaunch.getNet() * 1000);

        context.startActivity(intent);
    }

    public UpcomingLaunchAdapter(AppCompatActivity appCompatActivity) {
        mActivity = appCompatActivity;
        rocketLaunches = new ArrayList<>();

        SharedPreferences sharedPref =
                PreferenceManager.getDefaultSharedPreferences(appCompatActivity);

        dark_theme = sharedPref.getBoolean("theme", false);
    }

    public void setRockets(ArrayList<RocketLaunch> rocketsl) {
        rocketLaunches = rocketsl;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvDay;
        private TextView tvMonth;
        private TextView tvTitle;
        private TextView tvSubTitle;

        private SharedPreference sharedPreference;

        public ViewHolder(View v) {
            super(v);

            view = v;

            sharedPreference = SharedPreference.getInstance(v.getContext());

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sharedPreference.saveCurrentPosition(getItem(getAdapterPosition()).getPosition());

                    Intent intent = new Intent(v.getContext(), DetailActivity.class);
                    intent.putExtra("IS_UPCOMING", true);

                    ActivityOptionsCompat activityOptionsCompat =
                            ActivityOptionsCompat.makeScaleUpAnimation(v,
                                    (int) v.getX(), (int) v.getY(), v.getWidth(), v.getHeight());

                    v.getContext().startActivity(intent, activityOptionsCompat.toBundle());
                }
            });

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    pos = getAdapterPosition();

                    if (mActionMode != null) {

                        return false;
                    }

                    // Start the CAB using the ActionMode.Callback defined above
                    mActionMode = mActivity.startSupportActionMode(mActionModeCallback);
                    v.setSelected(true);

                    return true;
                }
            });

            tvDay = (TextView) v.findViewById(R.id.tvDay);
            tvMonth = (TextView) v.findViewById(R.id.tvMonth);
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvSubTitle = (TextView) v.findViewById(R.id.tvSubTitle);

            SharedPreferences sharedPref =
                    PreferenceManager.getDefaultSharedPreferences(v.getContext());
            boolean dark_theme = sharedPref.getBoolean("theme", false);

            if (dark_theme) {
                tvDay.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.dark_theme_primary_text_color));
                tvMonth.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.dark_theme_secondary_text_color));
                tvTitle.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.dark_theme_primary_text_color));
                tvSubTitle.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.dark_theme_secondary_text_color));
            }
        }

        public TextView getTvTitle() {
            return tvTitle;
        }

        TextView getTvSubTitle() {
            return tvSubTitle;
        }

        public TextView getTvDay() {
            return tvDay;
        }

        TextView getTvMonth() {
            return tvMonth;
        }

        @Override
        public void onClick(View v) {
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.launch_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Calendar cal = Calendar.getInstance();

        if (!isLocalTime)
            cal.setTimeZone(TimeZone.getTimeZone("UTC"));

        cal.setTimeInMillis(rocketLaunches.get(position).getNet() * 1000);

        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);

        if (rocketLaunches.get(position).getNet() == 0) {
            viewHolder.getTvDay().setText("Tbd");
            viewHolder.getTvMonth().setText("");
        } else {
            viewHolder.getTvDay().setText(String.valueOf(day));

            switch (month) {
                case 0:
                    viewHolder.getTvMonth().setText("Jan");
                    break;

                case 1:
                    viewHolder.getTvMonth().setText("Feb");
                    break;

                case 2:
                    viewHolder.getTvMonth().setText("Mar");
                    break;

                case 3:
                    viewHolder.getTvMonth().setText("Apr");
                    break;

                case 4:
                    viewHolder.getTvMonth().setText("May");
                    break;

                case 5:
                    viewHolder.getTvMonth().setText("Jun");
                    break;

                case 6:
                    viewHolder.getTvMonth().setText("Jul");
                    break;

                case 7:
                    viewHolder.getTvMonth().setText("Aug");
                    break;

                case 8:
                    viewHolder.getTvMonth().setText("Sep");
                    break;

                case 9:
                    viewHolder.getTvMonth().setText("Oct");
                    break;

                case 10:
                    viewHolder.getTvMonth().setText("Nov");
                    break;

                case 11:
                    viewHolder.getTvMonth().setText("Dec");
                    break;

                default:
                    viewHolder.getTvMonth().setText("Tbd");
                    break;
            }
        }

        viewHolder.getTvTitle().setText(rocketLaunches.get(position).getName());

        if (isSubTitleTime) {
            if (!isLocalTime)
                viewHolder.getTvSubTitle().setText(
                        rocketLaunches.get(position).getGlobal_time_without_day_month());
            else
                viewHolder.getTvSubTitle().setText(
                        rocketLaunches.get(position).getLocal_time_without_day_month());
        } else {
            viewHolder.getTvSubTitle().setText(rocketLaunches.get(position).getLocation());
        }
    }

    @Override
    public int getItemCount() {
        return rocketLaunches.size();
    }

    public void setLocalTime(boolean localTime) {
        this.isLocalTime = localTime;
    }

    public void setSubTitleTime(boolean subTitleTime) {
        this.isSubTitleTime = subTitleTime;
    }

    private static RocketLaunch getItem(int pos) {
        return rocketLaunches.get(pos);
    }

    public void clear() {
        rocketLaunches.clear();
        notifyDataSetChanged();
    }

    // for Animations
    public void animateTo(List<RocketLaunch> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<RocketLaunch> newModels) {
        for (int i = rocketLaunches.size() - 1; i >= 0; i--) {
            final RocketLaunch rocketLaunch = rocketLaunches.get(i);
            if (!newModels.contains(rocketLaunch)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<RocketLaunch> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final RocketLaunch rocketLaunch = newModels.get(i);
            if (!rocketLaunches.contains(rocketLaunch)) {
                addItem(i, rocketLaunch);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<RocketLaunch> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final RocketLaunch rocketLaunch = newModels.get(toPosition);
            final int fromPosition = rocketLaunches.indexOf(rocketLaunch);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    private RocketLaunch removeItem(int position) {
        final RocketLaunch rocketLaunch = rocketLaunches.remove(position);
        notifyItemRemoved(position);
        return rocketLaunch;
    }

    private void addItem(int position, RocketLaunch model) {
        rocketLaunches.add(position, model);
        notifyItemInserted(position);
    }

    private void moveItem(int fromPosition, int toPosition) {
        final RocketLaunch rocketLaunch = rocketLaunches.remove(fromPosition);
        rocketLaunches.add(toPosition, rocketLaunch);
        notifyItemMoved(fromPosition, toPosition);
    }
}
