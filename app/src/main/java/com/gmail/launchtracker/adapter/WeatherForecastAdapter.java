package com.gmail.launchtracker.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.pwittchen.weathericonview.WeatherIconView;
import com.gmail.launchtracker.greenDao.Forecast;
import com.gmail.launchtracker.R;
import com.gmail.launchtracker.models.DayOfWeek;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by edwn112 on 6/2/2016.
 */
public class WeatherForecastAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static List<Object> mObjects;
    private Context mContext;

    private final int FORECAST = 0, DAY_OF_WEEK = 1;

    private AlertDialog alertDialog;

    private TextView tvDayOfWeek;
    private TextView tvDescription;
    private TextView tvTemp;
    private TextView tvWindSpeed;
    private TextView tvHumidity;
    private TextView tvCloudiness;
    private TextView tvPressure;

    private ImageView ivMainIcon;

    public WeatherForecastAdapter(Context context, LayoutInflater layoutInflater, boolean dark_theme) {
        mObjects = new ArrayList<>();
        mContext = context;

        View dialogView = layoutInflater.inflate(R.layout.forecast_dialog, null);

        tvDayOfWeek = (TextView) dialogView.findViewById(R.id.tvDayOfWeek);
        tvDescription = (TextView) dialogView.findViewById(R.id.tvDescription);
        tvTemp = (TextView) dialogView.findViewById(R.id.tvTemp);
        tvWindSpeed = (TextView) dialogView.findViewById(R.id.tvWindSpeed);
        tvHumidity = (TextView) dialogView.findViewById(R.id.tvHumidity);
        tvCloudiness = (TextView) dialogView.findViewById(R.id.tvCloudiness);
        tvPressure = (TextView) dialogView.findViewById(R.id.tvPressure);

        ivMainIcon = (ImageView) dialogView.findViewById(R.id.imMainIcon);

        ImageView ivHumidity = (ImageView) dialogView.findViewById(R.id.ivHumidity);
        ImageView ivWindSpeed = (ImageView) dialogView.findViewById(R.id.ivWindSpeed);
        ImageView ivCloudiness = (ImageView) dialogView.findViewById(R.id.ivCloudiness);
        ImageView ivPressure = (ImageView) dialogView.findViewById(R.id.ivPressure);

        if (dark_theme) {
            ivHumidity.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.water_white));
            ivWindSpeed.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.weather_windy_white));
            ivCloudiness.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.weather_cloudy_white));
            ivPressure.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.weather_pressure_white));

            tvDayOfWeek.setTextColor(ContextCompat.getColor(context, R.color.dark_theme_secondary_text_color));
            tvDescription.setTextColor(ContextCompat.getColor(context, R.color.dark_theme_secondary_text_color));
            tvTemp.setTextColor(ContextCompat.getColor(context, R.color.dark_theme_secondary_text_color));
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialogView);

        alertDialog = alertDialogBuilder.create();
    }

    public void setObjects(List<Object> objects) {
        mObjects = objects;
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener {

        private WeatherIconView imWeather;
        private TextView tvWeatherDescription;
        private TextView tvMinMaxTemp;
        private TextView tvDay;
        private TextView tvTime;

        public ViewHolder1(View v) {
            super(v);

            imWeather = (WeatherIconView) v.findViewById(R.id.imWeather);
            tvWeatherDescription = (TextView) v.findViewById(R.id.tvWeatherDescription);
            tvMinMaxTemp = (TextView) v.findViewById(R.id.tvMinMaxTemp);
            tvDay = (TextView) v.findViewById(R.id.tvDay);
            tvTime = (TextView) v.findViewById(R.id.tvTime);

            SharedPreferences sharedPref =
                    PreferenceManager.getDefaultSharedPreferences(v.getContext());

            boolean dark_theme = sharedPref.getBoolean("theme", false);

            if (dark_theme) {
                tvWeatherDescription.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.dark_theme_primary_text_color));
                tvMinMaxTemp.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.dark_theme_secondary_text_color));
                tvDay.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.dark_theme_primary_text_color));
                tvTime.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.dark_theme_secondary_text_color));

                getImWeather().setIconColor(Color.WHITE);
            } else {
                tvWeatherDescription.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.light_theme_primary_text_color));
                tvMinMaxTemp.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.light_theme_secondary_text_color));
                tvDay.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.light_theme_primary_text_color));
                tvTime.setTextColor(ContextCompat.getColor(v.getContext(),
                        R.color.light_theme_secondary_text_color));

                getImWeather().setIconColor(Color.BLACK);
            }


            v.setOnClickListener(
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            int pos = getAdapterPosition();

                            tvDayOfWeek.setText(((Forecast) mObjects.get(pos)).getDay_full());
                            tvDescription.setText(((Forecast) mObjects.get(pos)).getDesc());
                            tvWindSpeed.setText(String.format("%s m/s", ((Forecast) mObjects.get(pos)).getWindSpeed()));
                            tvHumidity.setText(String.format("%s %%", ((Forecast) mObjects.get(pos)).getHumidity()));
                            tvCloudiness.setText(String.format("%s %%", String.valueOf(((Forecast) mObjects.get(pos)).getCloud())));
                            tvPressure.setText(String.format("%s hp", ((Forecast) mObjects.get(pos)).getPressure()));
                            tvTemp.setText(String.format("%s" + (char) 0x00B0, ((Forecast) mObjects.get(pos)).getTemp()));

                            String iconName = ((Forecast) mObjects.get(pos)).getIcon();

                            if (iconName.equals("02n") || iconName.equals("03d") || iconName.equals("03n")
                                    || iconName.equals("04d") || iconName.equals("04n")) {

                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.cloud_02n03dn04dn));
                            }

                            if (iconName.equals("09d") || iconName.equals("09n")) {
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.rain_09dn));
                            }

                            if (iconName.equals("01d"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.clear_01d));

                            if (iconName.equals("01n"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.clear_01n));

                            if (iconName.equals("02d"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.cloudy_02d));

                            if (iconName.equals("10d"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.rain_10d));

                            if (iconName.equals("10n"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.rain_10n));

                            if (iconName.equals("11d"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.thunder_11d));

                            if (iconName.equals("11n"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.thunder_11n));

                            if (iconName.equals("13d"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.snow_13d));

                            if (iconName.equals("13n"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.snow_13n));

                            if (iconName.equals("50d"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.mist_50d));

                            if (iconName.equals("50n"))
                                ivMainIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.mist_50n));


                            alertDialog.show();
                        }
                    }
            );
        }

        public WeatherIconView getImWeather() {
            return imWeather;
        }

        public TextView getTvWeatherDescription() {
            return tvWeatherDescription;
        }

        public TextView getTvMinMaxTemp() {
            return tvMinMaxTemp;
        }

        public TextView getTvDay() {
            return tvDay;
        }

        public TextView getTvTime() {
            return tvTime;
        }

        @Override
        public void onClick(View v) {
        }
    }

    public class ViewHolder2 extends RecyclerView.ViewHolder {

        private TextView tvDayOfWeek;
        private TextView tvDate;

        public ViewHolder2(View v) {
            super(v);

            tvDayOfWeek = (TextView) v.findViewById(R.id.tvDayOfWeek);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
        }

        public TextView getTvDayOfWeek() {
            return tvDayOfWeek;
        }

        public TextView getTvDate() {
            return tvDate;
        }

        public void setTvDayOfWeek(TextView tvDayOfWeek) {
            this.tvDayOfWeek = tvDayOfWeek;
        }

        public void setTvDate(TextView tvDate) {
            this.tvDate = tvDate;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case FORECAST:
                View v1 = inflater.inflate(R.layout.weather_forecast_item, viewGroup, false);
                viewHolder = new ViewHolder1(v1);
                break;
            case DAY_OF_WEEK:
                View v2 = inflater.inflate(R.layout.forecast_date_item, viewGroup, false);
                viewHolder = new ViewHolder2(v2);
                break;
            default:
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        switch (viewHolder.getItemViewType()) {
            case FORECAST:
                ViewHolder1 vh1 = (ViewHolder1) viewHolder;
                configureViewHolder1(vh1, position);

                break;
            case DAY_OF_WEEK:
                ViewHolder2 vh2 = (ViewHolder2) viewHolder;
                configureViewHolder2(vh2, position);

                break;
            default:
                break;
        }
    }

    public void configureViewHolder1(ViewHolder1 vh1, int position) {

        Forecast pojo = (Forecast) mObjects.get(position);

        vh1.getTvWeatherDescription().setText(pojo.getDesc().toUpperCase());
        vh1.getTvMinMaxTemp().setText(pojo.getTempMinMax());
        vh1.getTvDay().setText(pojo.getDay().toUpperCase());
        vh1.getTvTime().setText(pojo.getTime());

        if (pojo.getIcon().equals("01d"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_day_sunny));
        if (pojo.getIcon().equals("01n"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_night_clear));
        if (pojo.getIcon().equals("02d"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_day_cloudy));
        if (pojo.getIcon().equals("03d") || pojo.getIcon().equals("03n"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_cloud));
        if (pojo.getIcon().equals("02n"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_night_alt_cloudy));
        if (pojo.getIcon().equals("04d") || pojo.getIcon().equals("04n"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_cloudy));
        if (pojo.getIcon().equals("09d") || pojo.getIcon().equals("09n"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_showers));
        if (pojo.getIcon().equals("10d"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_day_rain));
        if (pojo.getIcon().equals("10n"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_night_alt_rain));
        if (pojo.getIcon().equals("11d") || pojo.getIcon().equals("11n"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_thunderstorm));
        if (pojo.getIcon().equals("13d") || pojo.getIcon().equals("13n"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_snow));
        if (pojo.getIcon().equals("50d") || pojo.getIcon().equals("50n"))
            vh1.getImWeather().setIconResource(mContext.getResources().getString(R.string.wi_fog));
    }

    public void configureViewHolder2(ViewHolder2 vh2, int position) {

        DayOfWeek dayOfWeek = (DayOfWeek) mObjects.get(position);

        vh2.getTvDayOfWeek().setText(dayOfWeek.getmDayOfWeek());
        vh2.getTvDate().setText(dayOfWeek.getmDate());
    }

    @Override
    public int getItemCount() {
        return mObjects.size();
    }

    public void clear() {
        mObjects.clear();

        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (mObjects.get(position) instanceof Forecast)
            return FORECAST;
        else if (mObjects.get(position) instanceof DayOfWeek)
            return DAY_OF_WEEK;

        return FORECAST;
    }
}
